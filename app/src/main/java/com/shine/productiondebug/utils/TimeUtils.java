package com.shine.productiondebug.utils;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.media.MediaPlayer;
import android.text.TextUtils;
import android.widget.TextView;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created with Android Studio.
 * User: ryan.hoo.j@gmail.com
 * Date: 9/2/16
 * Time: 6:07 PM
 * Desc: TimeUtils
 */
public class TimeUtils {

    public static long  seconds_of_1day_long = 24 * 60 * 60 * 1000;

    /**
     * 设置每个阶段时间
     */
    private static final int seconds_of_1minute = 60;

    private static final int seconds_of_30minutes = 30 * 60;

    private static final int seconds_of_1hour = 60 * 60;

    private static final int seconds_of_1day = 24 * 60 * 60;

    private static final int seconds_of_15days = seconds_of_1day * 15;

    private static final int seconds_of_30days = seconds_of_1day * 30;

    private static final int seconds_of_6months = seconds_of_30days * 6;

    private static final int seconds_of_1year = seconds_of_30days * 12;

    /**
     * Parse the time in milliseconds into String with the format: hh:mm:ss or mm:ss
     *
     * @param duration The time needs to be parsed.
     */
    @SuppressLint("DefaultLocale")
    public static String formatDuration(int duration) {
        duration /= 1000; // milliseconds into seconds
        int minute = duration / 60;
        int hour = minute / 60;
        minute %= 60;
        int second = duration % 60;
        if (hour != 0)
            return String.format("%2d:%02d:%02d", hour, minute, second);
        else
            return String.format("%02d:%02d", minute, second);
    }


    public static String getTimeSDF(String mTime) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(new Date(Long.parseLong(mTime + "000")));
    }

    /**
     * 和当前时间比较是否过了XX分钟
     */
    public static boolean compareDate(long compareDate, int minutes) {
        return System.currentTimeMillis() - compareDate > minutes * 60 * 1000;
    }

    /**
     * 和当前时间比较
     */
    public static boolean compareDate(String compareDate) {
        if (TextUtils.isEmpty(compareDate))
            return false;
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            /**获取当前时间*/
            Date now = new Date(System.currentTimeMillis());
            Date compare = df.parse(compareDate);
            if (now.before(compare)) {
                return true;
            } else {
                return false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 比较当前时间和服务器返回时间大小
     *
     * @param nowDate
     * @param compareDate
     * @return
     */
    public boolean compareDate(String nowDate, String compareDate) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        try {
            Date now = df.parse(nowDate);
            Date compare = df.parse(compareDate);
            if (now.before(compare)) {
                return true;
            } else {
                return false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static long getCutTime(String mTime) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        /* 当前系统时间*/
        Date date = new Date(System.currentTimeMillis());
        String time1 = simpleDateFormat.format(date);

        /*计算时间差*/
        Date begin = null, end = null;
        try {
            begin = simpleDateFormat.parse(time1);
            end = simpleDateFormat.parse(mTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return end.getTime() - begin.getTime();
    }

    public static long getCutTime(String mTimeStart, String mTimeEnd) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        /*计算时间差*/
        Date begin = null, end = null;
        try {
            begin = simpleDateFormat.parse(mTimeStart);
            end = simpleDateFormat.parse(mTimeEnd);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return end.getTime() - begin.getTime();
    }

    /**
     * 格式化时间
     *
     * @param mTime
     * @return
     */
    public static String getTimeRange(String mTime) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        /**获取当前时间*/
        Date curDate = new Date(System.currentTimeMillis());
        String dataStrNew = sdf.format(curDate);
        Date startTime = null;
        try {
            /**将时间转化成Date*/
            curDate = sdf.parse(dataStrNew);
            Date date = null;
            if (mTime.length() != 13) {
                date = new Date(Integer.parseInt(mTime) * 1000L);
            } else {
                date = new Date(Long.parseLong(mTime));
            }
            startTime = sdf.parse(sdf.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        /**除以1000是为了转换成秒*/
        long between = (curDate.getTime() - startTime.getTime()) / 1000;
//        long between = (curDate.getTime() - startTime.getTime()) / 1000;
        int elapsedTime = (int) (between);
        if (elapsedTime < seconds_of_1minute) {

            return "刚刚";
        }
        if (elapsedTime < seconds_of_1hour) {

            return elapsedTime / seconds_of_1minute + "分钟前";
        }
//        if (elapsedTime < seconds_of_1hour) {
//
//            return "半小时前";
//        }
        if (elapsedTime < seconds_of_1day) {

            return elapsedTime / seconds_of_1hour + "小时前";
        }
        if (elapsedTime < seconds_of_15days) {

            return elapsedTime / seconds_of_1day + "天前";
        }


//        if (elapsedTime < seconds_of_30days) {
//
//            if( mTime.length() != 13) {
//                return new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date(Long.parseLong(String.valueOf(mTime + "000"))));
//            }else {
//                return new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date(Long.parseLong(String.valueOf(mTime))));
//            }
////            return new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date(Long.parseLong(String.valueOf(mTime))));
//
//        }
//        if (elapsedTime < seconds_of_6months) {
//
//            return elapsedTime / seconds_of_30days + "月前";
//        }
//        if (elapsedTime < seconds_of_1year) {
//
//            return new SimpleDateFormat("yyyy-MM-dd").format(new Date(Long.parseLong(String.valueOf(mTime + "000"))));
//
//        }
//        if (elapsedTime >= seconds_of_1year) {
//
////            return elapsedTime / seconds_of_1year + "年前";
//            return new SimpleDateFormat("yyyy-MM-dd").format(new Date(Long.parseLong(String.valueOf(mTime + "000"))));
//        }


        if (mTime.length() != 13) {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date(Long.parseLong(String.valueOf(mTime + "000"))));
        } else {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date(Long.parseLong(String.valueOf(mTime))));
        }
//        return new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date(Long.parseLong(String.valueOf(mTime + "000"))));
    }

    public static String getElapsedTime(long duration) {

        long time = duration;
        long hours = time / seconds_of_1hour;
        long minutes = (time % seconds_of_1hour) / seconds_of_1minute;
        long seconds = time % seconds_of_1hour % seconds_of_1minute;

        StringBuilder sb = new StringBuilder();

        if (hours > 0) {
            sb.append(hours).append("小时");
        }
        if (minutes > 0) {
            sb.append(minutes).append("分钟");
        }
        return sb.append(seconds).append("秒").toString();
    }


    public static int getUrlLOng(String url) {
        MediaPlayer mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(url);
            mediaPlayer.prepare();
            return mediaPlayer.getDuration();
        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }
    }


    public static void startIntRun(final TextView show, int end) {
        ValueAnimator valueAnimator = ValueAnimator.ofInt(0,
                end);
        valueAnimator.setDuration(3000);

        valueAnimator
                .addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                        //设置瞬时的数据值到界面上
                        setShowText(show, valueAnimator.getAnimatedValue().toString());
                        if (valueAnimator.getAnimatedFraction() >= 1) {
//                            //设置状态为停止
//                            mPlayingState = STOPPED;
//                            if (mEndListener != null)
//                                //通知监听器，动画结束事件
//                                mEndListener.onEndFinish();
                        }
                    }
                });
        valueAnimator.start();
    }

    public static void startFloatRun(final TextView show, float end) {
        ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, end);
        valueAnimator.setDuration(3000);

        valueAnimator
                .addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                        //设置瞬时的数据值到界面上
                        setShowText(show, valueAnimator.getAnimatedValue().toString());
//                        if (valueAnimator.getAnimatedFraction() >= 1) {
//                            //设置状态为停止
//                            mPlayingState = STOPPED;
//                            if (mEndListener != null)
//                                //通知监听器，动画结束事件
//                                mEndListener.onEndFinish();
//                        }
                    }
                });
        valueAnimator.start();
    }

    private static void setShowText(TextView show, String toString) {
        show.setText(toString);
    }

    public static long getTimeMills(String formatText) {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = new Date();
        try {
            date = format.parse(formatText);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date.getTime();
    }

    public static String getActiveTime(long startTime, long endTime) {

        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy.MM.dd");
        SimpleDateFormat sdf2 = new SimpleDateFormat("-MM.dd");

        return sdf1.format(new Date(startTime * 1000)) + sdf2.format(new Date(endTime * 1000)) + " 实时更新";
    }

    public static String getTimeDate(long timeMills) {

        SimpleDateFormat format = new SimpleDateFormat("MM-dd HH:mm");
        Date date = new Date(timeMills);
        return format.format(date);
    }

    public static String getTimeDateMills(long timeMills) {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = new Date(timeMills);
        return format.format(date);
    }

    public static Long getTimeMillsByDate(int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);//控制时
        cal.set(Calendar.MONTH, month);//控制分
        cal.set(Calendar.DAY_OF_MONTH, day);//控制秒
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        return cal.getTimeInMillis();
    }

    static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd");

    public static String getConstellation(String month, String day) {

        try {
            long nowDay = simpleDateFormat.parse(month + "-" + day).getTime();

            if ((nowDay >= simpleDateFormat.parse("03-21").getTime()) && (nowDay < simpleDateFormat.parse("04-20").getTime())) {
                return "白羊座";
            } else if ((nowDay >= simpleDateFormat.parse("04-20").getTime()) && (nowDay < simpleDateFormat.parse("05-21").getTime())) {
                return "金牛座";
            } else if ((nowDay >= simpleDateFormat.parse("05-21").getTime()) && (nowDay < simpleDateFormat.parse("06-22").getTime())) {
                return "双子座";
            } else if ((nowDay >= simpleDateFormat.parse("06-22").getTime()) && (nowDay < simpleDateFormat.parse("07-23").getTime())) {
                return "巨蟹座";
            } else if ((nowDay >= simpleDateFormat.parse("07-23").getTime()) && (nowDay < simpleDateFormat.parse("08-23").getTime())) {
                return "狮子座";
            } else if ((nowDay >= simpleDateFormat.parse("08-23").getTime()) && (nowDay < simpleDateFormat.parse("09-23").getTime())) {
                return "处女座";
            } else if ((nowDay >= simpleDateFormat.parse("09-23").getTime()) && (nowDay < simpleDateFormat.parse("10-24").getTime())) {
                return "天秤座";
            } else if ((nowDay >= simpleDateFormat.parse("10-24").getTime()) && (nowDay < simpleDateFormat.parse("11-23").getTime())) {
                return "天蝎座";
            } else if ((nowDay >= simpleDateFormat.parse("11-23").getTime()) && (nowDay < simpleDateFormat.parse("12-21").getTime())) {
                return "射手座";
            } else if ((nowDay >= simpleDateFormat.parse("01-20").getTime()) && (nowDay < simpleDateFormat.parse("02-19").getTime())) {
                return "水瓶座";
            } else if ((nowDay >= simpleDateFormat.parse("02-19").getTime()) && (nowDay < simpleDateFormat.parse("03-21").getTime())) {
                return "双鱼座";
            } else {
                return "摩羯座";
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }


    }


    public static String getMoneth(String mTime) {
        Date date = null;
        if (mTime.length() != 13) {
            date = new Date(Integer.parseInt(mTime) * 1000L);
        } else {
            date = new Date(Long.parseLong(mTime));
        }
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return (c.get(Calendar.MONTH) + 1) + "月";

    }


    public static String getDay(String mTime) {
        Date date = null;
        if (mTime.length() != 13) {
            date = new Date(Integer.parseInt(mTime) * 1000L);
        } else {
            date = new Date(Long.parseLong(mTime));
        }
        Calendar c = Calendar.getInstance();
        c.setTime(date);

        if (c.get(Calendar.DAY_OF_MONTH) < 10) {
            return "0" + c.get(Calendar.DAY_OF_MONTH);
        } else {
            return "" + c.get(Calendar.DAY_OF_MONTH);
        }

    }


    public static String getYYYYMMDD(String time) {

        Date date = null;

        if (time.length() != 13) {
            date = new Date(Long.parseLong(time + "000"));
        } else {
            date = new Date(Long.parseLong(time));
        }

        return new SimpleDateFormat("yyyy.MM.dd").format(date);
    }


    public static String getHHmm(String time) {

        Date date = null;

        if (time.length() != 13) {
            date = new Date(Long.parseLong(time + "000"));
        } else {
            date = new Date(Long.parseLong(time));
        }

        return new SimpleDateFormat("HH:mm").format(date);
    }


    public static String getWeek(String index) {

        String weekStr = "";

        switch (index) {
            case "1":
                weekStr = "周日";
                break;
            case "2":
                weekStr = "周一";
                break;
            case "3":
                weekStr = "周二";
                break;
            case "4":
                weekStr = "周三";
                break;
            case "5":
                weekStr = "周四";
                break;
            case "6":
                weekStr = "周五";
                break;
            case "7":
                weekStr = "周六";
                break;
        }

        return weekStr;

    }
}
