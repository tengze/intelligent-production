package com.shine.productiondebug.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Environment;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.Base64;
import android.view.View;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonUtils {
    /**
     * 下载路径
     */
    public final static String FILE_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/com.shine.productiondebug/";

    public static File getCameraFile() {
        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/DCIM/Camera");
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    /**
     * 判断是否是非负数
     *
     * @return
     */
    public static boolean isNumber(String num) {
        if ("".equals(num) || num == null)
            return false;
        Pattern pattern = Pattern.compile("^\\d+$|\\d+\\.\\d+$");
        Matcher matcher = pattern.matcher(num);
        return matcher.find();
    }

    public static int dp2Pix(Context context, float dp) {
        try {
            float density = context.getResources().getDisplayMetrics().density;
            return (int) (dp * density + 0.5F);
        } catch (Exception e) {
            return (int) dp;
        }
    }

    public static int px2dip(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }

    public static int randomInScope() {
        final int min = 80;
        final int max = 90;
        return new Random().nextInt((max - min) + 1) + min;
    }

    public static int randomInScope(int min, int max) {
        return new Random().nextInt((max - min) + 1) + min;
    }

    /**
     * 随机几位数字
     *
     * @param size 位数
     * @return
     */
    public static String randomNum(int size) {
        Random random = new Random();
        Set<Integer> set = new HashSet<Integer>();
        while (set.size() < size) {
            int randomInt = random.nextInt(10);
            set.add(randomInt);
        }
        StringBuffer sb = new StringBuffer();
        for (Integer i : set) {
            sb.append("" + i);
        }

        return sb.toString();
    }

    /**
     * 姓名后面显示星号
     *
     * @param str
     * @return
     */
    public static String starInName(String str) {
        if (str == null)
            return null;

        if (str.length() > 1) {
            String star = "";
            for (int i = 0; i < str.length() - 1; i++)
                star = star + "*";
            return str.substring(0, 1) + star;
        }
        return str;
    }

    /**
     * 身份证中间显示星号
     *
     * @param str
     * @return
     */
    public static String starInText(String str) {
        if (str == null)
            return null;

        if (str.length() > 10) {
            String star = "";
            for (int i = 0; i < str.length() - 10; i++)
                star = star + "*";
            return str.substring(0, 6) + star + str.substring(str.length() - 4, str.length());
        }
        return str;
    }

    /**
     * 文字中高亮可点击
     *
     * @param tv
     * @param text
     * @param keyWord1
     * @param word1Listener
     */
    public static void setTextHighLightWithClick(TextView tv, String text, String keyWord1, View.OnClickListener word1Listener) {
        tv.setClickable(true);
        tv.setHighlightColor(Color.TRANSPARENT);
        tv.setMovementMethod(LinkMovementMethod.getInstance());

        SpannableString s = new SpannableString(text);
        Pattern pattern1 = Pattern.compile(keyWord1);
        Matcher matcher1 = pattern1.matcher(s);

        while (matcher1.find()) {
            int start = matcher1.start();
            int end = matcher1.end();
            s.setSpan(new MyClickSpan(word1Listener), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        tv.setText(s);
    }

    /**
     * 文字中高亮可点击
     *
     * @param tv
     * @param text
     * @param keyWord1
     * @param keyWord2
     * @param word1Listener
     * @param word2Listener
     */
    public static void setTextHighLightWithClick(TextView tv, String text, String keyWord1, String keyWord2, View.OnClickListener word1Listener, View.OnClickListener word2Listener) {
        tv.setClickable(true);
        tv.setHighlightColor(Color.TRANSPARENT);
        tv.setMovementMethod(LinkMovementMethod.getInstance());

        SpannableString s = new SpannableString(text);
        Pattern pattern1 = Pattern.compile(keyWord1);
        Matcher matcher1 = pattern1.matcher(s);

        Pattern pattern2 = Pattern.compile(keyWord2);
        Matcher matcher2 = pattern2.matcher(s);

        while (matcher1.find()) {
            int start = matcher1.start();
            int end = matcher1.end();
            s.setSpan(new MyClickSpan(word1Listener), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        while (matcher2.find()) {
            int start = matcher2.start();
            int end = matcher2.end();
            s.setSpan(new MyClickSpan(word2Listener), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        tv.setText(s);
    }

    /**
     * 删除文件
     *
     * @param fileName
     * @return
     */
    public static boolean deleteFile(String fileName) {
        boolean status;
        SecurityManager checker = new SecurityManager();
        File file = new File(FILE_PATH + fileName);
        if (file.exists()) {
            checker.checkDelete(file.toString());
            if (file.isFile()) {
                try {
                    file.delete();
                    status = true;
                } catch (SecurityException se) {
                    se.printStackTrace();
                    status = false;
                }
            } else
                status = false;
        } else
            status = false;
        return status;
    }

//    /**
//     * 获取手机型号
//     *
//     * @return 手机型号
//     */
//    public static String getSystemModel() {
//        return android.os.Build.MODEL;
//    }

//    /**
//     * 获取手机厂商
//     *
//     * @return 手机厂商
//     */
//    public static String getDeviceBrand() {
//        return android.os.Build.BRAND;
//    }

    public static Bitmap stringtoBitmap(String string) {
        //将字符串转换成Bitmap类型
        Bitmap bitmap = null;
        try {
            byte[] bitmapArray;
            bitmapArray = Base64.decode(string, Base64.DEFAULT);
            bitmap = BitmapFactory.decodeByteArray(bitmapArray, 0, bitmapArray.length);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }


    public String bitmaptoString(Bitmap bitmap) {
        //将Bitmap转换成字符串
        String string = null;
        ByteArrayOutputStream bStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, bStream);
        byte[] bytes = bStream.toByteArray();
        string = Base64.encodeToString(bytes, Base64.DEFAULT);
        return string;
    }
}
