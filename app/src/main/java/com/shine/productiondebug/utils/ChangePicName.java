package com.shine.productiondebug.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChangePicName {
    private static String filePath = "C:\\Users\\jssh-1\\Desktop\\切图\\筛选 查询 图标\\";
    private static String newFilePath = "C:\\Users\\jssh-1\\Desktop\\切图";
    private static String newName = "ic_icon";

    public static void main(String[] args) {

        createFile(newFilePath);

        File dir = new File(filePath);
        Map<String, List<String>> nameMap = new HashMap();

        String[] names = dir.list();
        for (String name : names) {
            if (!name.contains("."))
                continue;

            String fileName = name.substring(0, name.lastIndexOf("."));
            if (fileName.contains("@2x") || fileName.contains("@3x"))
                fileName = fileName.substring(0, fileName.length() - 3);

            if (nameMap.containsKey(fileName)) {
                List<String> fileNameList = nameMap.get(fileName);
                fileNameList.add(name);
                nameMap.put(fileName, fileNameList);
            } else {
                List<String> fileNameList = new ArrayList<>();
                fileNameList.add(name);
                nameMap.put(fileName, fileNameList);
            }

        }

        int index = 0;
        for (List<String> value : nameMap.values()) {
            for (String name : value){
                if (name.contains("@3x")) {
                    FixFileName(filePath + name, newName + index + ".png", 3);
                } else if (name.contains("@2x")) {
                    FixFileName(filePath + name, newName + index + ".png", 2);
                } else {
                    FixFileName(filePath + name, newName + index + ".png", 1);
                }
            }
            index ++;
        }

    }

    /**
     * 通过文件路径直接修改文件名
     *
     * @param filePath    需要修改的文件的完整路径
     * @param newFileName 需要修改的文件的名称
     * @return
     */
    private static void FixFileName(String filePath, String newFileName, int type) {
        File file = new File(filePath); //指定文件名及路径
        if (type == 1) {
            file.renameTo(new File(newFilePath + "\\mipmap-mdpi\\" + newFileName));
        }
        if (type == 2) {
            file.renameTo(new File(newFilePath + "\\mipmap-hdpi\\" + newFileName));
        }
        if (type == 3) {
            file.renameTo(new File(newFilePath + "\\mipmap-xhdpi\\" + newFileName));
        }
    }

    private static void createFile(String filePath) {
        File file1 = new File(filePath + "\\mipmap-mdpi");
        File file2 = new File(filePath + "\\mipmap-hdpi");
        File file3 = new File(filePath + "\\mipmap-xhdpi");
        if (!file1.exists()) {
            file1.mkdir();
        }
        if (!file2.exists()) {
            file2.mkdir();
        }
        if (!file3.exists()) {
            file3.mkdir();
        }
    }

}
