package com.shine.productiondebug.utils;


import android.graphics.Color;

public class ColorUtil {

    /**
     * Color对象转换成字符串
     * @param color Color对象
     * @return 16进制颜色字符串
     * */
    public static String toHexFromColor(int color) {

        return "#" +
//                Integer.toHexString(Color.alpha(color)) +
                Integer.toHexString(Color.red(color)) +
                Integer.toHexString(Color.green(color)) +
                Integer.toHexString(Color.blue(color));
    }
}
