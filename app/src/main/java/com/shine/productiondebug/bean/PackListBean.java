package com.shine.productiondebug.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PackListBean {


    @SerializedName("PackList")
    private List<PackListDTO> packList;

    public List<PackListDTO> getPackList() {
        return packList;
    }

    public void setPackList(List<PackListDTO> packList) {
        this.packList = packList;
    }

    public static class PackListDTO {
        @SerializedName("PackType")
        private int packType;
        @SerializedName("PackName")
        private String packName;
        boolean isSelect;

        public boolean isSelect() {
            return isSelect;
        }

        public void setSelect(boolean select) {
            isSelect = select;
        }

        public int getPackType() {
            return packType;
        }

        public void setPackType(int packType) {
            this.packType = packType;
        }

        public String getPackName() {
            return packName;
        }

        public void setPackName(String packName) {
            this.packName = packName;
        }
    }
}
