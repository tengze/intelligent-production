package com.shine.productiondebug.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SpotCheckRateBean {
//{"SpotCheckRateModel":[{"StatisticsTime":"2021-05-13","ProductionNum":2,"SpotCheckNum":0,"QualityNum":0}]}
    @SerializedName("SpotCheckRateModel")
    private List<SpotCheckRateModelDTO> spotCheckRateModel;

    public List<SpotCheckRateModelDTO> getSpotCheckRateModel() {
        return spotCheckRateModel;
    }

    public void setSpotCheckRateModel(List<SpotCheckRateModelDTO> spotCheckRateModel) {
        this.spotCheckRateModel = spotCheckRateModel;
    }

    public static class SpotCheckRateModelDTO {
        @SerializedName("StatisticsTime")
        private String statisticsTime;
        @SerializedName("ProductionNum")
        private int productionNum;//生产合格
        @SerializedName("SpotCheckNum")
        private int spotCheckNum;//抽查合格
        @SerializedName("QualityNum")
        private int qualityNum;//质检抽查

        public String getStatisticsTime() {
            return statisticsTime;
        }

        public void setStatisticsTime(String statisticsTime) {
            this.statisticsTime = statisticsTime;
        }

        public int getProductionNum() {
            return productionNum;
        }

        public void setProductionNum(int productionNum) {
            this.productionNum = productionNum;
        }

        public int getSpotCheckNum() {
            return spotCheckNum;
        }

        public void setSpotCheckNum(int spotCheckNum) {
            this.spotCheckNum = spotCheckNum;
        }

        public int getQualityNum() {
            return qualityNum;
        }

        public void setQualityNum(int qualityNum) {
            this.qualityNum = qualityNum;
        }
    }
}
