package com.shine.productiondebug.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BatchNumBean {

    @SerializedName("HistoryInspectionSensors")
    private List<HistoryInspectionBatchDTO> historyInspectionBatch;

    public List<HistoryInspectionBatchDTO> getHistoryInspectionBatch() {
        return historyInspectionBatch;
    }

    public void setHistoryInspectionBatch(List<HistoryInspectionBatchDTO> historyInspectionBatch) {
        this.historyInspectionBatch = historyInspectionBatch;
    }

    public static class HistoryInspectionBatchDTO {
        @SerializedName("BatchNum")
        private String batchNum;
        @SerializedName("FactoryHorizon")
        private String factoryHorizon;

        public String getBatchNum() {
            return batchNum;
        }

        public void setBatchNum(String batchNum) {
            this.batchNum = batchNum;
        }

        public String getFactoryHorizon() {
            return factoryHorizon;
        }

        public void setFactoryHorizon(String factoryHorizon) {
            this.factoryHorizon = factoryHorizon;
        }
    }
}
