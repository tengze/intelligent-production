package com.shine.productiondebug.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UnqualifiedPercentageBean {
// {"UnqualifiedPercentageModel":[
// {"UnqualifiedNumber":48,"FlowName":""},
// {"UnqualifiedNumber":31,"FlowName":"标校准备"},
// {"UnqualifiedNumber":20,"FlowName":"调整传感器零点"},
// {"UnqualifiedNumber":49,"FlowName":"调整传感器线性放大倍数"},
// {"UnqualifiedNumber":11,"FlowName":"通0.5气样"},
// {"UnqualifiedNumber":15,"FlowName":"通1.5气样"},
// {"UnqualifiedNumber":9,"FlowName":"通2.0气样检测响应时间、稳定性"},
// {"UnqualifiedNumber":9,"FlowName":"通3.5气样"},
// {"UnqualifiedNumber":5,"FlowName":"下线"}]}
    @SerializedName("UnqualifiedPercentageModel")
    private List<UnqualifiedPercentageModelDTO> unqualifiedPercentageModel;

    public List<UnqualifiedPercentageModelDTO> getUnqualifiedPercentageModel() {
        return unqualifiedPercentageModel;
    }

    public void setUnqualifiedPercentageModel(List<UnqualifiedPercentageModelDTO> unqualifiedPercentageModel) {
        this.unqualifiedPercentageModel = unqualifiedPercentageModel;
    }

    public static class UnqualifiedPercentageModelDTO {
        @SerializedName("UnqualifiedNumber")
        private int unqualifiedNumber;
        @SerializedName("FlowName")
        private String flowName;

        public int getUnqualifiedNumber() {
            return unqualifiedNumber;
        }

        public void setUnqualifiedNumber(int unqualifiedNumber) {
            this.unqualifiedNumber = unqualifiedNumber;
        }

        public String getFlowName() {
            return flowName;
        }

        public void setFlowName(String flowName) {
            this.flowName = flowName;
        }
    }
}
