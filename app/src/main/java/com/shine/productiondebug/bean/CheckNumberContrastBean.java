package com.shine.productiondebug.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CheckNumberContrastBean {

//    {
//        "CheckNumberContrastModel": [{
//        "StatisticsTime": "2021-05-08",
//                "Model": [{
//            "TotalNum": 2,
//                    "Stage": 2
//        }]
//    }, {
//        "StatisticsTime": "2021-05-11",
//                "Model": [{
//            "TotalNum": 12,
//                    "Stage": 1
//        }]
//    }, {
//        "StatisticsTime": "2021-05-12",
//                "Model": [{
//            "TotalNum": 51,
//                    "Stage": 1
//        }]
//    }, {
//        "StatisticsTime": "2021-05-13",
//                "Model": [{
//            "TotalNum": 43,
//                    "Stage": 1
//        }]
//    }, {
//        "StatisticsTime": "2021-05-14",
//                "Model": [{
//            "TotalNum": 2,
//                    "Stage": 1
//        }]
//    }]
//    }

    @SerializedName("CheckNumberContrastModel")
    private List<CheckNumberContrastModelDTO> checkNumberContrastModel;

    public List<CheckNumberContrastModelDTO> getCheckNumberContrastModel() {
        return checkNumberContrastModel;
    }

    public void setCheckNumberContrastModel(List<CheckNumberContrastModelDTO> checkNumberContrastModel) {
        this.checkNumberContrastModel = checkNumberContrastModel;
    }

    public static class CheckNumberContrastModelDTO {
        @SerializedName("StatisticsTime")
        private String statisticsTime;
        @SerializedName("Model")
        private List<ModelDTO> model;

        public String getStatisticsTime() {
            return statisticsTime;
        }

        public void setStatisticsTime(String statisticsTime) {
            this.statisticsTime = statisticsTime;
        }

        public List<ModelDTO> getModel() {
            return model;
        }

        public void setModel(List<ModelDTO> model) {
            this.model = model;
        }

        public static class ModelDTO {
            @SerializedName("TotalNum")
            private int totalNum;
            @SerializedName("Stage")
            private int stage;

            public int getTotalNum() {
                return totalNum;
            }

            public void setTotalNum(int totalNum) {
                this.totalNum = totalNum;
            }

            public int getStage() {
                return stage;
            }

            public void setStage(int stage) {
                this.stage = stage;
            }
        }
    }

}
