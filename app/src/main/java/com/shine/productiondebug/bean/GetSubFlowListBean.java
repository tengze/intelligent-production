package com.shine.productiondebug.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetSubFlowListBean {

    //{"FlowModule":[
    // {"FlowID":"d03587e4-478e-4e60-b685-1854ec5ced48","FlowName":"标校准备","Describe":"标校准备","InformAttribute":"","UserName":"刘亚洲","CreatTime":"2021-04-25 17:34:43","UserID":"2e082566-6939-4ca8-a253-0e46e60598c4"},
    // {"FlowID":"76fa4cde-980d-4392-9daf-47acd3f5c339","FlowName":"调整传感器零点","Describe":"调整传感器零点","InformAttribute":"","UserName":"刘亚洲","CreatTime":"2021-04-25 17:34:56","UserID":"2e082566-6939-4ca8-a253-0e46e60598c4"},
    // {"FlowID":"fe24ff67-280e-4aec-a0ca-da7b14861732","FlowName":"通0.5气样","Describe":"通0.5气样","InformAttribute":"","UserName":"刘亚洲","CreatTime":"2021-04-25 17:35:07","UserID":"2e082566-6939-4ca8-a253-0e46e60598c4"},
    // {"FlowID":"82e3e0cd-69c8-4dc0-858a-31824c5ba32c","FlowName":"通1.5气样","Describe":"通1.5气样","InformAttribute":"","UserName":"刘亚洲","CreatTime":"2021-04-25 17:35:21","UserID":"2e082566-6939-4ca8-a253-0e46e60598c4"},
    // {"FlowID":"a21963c8-b756-49e6-942a-8b77012ca3f3","FlowName":"通3.5气样","Describe":"通3.5气样","InformAttribute":"","UserName":"刘亚洲","CreatTime":"2021-04-25 17:35:34","UserID":"2e082566-6939-4ca8-a253-0e46e60598c4"},
    // {"FlowID":"89061252-909b-41cf-908b-69895a0aa218","FlowName":"通2.0气样检测响应时间、稳定性","Describe":"通2.0气样检测响应时间、稳定性","InformAttribute":"","UserName":"刘亚洲","CreatTime":"2021-04-25 17:35:44","UserID":"2e082566-6939-4ca8-a253-0e46e60598c4"},
    // {"FlowID":"98736ffe-8ad0-4417-a683-0831f3c94c55","FlowName":"下线","Describe":"下线","InformAttribute":"","UserName":"刘亚洲","CreatTime":"2021-04-25 17:35:56","UserID":"2e082566-6939-4ca8-a253-0e46e60598c4"},
    // {"FlowID":"3dc524e9-4a4a-4785-aee9-ee7ccb69f307","FlowName":"调整传感器线性放大倍数","Describe":"调整传感器线性放大倍数","InformAttribute":"","UserName":"刘亚洲","CreatTime":"2021-04-25 17:36:39","UserID":"2e082566-6939-4ca8-a253-0e46e60598c4"}]}

    @SerializedName("FlowModule")
    private List<FlowModuleDTO> flowModule;

    public List<FlowModuleDTO> getFlowModule() {
        return flowModule;
    }

    public void setFlowModule(List<FlowModuleDTO> flowModule) {
        this.flowModule = flowModule;
    }

    public static class FlowModuleDTO {
        @SerializedName("FlowID")
        private String flowID;
        @SerializedName("FlowName")
        private String flowName;
        @SerializedName("Describe")
        private String describe;
        @SerializedName("InformAttribute")
        private String informAttribute;
        @SerializedName("UserName")
        private String userName;
        @SerializedName("CreatTime")
        private String creatTime;
        @SerializedName("UserID")
        private String userID;

        public String getFlowID() {
            return flowID;
        }

        public void setFlowID(String flowID) {
            this.flowID = flowID;
        }

        public String getFlowName() {
            return flowName;
        }

        public void setFlowName(String flowName) {
            this.flowName = flowName;
        }

        public String getDescribe() {
            return describe;
        }

        public void setDescribe(String describe) {
            this.describe = describe;
        }

        public String getInformAttribute() {
            return informAttribute;
        }

        public void setInformAttribute(String informAttribute) {
            this.informAttribute = informAttribute;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getCreatTime() {
            return creatTime;
        }

        public void setCreatTime(String creatTime) {
            this.creatTime = creatTime;
        }

        public String getUserID() {
            return userID;
        }

        public void setUserID(String userID) {
            this.userID = userID;
        }
    }
}
