package com.shine.productiondebug.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductQualificationRateBean {

//{"ProductQualificationRateModel":[{"StatisticsTime":"2021-05-06","TotalNum":8,"QualifiedNum":0},
// {"StatisticsTime":"2021-05-08","TotalNum":2,"QualifiedNum":0},
// {"StatisticsTime":"2021-05-11","TotalNum":12,"QualifiedNum":0},
// {"StatisticsTime":"2021-05-12","TotalNum":51,"QualifiedNum":0}]}

    @SerializedName("ProductQualificationRateModel")
    private List<ProductQualificationRateModelDTO> productQualificationRateModel;

    public List<ProductQualificationRateModelDTO> getProductQualificationRateModel() {
        return productQualificationRateModel;
    }

    public void setProductQualificationRateModel(List<ProductQualificationRateModelDTO> productQualificationRateModel) {
        this.productQualificationRateModel = productQualificationRateModel;
    }

    public static class ProductQualificationRateModelDTO {
        @SerializedName("StatisticsTime")
        private String statisticsTime;
        @SerializedName("TotalNum")
        private int totalNum;
        @SerializedName("QualifiedNum")
        private int qualifiedNum;

        public String getStatisticsTime() {
            return statisticsTime;
        }

        public void setStatisticsTime(String statisticsTime) {
            this.statisticsTime = statisticsTime;
        }

        public int getTotalNum() {
            return totalNum;
        }

        public void setTotalNum(int totalNum) {
            this.totalNum = totalNum;
        }

        public int getQualifiedNum() {
            return qualifiedNum;
        }

        public void setQualifiedNum(int qualifiedNum) {
            this.qualifiedNum = qualifiedNum;
        }
    }
}
