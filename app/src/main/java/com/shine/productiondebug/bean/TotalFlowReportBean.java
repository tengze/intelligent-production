package com.shine.productiondebug.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TotalFlowReportBean {

    @SerializedName("TotalNum")
    private int totalNum;

    public int getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(int totalNum) {
        this.totalNum = totalNum;
    }

    @SerializedName("AllProcessInspectionSearch")
    private List<AllProcessInspectionSearchDTO> allProcessInspectionSearch;

    public List<AllProcessInspectionSearchDTO> getAllProcessInspectionSearch() {
        return allProcessInspectionSearch;
    }

    public void setAllProcessInspectionSearch(List<AllProcessInspectionSearchDTO> allProcessInspectionSearch) {
        this.allProcessInspectionSearch = allProcessInspectionSearch;
    }

    public static class AllProcessInspectionSearchDTO {
        @SerializedName("TotalFlowReportID")
        private String totalFlowReportID;
        @SerializedName("FactoryNum")
        private String factoryNum;
        @SerializedName("SensorName")
        private String sensorName;
        @SerializedName("BatchNum")
        private String batchNum;
        @SerializedName("BeginTime")
        private String beginTime;
        @SerializedName("TotalFlowName")
        private String totalFlowName;
        @SerializedName("EndTime")
        private String endTime;
        @SerializedName("Result")
        private boolean result;
        @SerializedName("Dept")
        private int dept;
        @SerializedName("UserName")
        private String userName;
        @SerializedName("UserID")
        private String userID;
        @SerializedName("TotalFlowID")
        private String totalFlowID;

        public String getTotalFlowReportID() {
            return totalFlowReportID;
        }

        public void setTotalFlowReportID(String totalFlowReportID) {
            this.totalFlowReportID = totalFlowReportID;
        }

        public String getFactoryNum() {
            return factoryNum;
        }

        public void setFactoryNum(String factoryNum) {
            this.factoryNum = factoryNum;
        }

        public String getSensorName() {
            return sensorName;
        }

        public void setSensorName(String sensorName) {
            this.sensorName = sensorName;
        }

        public String getBatchNum() {
            return batchNum;
        }

        public void setBatchNum(String batchNum) {
            this.batchNum = batchNum;
        }

        public String getBeginTime() {
            return beginTime;
        }

        public void setBeginTime(String beginTime) {
            this.beginTime = beginTime;
        }

        public String getTotalFlowName() {
            return totalFlowName;
        }

        public void setTotalFlowName(String totalFlowName) {
            this.totalFlowName = totalFlowName;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public boolean isResult() {
            return result;
        }

        public void setResult(boolean result) {
            this.result = result;
        }

        public int getDept() {
            return dept;
        }

        public void setDept(int dept) {
            this.dept = dept;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getUserID() {
            return userID;
        }

        public void setUserID(String userID) {
            this.userID = userID;
        }

        public String getTotalFlowID() {
            return totalFlowID;
        }

        public void setTotalFlowID(String totalFlowID) {
            this.totalFlowID = totalFlowID;
        }
    }
}
