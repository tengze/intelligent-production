package com.shine.productiondebug.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CertificateBean {

    @SerializedName("TotalNum")
    private int totalNum;

    public int getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(int totalNum) {
        this.totalNum = totalNum;
    }

    @SerializedName("CertificateModel")
    private List<CertificateModelDTO> certificateModel;

    public List<CertificateModelDTO> getCertificateModel() {
        return certificateModel;
    }

    public void setCertificateModel(List<CertificateModelDTO> certificateModel) {
        this.certificateModel = certificateModel;
    }

    public static class CertificateModelDTO {
        @SerializedName("CertificateID")
        private String certificateID;
        @SerializedName("FactoryNum")
        private String factoryNum;
        @SerializedName("SensorName")
        private String sensorName;
        @SerializedName("BatchNum")
        private String batchNum;
        @SerializedName("ProductionTime")
        private String productionTime;
        @SerializedName("ProductionResult")
        private boolean productionResult;
        @SerializedName("QualityTime")
        private String qualityTime;
        @SerializedName("QualityResult")
        private boolean qualityResult;
        @SerializedName("IsPrint")
        private boolean isPrint;
        @SerializedName("CheckReportID")
        private String checkReportID;
        @SerializedName("InspectionDept")
        private String inspectionDept;
        @SerializedName("InspectionUser")
        private String inspectionUser;
        @SerializedName("Result")
        private String result;
        @SerializedName("CheckResult")
        private String checkResult;
        @SerializedName("CheckTime")
        private String checkTime;
        @SerializedName("CheckDept")
        private String checkDept;
        @SerializedName("CheckName")
        private String checkName;
        @SerializedName("ProductionUserName")
        private String productionUserName;
        @SerializedName("QualityUserName")
        private String qualityUserName;

        public String getProductionUserName() {
            return productionUserName;
        }

        public void setProductionUserName(String productionUserName) {
            this.productionUserName = productionUserName;
        }

        public String getQualityUserName() {
            return qualityUserName;
        }

        public void setQualityUserName(String qualityUserName) {
            this.qualityUserName = qualityUserName;
        }

        public boolean isPrint() {
            return isPrint;
        }

        public void setPrint(boolean print) {
            isPrint = print;
        }

        public String getCheckReportID() {
            return checkReportID;
        }

        public void setCheckReportID(String checkReportID) {
            this.checkReportID = checkReportID;
        }

        public String getInspectionDept() {
            return inspectionDept;
        }

        public void setInspectionDept(String inspectionDept) {
            this.inspectionDept = inspectionDept;
        }

        public String getInspectionUser() {
            return inspectionUser;
        }

        public void setInspectionUser(String inspectionUser) {
            this.inspectionUser = inspectionUser;
        }

        public String getResult() {
            return result;
        }

        public void setResult(String result) {
            this.result = result;
        }

        public String getCheckResult() {
            return checkResult;
        }

        public void setCheckResult(String checkResult) {
            this.checkResult = checkResult;
        }

        public String getCheckTime() {
            return checkTime;
        }

        public void setCheckTime(String checkTime) {
            this.checkTime = checkTime;
        }

        public String getCheckDept() {
            return checkDept;
        }

        public void setCheckDept(String checkDept) {
            this.checkDept = checkDept;
        }

        public String getCheckName() {
            return checkName;
        }

        public void setCheckName(String checkName) {
            this.checkName = checkName;
        }

        public String getCertificateID() {
            return certificateID;
        }

        public void setCertificateID(String certificateID) {
            this.certificateID = certificateID;
        }

        public String getFactoryNum() {
            return factoryNum;
        }

        public void setFactoryNum(String factoryNum) {
            this.factoryNum = factoryNum;
        }

        public String getSensorName() {
            return sensorName;
        }

        public void setSensorName(String sensorName) {
            this.sensorName = sensorName;
        }

        public String getBatchNum() {
            return batchNum;
        }

        public void setBatchNum(String batchNum) {
            this.batchNum = batchNum;
        }

        public String getProductionTime() {
            return productionTime;
        }

        public void setProductionTime(String productionTime) {
            this.productionTime = productionTime;
        }

        public boolean isProductionResult() {
            return productionResult;
        }

        public void setProductionResult(boolean productionResult) {
            this.productionResult = productionResult;
        }

        public String getQualityTime() {
            return qualityTime;
        }

        public void setQualityTime(String qualityTime) {
            this.qualityTime = qualityTime;
        }

        public boolean isQualityResult() {
            return qualityResult;
        }

        public void setQualityResult(boolean qualityResult) {
            this.qualityResult = qualityResult;
        }

        public boolean isIsPrint() {
            return isPrint;
        }

        public void setIsPrint(boolean isPrint) {
            this.isPrint = isPrint;
        }
    }
}
