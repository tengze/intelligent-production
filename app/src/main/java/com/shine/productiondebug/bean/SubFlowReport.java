package com.shine.productiondebug.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubFlowReport {

    @SerializedName("TotalNum")
    private int totalNum;

    public int getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(int totalNum) {
        this.totalNum = totalNum;
    }

    @SerializedName("ProcessInspectionSearch")
    private List<ProcessInspectionSearchDTO> processInspectionSearch;

    public List<ProcessInspectionSearchDTO> getProcessInspectionSearch() {
        return processInspectionSearch;
    }

    public void setProcessInspectionSearch(List<ProcessInspectionSearchDTO> processInspectionSearch) {
        this.processInspectionSearch = processInspectionSearch;
    }

    public static class ProcessInspectionSearchDTO {
        @SerializedName("SubFlowReportID")
        private String subFlowReportID;
        @SerializedName("FactoryNum")
        private String factoryNum;
        @SerializedName("SensorName")
        private String sensorName;
        @SerializedName("BatchNum")
        private String batchNum;
        @SerializedName("BeginTime")
        private String beginTime;
        @SerializedName("TotalFlowID")
        private String totalFlowID;
        @SerializedName("TotalFlowName")
        private String totalFlowName;
        @SerializedName("EndTime")
        private String endTime;
        @SerializedName("SubFlowName")
        private String subFlowName;
        @SerializedName("Result")
        private boolean result;
        @SerializedName("Dept")
        private int dept;
        @SerializedName("UserName")
        private String userName;
        @SerializedName("SubFlowID")
        private String subFlowID;

        public String getSubFlowReportID() {
            return subFlowReportID;
        }

        public void setSubFlowReportID(String subFlowReportID) {
            this.subFlowReportID = subFlowReportID;
        }

        public String getFactoryNum() {
            return factoryNum;
        }

        public void setFactoryNum(String factoryNum) {
            this.factoryNum = factoryNum;
        }

        public String getSensorName() {
            return sensorName;
        }

        public void setSensorName(String sensorName) {
            this.sensorName = sensorName;
        }

        public String getBatchNum() {
            return batchNum;
        }

        public void setBatchNum(String batchNum) {
            this.batchNum = batchNum;
        }

        public String getBeginTime() {
            return beginTime;
        }

        public void setBeginTime(String beginTime) {
            this.beginTime = beginTime;
        }

        public String getTotalFlowID() {
            return totalFlowID;
        }

        public void setTotalFlowID(String totalFlowID) {
            this.totalFlowID = totalFlowID;
        }

        public String getTotalFlowName() {
            return totalFlowName;
        }

        public void setTotalFlowName(String totalFlowName) {
            this.totalFlowName = totalFlowName;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public String getSubFlowName() {
            return subFlowName;
        }

        public void setSubFlowName(String subFlowName) {
            this.subFlowName = subFlowName;
        }

        public boolean isResult() {
            return result;
        }

        public void setResult(boolean result) {
            this.result = result;
        }

        public int getDept() {
            return dept;
        }

        public void setDept(int dept) {
            this.dept = dept;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getSubFlowID() {
            return subFlowID;
        }

        public void setSubFlowID(String subFlowID) {
            this.subFlowID = subFlowID;
        }
    }
}
