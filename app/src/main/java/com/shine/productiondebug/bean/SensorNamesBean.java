package com.shine.productiondebug.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SensorNamesBean {

    @SerializedName("HistoryInspectionSensors")
    private List<HistoryInspectionSensorsDTO> historyInspectionSensors;

    public List<HistoryInspectionSensorsDTO> getHistoryInspectionSensors() {
        return historyInspectionSensors;
    }

    public void setHistoryInspectionSensors(List<HistoryInspectionSensorsDTO> historyInspectionSensors) {
        this.historyInspectionSensors = historyInspectionSensors;
    }

    public static class HistoryInspectionSensorsDTO {
        @SerializedName("BatchNum")
        private String batchNum;
        @SerializedName("FactoryNum")
        private String factoryNum;
        @SerializedName("StageID")
        private int stageID;
        @SerializedName("SensorName")
        private String sensorName;
        @SerializedName("SensorType")
        private String sensorType;
        @SerializedName("Sensorconfigid")
        private String sensorconfigid;

        public String getBatchNum() {
            return batchNum;
        }

        public void setBatchNum(String batchNum) {
            this.batchNum = batchNum;
        }

        public String getFactoryNum() {
            return factoryNum;
        }

        public void setFactoryNum(String factoryNum) {
            this.factoryNum = factoryNum;
        }

        public int getStageID() {
            return stageID;
        }

        public void setStageID(int stageID) {
            this.stageID = stageID;
        }

        public String getSensorName() {
            return sensorName;
        }

        public void setSensorName(String sensorName) {
            this.sensorName = sensorName;
        }

        public String getSensorType() {
            return sensorType;
        }

        public void setSensorType(String sensorType) {
            this.sensorType = sensorType;
        }

        public String getSensorconfigid() {
            return sensorconfigid;
        }

        public void setSensorconfigid(String sensorconfigid) {
            this.sensorconfigid = sensorconfigid;
        }
    }
}
