package com.shine.productiondebug.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CheckNameBean {

    @SerializedName("CheckReport")
    private List<CheckReportDTO> checkReport;

    public List<CheckReportDTO> getCheckReport() {
        return checkReport;
    }

    public void setCheckReport(List<CheckReportDTO> checkReport) {
        this.checkReport = checkReport;
    }

    public static class CheckReportDTO {
        @SerializedName("CheckReportID")
        private String checkReportID;
        @SerializedName("BatchNum")
        private String batchNum;
        @SerializedName("FactoryNum")
        private String factoryNum;
        @SerializedName("SensorName")
        private String sensorName;
        @SerializedName("InspectionDept")
        private String inspectionDept;
        @SerializedName("InspectionUser")
        private String inspectionUser;
        @SerializedName("Result")
        private String result;
        @SerializedName("CheckResult")
        private String checkResult;
        @SerializedName("CheckTime")
        private String checkTime;
        @SerializedName("CheckDept")
        private String checkDept;
        @SerializedName("CheckName")
        private String checkName;

        public String getCheckReportID() {
            return checkReportID;
        }

        public void setCheckReportID(String checkReportID) {
            this.checkReportID = checkReportID;
        }

        public String getBatchNum() {
            return batchNum;
        }

        public void setBatchNum(String batchNum) {
            this.batchNum = batchNum;
        }

        public String getFactoryNum() {
            return factoryNum;
        }

        public void setFactoryNum(String factoryNum) {
            this.factoryNum = factoryNum;
        }

        public String getSensorName() {
            return sensorName;
        }

        public void setSensorName(String sensorName) {
            this.sensorName = sensorName;
        }

        public String getInspectionDept() {
            return inspectionDept;
        }

        public void setInspectionDept(String inspectionDept) {
            this.inspectionDept = inspectionDept;
        }

        public String getInspectionUser() {
            return inspectionUser;
        }

        public void setInspectionUser(String inspectionUser) {
            this.inspectionUser = inspectionUser;
        }

        public String getResult() {
            return result;
        }

        public void setResult(String result) {
            this.result = result;
        }

        public String getCheckResult() {
            return checkResult;
        }

        public void setCheckResult(String checkResult) {
            this.checkResult = checkResult;
        }

        public String getCheckTime() {
            return checkTime;
        }

        public void setCheckTime(String checkTime) {
            this.checkTime = checkTime;
        }

        public String getCheckDept() {
            return checkDept;
        }

        public void setCheckDept(String checkDept) {
            this.checkDept = checkDept;
        }

        public String getCheckName() {
            return checkName;
        }

        public void setCheckName(String checkName) {
            this.checkName = checkName;
        }
    }
}
