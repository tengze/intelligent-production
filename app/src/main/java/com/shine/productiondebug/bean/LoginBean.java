package com.shine.productiondebug.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Copyright (C), 2015-2018
 * FileName: LoginBean
 * Author: Jesse
 * Date: 2018/9/26 9:40
 * Description: ${DESCRIPTION}
 * Version: 1.0
 */
public class LoginBean {

    @SerializedName("Users")
    private List<UsersDTO> users;

    public List<UsersDTO> getUsers() {
        return users;
    }

    public void setUsers(List<UsersDTO> users) {
        this.users = users;
    }

    public static class UsersDTO {
        @SerializedName("UserId")
        private String userId;
        @SerializedName("UserName")
        private String userName;
        @SerializedName("UserPwd")
        private String userPwd;
        @SerializedName("UserLoginTime")
        private String userLoginTime;
        @SerializedName("UserLoginState")
        private boolean userLoginState;
        @SerializedName("UserOperationOverTime")
        private int userOperationOverTime;

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getUserPwd() {
            return userPwd;
        }

        public void setUserPwd(String userPwd) {
            this.userPwd = userPwd;
        }

        public String getUserLoginTime() {
            return userLoginTime;
        }

        public void setUserLoginTime(String userLoginTime) {
            this.userLoginTime = userLoginTime;
        }

        public boolean isUserLoginState() {
            return userLoginState;
        }

        public void setUserLoginState(boolean userLoginState) {
            this.userLoginState = userLoginState;
        }

        public int getUserOperationOverTime() {
            return userOperationOverTime;
        }

        public void setUserOperationOverTime(int userOperationOverTime) {
            this.userOperationOverTime = userOperationOverTime;
        }
    }
}
