package com.shine.productiondebug.bus.event;

import java.util.List;

public class DownLoadUrl {
    List<String> downloadUrlList;

    public DownLoadUrl(List<String> downloadUrlList){
        this.downloadUrlList = downloadUrlList;
    }

    public List<String> getDownloadUrlList() {
        return downloadUrlList;
    }

    public void setDownloadUrlList(List<String> downloadUrlList) {
        this.downloadUrlList = downloadUrlList;
    }
}
