package com.shine.productiondebug.bus;

/**
 * A zero-argument action.
 */

public interface BindingAction {
    void call();
}
