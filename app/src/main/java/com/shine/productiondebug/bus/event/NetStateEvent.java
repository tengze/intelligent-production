package com.shine.productiondebug.bus.event;

public class NetStateEvent {
    private boolean isNetAvailable;

    public NetStateEvent(boolean isNetAvailable) {
        this.isNetAvailable = isNetAvailable;
    }

    public boolean isNetAvailable() {
        return isNetAvailable;
    }

    public void setNetAvailable(boolean netAvailable) {
        isNetAvailable = netAvailable;
    }
}
