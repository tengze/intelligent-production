package com.shine.productiondebug.bus.event;

/**
 * Created by Jesse on 2018/10/20 0020.
 * Version 1.0
 */
public class SelectTabEvent {
    //tab 0:首页 ；1：贷款；2：趣圈；3：我的
    private int tab;

    public SelectTabEvent(int tab){
        this.tab = tab;
    }

    public int getTab() {
        return tab;
    }

    public void setTab(int tab) {
        this.tab = tab;
    }
}
