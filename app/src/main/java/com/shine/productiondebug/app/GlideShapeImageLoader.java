package com.shine.productiondebug.app;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.youth.banner.loader.ImageLoader;

/**
 * Copyright (C), 2015-2018
 * FileName: GlideImageLoader
 * Author: Jesse
 * Date: 2018/9/7 15:11
 * Description: 加载图片工具类，方便拓展
 * Version: 1.0
 */
public class GlideShapeImageLoader extends ImageLoader {


    @Override
    public void displayImage(Context context, Object path, ImageView imageView) {

//        RequestOptions myOptions = new RequestOptions()
//                .centerCrop()
//                .apply(bitmapTransform(new RoundedCornersTransformation(8, 8, RoundedCornersTransformation.CornerType.ALL)));

        Glide.with(context)
                .load(path)
//                .apply(myOptions)
                .into(imageView);
    }

}
