package com.shine.productiondebug.app;

public class Constants {

    //主题：全屏
    public static final int THEME_FULL = 1;

    //主题：普通，状态栏颜色与APP保持一致
    public static final int THEME_NORMAL = 2;

    //主题：状态栏白色，图标颜色反转
    public static final int THEME_WHITE = 3;

    public static final int THEME_BLACK_FULL = 4;

    public static final int THEME_MAIN = 5;

    public static final String BUNDLE_EXTRA = "bundle.extra";
    public static final String BUNDLE_REFRESH = "bundle_refresh";

    public static final int PAGE_SIZE = 10;

    public static final String BUNDLE_URL = "bundle.url";

    //用户信息存放key
    public static final String SP_LOGIN_BY_PASSWORD = "sp_login_by_password";
    public static final String SP_LOGIN_NAME = "sp_login_name";
    public static final String SP_LOGIN_PASS = "sp_login_pass";
    public static final String SP_LOGIN_USER_ID = "sp_login_user_id";
    public static final String SP_IS_REMEMBER_PASSWORD = "sp_is_login";
    public static final String SP_TOKEN = "sp_token";
    public static final String SP_ME = "sp_me";
    public static final String SP_COOKIE = "sp_cookie";


    public static final String SP_FIRST_OPEN_APP = "sp_first_open_app";//第一次打开app
}
