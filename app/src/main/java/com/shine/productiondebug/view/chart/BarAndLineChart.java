package com.shine.productiondebug.view.chart;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.view.View;

import com.shine.productiondebug.utils.ScreenUtils;

import java.util.ArrayList;
import java.util.List;

public class BarAndLineChart extends View {

    private Context mContext;
    private int viewWidth, viewHeight;
    private int leftMargin = 40;

    //底线 上边距
    private int topSpacing;
    //底线 下边距
    private int bottomSpacing;
    //底线 左边距
    private int leftSpacing;
    //底线 右边距
    private int rightSpacing;
    //竖间距
    private int verticalSpacing;
    //横间距
    private int horizontalSpacing;
    //柱状图宽度
    private int barWidth;

    private List<Integer> leftScale = new ArrayList<>();
    private List<String> bottomScale = new ArrayList<>();
    private List<Integer> dataList = new ArrayList<>();
    private List<Integer> lineList = new ArrayList<>();


    public BarAndLineChart(Context context, List<Integer> leftScaleList, List<String> bottomScaleList, List<Integer> inspectDataList, List<Integer> qualifiedDataList) {
        super(context);
        this.mContext = context;
        leftScale.addAll(leftScaleList);
//        leftScale.add(1000);
//        leftScale.add(800);
//        leftScale.add(600);
//        leftScale.add(400);
//        leftScale.add(200);
//        leftScale.add(0);

        bottomScale.addAll(bottomScaleList);
//        bottomScale.add("04/01");
//        bottomScale.add("04/02");
//        bottomScale.add("04/03");
//        bottomScale.add("04/04");
//        bottomScale.add("04/05");
//        bottomScale.add("04/06");
//        bottomScale.add("04/07");

        lineList.addAll(inspectDataList);
//        lineList.add(170);
//        lineList.add(720);
//        lineList.add(660);
//        lineList.add(400);
//        lineList.add(600);
//        lineList.add(130);
//        lineList.add(800);

        dataList.addAll(qualifiedDataList);
//        dataList.add(150);
//        dataList.add(700);
//        dataList.add(600);
//        dataList.add(300);
//        dataList.add(200);
//        dataList.add(100);
//        dataList.add(800);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        viewWidth = ScreenUtils.getScreenWidth(mContext);
        viewHeight = viewWidth / 4 * 2;
        setMeasuredDimension(viewWidth, viewHeight);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        topSpacing = 130;//需要计算
        bottomSpacing = 80;//需要计算
        leftSpacing = 100;//需要计算
        rightSpacing = 40;//需要计算
        //间距
        verticalSpacing = (viewHeight - topSpacing - bottomSpacing) / (leftScale.size() - 1);
        horizontalSpacing = (viewWidth - leftMargin * 2 - leftSpacing - rightSpacing) / (bottomScale.size());

        barWidth = horizontalSpacing / 3;

        //绘制背景
        drawBg(canvas);
        //绘制柱状图
        drawBar(canvas);
        //绘制折线图
        drawLine(canvas);
        //绘制分类
        drawType(canvas);

    }

    /**
     * 绘制背景
     *
     * @param canvas
     */
    private void drawBg(Canvas canvas) {
        Paint bgPaint = new Paint();
        bgPaint.setStyle(Paint.Style.FILL);//充满
        bgPaint.setColor(Color.parseColor("#212A49"));
        bgPaint.setAntiAlias(true);// 设置画笔的锯齿效果
        RectF bgRectF = new RectF(leftMargin, 0, viewWidth - leftMargin, viewHeight);// 设置个新的长方形
        canvas.drawRoundRect(bgRectF, 30, 30, bgPaint);//第二个参数是x半径，第三个参数是y半径

        Paint linePaint = new Paint();
        linePaint.setColor(Color.parseColor("#424E5D"));

        Paint scaleTextPaint = new Paint();
        scaleTextPaint.setColor(Color.WHITE);
        Rect scaleTextRect = new Rect();
        scaleTextPaint.setTextSize(30);


        for (int i = 0; i < leftScale.size(); i++) {
            //横底线
            canvas.drawLine(leftMargin + leftSpacing, topSpacing + i * verticalSpacing, viewWidth - leftMargin - rightSpacing, topSpacing + i * verticalSpacing, linePaint);
            //刻度
            String mText = leftScale.get(i) + "";
            scaleTextPaint.getTextBounds(mText, 0, mText.length(), scaleTextRect);
            canvas.drawText(mText, leftMargin + leftSpacing - scaleTextRect.width() - 20, topSpacing + i * verticalSpacing + scaleTextRect.height() / 2, scaleTextPaint);
        }

        for (int i = 0; i < bottomScale.size() + 1; i++) {
            //竖底线
            canvas.drawLine(leftMargin + leftSpacing + i * horizontalSpacing, topSpacing + (leftScale.size() - 1) * verticalSpacing, leftMargin + leftSpacing + i * horizontalSpacing, topSpacing + (leftScale.size() - 1) * verticalSpacing + 10, linePaint);
        }

        for (int i = 0; i < bottomScale.size(); i++) {
            //刻度
            String mText = bottomScale.get(i);
            scaleTextPaint.getTextBounds(mText, 0, mText.length(), scaleTextRect);
            int middleX = leftMargin + leftSpacing + horizontalSpacing / 2 + i * horizontalSpacing;
            canvas.drawText(mText, middleX - scaleTextRect.width() / 2, topSpacing + (leftScale.size() - 1) * verticalSpacing + scaleTextRect.height() + 20, scaleTextPaint);
        }

    }

    private void drawBar(Canvas canvas) {
        Paint barPaint = new Paint();
        barPaint.setStyle(Paint.Style.FILL);//设置填满

        for (int i = 0; i < dataList.size(); i++) {
            int height = dataList.get(i) * verticalSpacing * (leftScale.size() - 1) / leftScale.get(0);
            int middleX = leftMargin + leftSpacing + horizontalSpacing / 2 + i * horizontalSpacing;

            Shader mShader = new LinearGradient(0, verticalSpacing * (leftScale.size() - 1) - height + topSpacing, 0, viewHeight - bottomSpacing,
                    new int[]{Color.parseColor("#1FB4FC"), Color.parseColor("#166BDF")}, null, Shader.TileMode.MIRROR);
            barPaint.setShader(mShader);

            canvas.drawRect(middleX - barWidth / 2, verticalSpacing * (leftScale.size() - 1) - height + topSpacing, middleX + barWidth / 2, topSpacing + (leftScale.size() - 1) * verticalSpacing, barPaint);
        }

    }

    private void drawLine(Canvas canvas) {
        Paint linePaint = new Paint();
        linePaint.setAntiAlias(true);
        linePaint.setStrokeWidth(5);
        linePaint.setColor(Color.parseColor("#00D98B"));

        int lastX = 0, lastY = 0;
        for (int i = 0; i < lineList.size(); i++) {
            int height = lineList.get(i) * verticalSpacing * (leftScale.size() - 1) / leftScale.get(0);
            height = verticalSpacing * (leftScale.size() - 1) - height + topSpacing;
            int middleX = leftMargin + leftSpacing + horizontalSpacing / 2 + i * horizontalSpacing;

            if (lastX == 0) {
                lastX = middleX;
                lastY = height;
                continue;
            }
            canvas.drawLine(lastX, lastY, middleX, height, linePaint);
            lastX = middleX;
            lastY = height;
        }

    }

    private void drawType(Canvas canvas) {
        Paint textPaint = new Paint();
        textPaint.setColor(Color.WHITE);// 设置红色
        textPaint.setTextSize(40);

        String mText = "合格数量";
        Rect textRect = new Rect();
        textPaint.getTextBounds(mText, 0, mText.length(), textRect);
        canvas.drawText(mText, viewWidth - leftMargin * 3 + 20 - textRect.width(), leftMargin + textRect.height(), textPaint);

        mText = "检查数量";
        textPaint.getTextBounds(mText, 0, mText.length(), textRect);
        canvas.drawText(mText, viewWidth - leftMargin * 3 + 20 - textRect.width() * 2 - 100, leftMargin + textRect.height(), textPaint);

        Paint squarePaint = new Paint();
        squarePaint.setColor(Color.parseColor("#1A93EE"));
        squarePaint.setStyle(Paint.Style.FILL);//设置填满
        canvas.drawRect(viewWidth - leftMargin * 3 + 20 - textRect.width() - 60, leftMargin + 10, viewWidth - leftMargin * 3 + 20 - textRect.width() - 20, leftMargin + textRect.height(), squarePaint);

        squarePaint.setColor(Color.parseColor("#00D98B"));
        squarePaint.setStyle(Paint.Style.FILL);//设置填满
        canvas.drawRect(viewWidth - leftMargin * 3 + 20 - textRect.width() * 2 - 160, leftMargin + 10, viewWidth - leftMargin * 3 + 20 - textRect.width() * 2 - 120, leftMargin + textRect.height(), squarePaint);
    }

}
