package com.shine.productiondebug.view.chart;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.View;

import com.shine.productiondebug.utils.ScreenUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RingChart extends View {

    private Context mContext;
    private int viewWidth, viewHeight;
    private int leftMargin = 40;
    //底线 上边距
    private int topSpacing;
    //底线 下边距
    private int bottomSpacing;
    //底线 左边距
    private int leftSpacing;
    //底线 右边距
    private int rightSpacing;
    //圆环半径
    private int ringRadius;
    //圆环宽度
    private int circleRadius;
    //随机一个圆环起始角度
    private int startAngle;

    //数据数组
    private List<Integer> dataList = new ArrayList<>();
    private List<String> nameList = new ArrayList<>();
    List<String> colorList = new ArrayList<>();
    //角度数组
    private List<Integer> radiusList = new ArrayList<>();

    public RingChart(Context context, List<Integer> mDataList, List<String> mNameList) {
        super(context);
        this.mContext = context;

        dataList.addAll(mDataList);
//        dataList.add(100);
//        dataList.add(80);
//        dataList.add(95);
//        dataList.add(70);
//        dataList.add(85);

        nameList.addAll(mNameList);
//        nameList.add("通气");
//        nameList.add("标校");
//        nameList.add("稳定性");
//        nameList.add("相应时间");
//        nameList.add("其他");

        for (int i = 0; i < nameList.size(); i++)
            colorList.add(getRandColorCode());
//        colorList.add("#1FB4FC");
//        colorList.add("#B936FE");
//        colorList.add("#1A93EE");
//        colorList.add("#00D98B");
//        colorList.add("#EA9748");


        //计算每条数据所占圆环的角度
        int total = 0;
        for (Integer i : dataList) {
            total = total + i;
        }

        for (Integer i : dataList) {
            radiusList.add(i * 360 / total);
        }
    }

    /**
     * 获取十六进制的颜色代码.例如  "#6E36B4" , For HTML ,
     *
     * @return String
     */
    public static String getRandColorCode() {
        String r, g, b;
        Random random = new Random();
        r = Integer.toHexString(random.nextInt(256)).toUpperCase();
        g = Integer.toHexString(random.nextInt(256)).toUpperCase();
        b = Integer.toHexString(random.nextInt(256)).toUpperCase();

        r = r.length() == 1 ? "0" + r : r;
        g = g.length() == 1 ? "0" + g : g;
        b = b.length() == 1 ? "0" + b : b;
        return "#" + r + g + b;
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        viewWidth = ScreenUtils.getScreenWidth(mContext);
        viewHeight = viewWidth / 4 * 2;
        setMeasuredDimension(viewWidth, viewHeight);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        topSpacing = 80;//需要计算
        bottomSpacing = 80;//需要计算
        leftSpacing = 100;//需要计算
        rightSpacing = 40;//需要计算

        ringRadius = (viewHeight - topSpacing - bottomSpacing) / 2;
        circleRadius = ringRadius * 2 / 3;

        int min = 0;
        int max = 359;
        Random random = new Random();
        startAngle = random.nextInt(max) % (max - min + 1) + min;

        //绘制背景
        drawBg(canvas);
        //绘制圆环
        drawRing(canvas);
        //画分类
        drawType(canvas);

    }


    /**
     * 绘制背景
     *
     * @param canvas
     */
    private void drawBg(Canvas canvas) {
        Paint bgPaint = new Paint();
        bgPaint.setStyle(Paint.Style.FILL);//充满
        bgPaint.setColor(Color.parseColor("#212A49"));
        bgPaint.setAntiAlias(true);// 设置画笔的锯齿效果
        RectF bgRectF = new RectF(leftMargin, 0, viewWidth - leftMargin, viewHeight);// 设置个新的长方形
        canvas.drawRoundRect(bgRectF, 30, 30, bgPaint);//第二个参数是x半径，第三个参数是y半径

        Paint linePaint = new Paint();
        linePaint.setColor(Color.parseColor("#424E5D"));

    }

    /**
     * 绘制圆环
     *
     * @param canvas
     */
    private void drawRing(Canvas canvas) {
        Paint bigRingPaint = new Paint();
        bigRingPaint.setAntiAlias(true);
        RectF mRecF = new RectF(ringRadius/2, topSpacing, ringRadius * 3 - ringRadius/2, viewHeight - bottomSpacing);

        Paint centerCirclePaint = new Paint();
        centerCirclePaint.setColor(Color.parseColor("#212A49"));
        centerCirclePaint.setAntiAlias(true);

        for (int i = 0; i < radiusList.size(); i++) {
//            String colorStart = colorList.get(i).substring(0, 3) + "22" + colorList.get(i).substring(5, 7);
//            String colorEnd = colorList.get(i).substring(0, 3) + "88" + colorList.get(i).substring(5, 7);
//            Shader mShader = new LinearGradient(0, 0, 0, viewHeight - topSpacing - bottomSpacing,
//                    new int[]{Color.parseColor(colorStart), Color.parseColor(colorEnd)}, null, Shader.TileMode.MIRROR);
//            bigRingPaint.setShader(mShader);

            bigRingPaint.setColor(Color.parseColor(colorList.get(i)));

            canvas.drawArc(mRecF, startAngle, radiusList.get(i), true, bigRingPaint);
            canvas.drawArc(mRecF, startAngle + radiusList.get(i), 1, true, centerCirclePaint);
            startAngle = startAngle + radiusList.get(i) + 1;
        }

        //画中心圆
        canvas.drawCircle(ringRadius * 2 - ringRadius/2, viewHeight / 2, circleRadius, centerCirclePaint);// 小圆

    }

    private void drawType(Canvas canvas) {
        Paint textPaint = new Paint();
        textPaint.setColor(Color.WHITE);
        Rect textRect = new Rect();
        textPaint.setTextSize(30);

        Paint squarePaint = new Paint();
        squarePaint.setStyle(Paint.Style.FILL);//设置填满

        for (int i = 0; i < nameList.size(); i++) {
            String mText = nameList.get(i);
            textPaint.getTextBounds(mText, 0, mText.length(), textRect);
            canvas.drawText(mText, ringRadius * 3, topSpacing + 30 * i + textRect.height() * i, textPaint);

            squarePaint.setColor(Color.parseColor(colorList.get(i)));
            canvas.drawRect(ringRadius * 3 - 40,
                    topSpacing + 30 * i + textRect.height() * i - 20,
                    ringRadius * 3 - 20,
                    topSpacing + 30 * i + textRect.height() * i, squarePaint);
        }


    }

}
