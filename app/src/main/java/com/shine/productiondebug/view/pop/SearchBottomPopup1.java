package com.shine.productiondebug.view.pop;

import android.app.DatePickerDialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BottomPopupView;
import com.lxj.xpopup.interfaces.OnCancelListener;
import com.lxj.xpopup.interfaces.OnConfirmListener;
import com.lxj.xpopup.util.XPopupUtils;
import com.shine.productiondebug.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class SearchBottomPopup1 extends BottomPopupView implements View.OnClickListener {
    OnConfirmListener confirmListener;
    OnCancelListener cancelListener;
    private List<String> productionConclusionList = new ArrayList<>();
    private List<String> auditorList = new ArrayList<>();
    private List<String> auditorConclusionList = new ArrayList<>();
    private TextView productionConclusionText, auditorText, auditorConclusionText, submitTimeStart, submitTimeEnd, timeStart, timeEnd;
    private int position1, position2, position3;

    private Boolean mProductionResult;
    private String mPBeginTime;
    private String mPEndTime;
    private String mCheckName;
    private Boolean mQualityResult;
    private String mQBeginTime;
    private String mQEndTime;

    public interface OnPopClickListener {
        //        void onPopClick(View view);
        void onConfirmClick(int position1, int position2, int position3, String time1Start, String time1End, String time2Start, String time2End);
    }

    private OnPopClickListener onPopClickListener;

    public SearchBottomPopup1(@NonNull Context context, List<String> str1List, List<String> str2List, List<String> str3List,
                              Boolean productionResult, String pBeginTime, String pEndTime, String checkName, Boolean qualityResult, String qBeginTime, String qEndTime) {
        super(context);
        productionConclusionList.addAll(str1List);
        auditorList.addAll(str2List);
        auditorConclusionList.addAll(str3List);

        mProductionResult = productionResult;
        mPBeginTime = pBeginTime;
        mPEndTime = pEndTime;
        mCheckName = checkName;
        mQualityResult = qualityResult;
        mQBeginTime = qBeginTime;
        mQEndTime = qEndTime;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_search_bottom1;
    }

    @Override
    protected void onCreate() {
        super.onCreate();

        ImageView cancel = (ImageView) findViewById(R.id.cancel);
        TextView confirm = (TextView) findViewById(R.id.confirm);

        productionConclusionText = (TextView) findViewById(R.id.productionConclusionText);
        auditorText = (TextView) findViewById(R.id.auditorText);
        auditorConclusionText = (TextView) findViewById(R.id.auditorConclusionText);

        submitTimeStart = (TextView) findViewById(R.id.submitTimeStart);
        submitTimeEnd = (TextView) findViewById(R.id.submitTimeEnd);
        timeStart = (TextView) findViewById(R.id.timeStart);
        timeEnd = (TextView) findViewById(R.id.timeEnd);

        if (mProductionResult == null) {
            productionConclusionText.setText("全部");
        } else {
            if (mProductionResult) {
                productionConclusionText.setText("合格");
            } else {
                productionConclusionText.setText("不合格");
            }
        }

        if (!TextUtils.isEmpty(mCheckName))
            auditorText.setText(mCheckName);

        if (mQualityResult== null) {
            auditorConclusionText.setText("全部");
        } else {
            if (mQualityResult) {
                auditorConclusionText.setText("合格");
            } else {
                auditorConclusionText.setText("不合格");
            }
        }

        if (!TextUtils.isEmpty(mPBeginTime))
            submitTimeStart.setText(mPBeginTime);
        if (!TextUtils.isEmpty(mPEndTime))
            submitTimeEnd.setText(mPEndTime);

        if (!TextUtils.isEmpty(mQBeginTime))
            timeStart.setText(mQBeginTime);
        if (!TextUtils.isEmpty(mQEndTime))
            timeEnd.setText(mQEndTime);

        confirm.setOnClickListener(this);
        cancel.setOnClickListener(this);
        submitTimeStart.setOnClickListener(this);
        submitTimeEnd.setOnClickListener(this);
        timeStart.setOnClickListener(this);
        timeEnd.setOnClickListener(this);

        findViewById(R.id.productionConclusionLayout).setOnClickListener(this);
        findViewById(R.id.auditorLayout).setOnClickListener(this);
        findViewById(R.id.auditorConclusionLayout).setOnClickListener(this);

    }

    public SearchBottomPopup1 setListener(OnConfirmListener confirmListener, OnCancelListener onCancelListener, OnPopClickListener onPopClickListener) {
        this.confirmListener = confirmListener;
        this.cancelListener = onCancelListener;
        this.onPopClickListener = onPopClickListener;
        return this;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submitTimeStart:
                Calendar calendar1 = Calendar.getInstance();
                new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        submitTimeStart.setText(year + "-" + (month + 1) + "-" + dayOfMonth + " " + "00:00:00");
//                        new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
//                            @Override
//                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//                                submitTimeStart.setText(year + "-" + (month + 1) + "-" + dayOfMonth + " " + hourOfDay + ":" + minute);
//                            }
//                        }, 0, 0, true).show();

                    }
                }, calendar1.get(Calendar.YEAR), calendar1.get(Calendar.MONTH), calendar1.get(Calendar.DAY_OF_MONTH))
                        .show();
                break;
            case R.id.submitTimeEnd:
                Calendar calendar2 = Calendar.getInstance();
                new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
//                        new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
//                            @Override
//                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//                                submitTimeEnd.setText(year + "-" + (month + 1) + "-" + dayOfMonth + " " + hourOfDay + ":" + minute);
//                            }
//                        }, 0, 0, true).show();
                        submitTimeEnd.setText(year + "-" + (month + 1) + "-" + dayOfMonth + " " + "23:59:59");
                    }
                }, calendar2.get(Calendar.YEAR), calendar2.get(Calendar.MONTH), calendar2.get(Calendar.DAY_OF_MONTH))
                        .show();
                break;
            case R.id.timeStart:
                Calendar calendar3 = Calendar.getInstance();
                new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        timeStart.setText(year + "-" + (month + 1) + "-" + dayOfMonth + " " + "00:00:00");
//                        new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
//                            @Override
//                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//                                timeStart.setText(year + "-" + (month + 1) + "-" + dayOfMonth + " " + hourOfDay + ":" + minute);
//                            }
//                        }, 0, 0, true).show();

                    }
                }, calendar3.get(Calendar.YEAR), calendar3.get(Calendar.MONTH), calendar3.get(Calendar.DAY_OF_MONTH))
                        .show();
                break;
            case R.id.timeEnd:
                Calendar calendar4 = Calendar.getInstance();
                new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        timeEnd.setText(year + "-" + (month + 1) + "-" + dayOfMonth + " " + "23:59:59");
//                        new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
//                            @Override
//                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//                                timeEnd.setText(year + "-" + (month + 1) + "-" + dayOfMonth + " " + hourOfDay + ":" + minute);
//                            }
//                        }, 0, 0, true).show();
                    }
                }, calendar4.get(Calendar.YEAR), calendar4.get(Calendar.MONTH), calendar4.get(Calendar.DAY_OF_MONTH))
                        .show();
                break;
            case R.id.confirm:
                if (confirmListener != null) confirmListener.onConfirm();
                onPopClickListener.onConfirmClick(position1, position2, position3,
                        submitTimeStart.getText().toString(), submitTimeEnd.getText().toString(), timeStart.getText().toString(), timeEnd.getText().toString());
                dismiss();
                break;
            case R.id.cancel:
                if (cancelListener != null) cancelListener.onCancel();
                dismiss();
                break;
            case R.id.productionConclusionLayout:
                CommonListBottomPopup commonListBottomPopup = new CommonListBottomPopup(getContext(), productionConclusionList);
                new XPopup.Builder(getContext())
                        .moveUpToKeyboard(false) //如果不加这个，评论弹窗会移动到软键盘上面
                        .enableDrag(true)
                        .isDestroyOnDismiss(true)
                        .maxHeight(200)
                        .asCustom(commonListBottomPopup)/*.enableDrag(false)*/
                        .show();
                commonListBottomPopup.setListener(new CommonListBottomPopup.OnPopItemClickListener() {
                    @Override
                    public void onPopItemClick(View view, int position) {
                        position1 = position;
                        productionConclusionText.setText(productionConclusionList.get(position));
                        commonListBottomPopup.dismiss();
                    }
                });
                break;
            case R.id.auditorLayout:
                CommonListBottomPopup commonListBottomPopup1 = new CommonListBottomPopup(getContext(), auditorList);
                new XPopup.Builder(getContext())
                        .moveUpToKeyboard(false) //如果不加这个，评论弹窗会移动到软键盘上面
                        .enableDrag(true)
                        .isDestroyOnDismiss(true)
                        .maxHeight(200)
                        .asCustom(commonListBottomPopup1)/*.enableDrag(false)*/
                        .show();
                commonListBottomPopup1.setListener(new CommonListBottomPopup.OnPopItemClickListener() {
                    @Override
                    public void onPopItemClick(View view, int position) {
                        position2 = position;
                        auditorText.setText(auditorList.get(position));
                        commonListBottomPopup1.dismiss();
                    }
                });
                break;
            case R.id.auditorConclusionLayout:
                CommonListBottomPopup commonListBottomPopup2 = new CommonListBottomPopup(getContext(), auditorConclusionList);
                new XPopup.Builder(getContext())
                        .moveUpToKeyboard(false) //如果不加这个，评论弹窗会移动到软键盘上面
                        .enableDrag(true)
                        .isDestroyOnDismiss(true)
                        .maxHeight(200)
                        .asCustom(commonListBottomPopup2)/*.enableDrag(false)*/
                        .show();
                commonListBottomPopup2.setListener(new CommonListBottomPopup.OnPopItemClickListener() {
                    @Override
                    public void onPopItemClick(View view, int position) {
                        position3 = position;
                        auditorConclusionText.setText(auditorConclusionList.get(position));
                        commonListBottomPopup2.dismiss();
                    }
                });
                break;
            default:
                break;
        }
    }

    //完全可见执行
    @Override
    protected void onShow() {
        super.onShow();
    }

    //完全消失执行
    @Override
    protected void onDismiss() {
    }

    @Override
    protected int getMaxHeight() {
//        return XPopupUtils.getWindowHeight(getContext());
        return (int) (XPopupUtils.getWindowHeight(getContext()) * .9f);
    }

}
