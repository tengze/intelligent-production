package com.shine.productiondebug.view.pop;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;

import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BottomPopupView;
import com.lxj.xpopup.interfaces.OnCancelListener;
import com.lxj.xpopup.interfaces.OnConfirmListener;
import com.lxj.xpopup.util.XPopupUtils;
import com.shine.productiondebug.R;
import com.shine.productiondebug.bean.BatchNumBean;
import com.shine.productiondebug.bean.FactoryNumBean;
import com.shine.productiondebug.bean.SensorNamesBean;
import com.shine.productiondebug.ui.search.SearchActivity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class SearchBottomPopup extends BottomPopupView implements View.OnClickListener {
    OnConfirmListener confirmListener;
    OnCancelListener cancelListener;
    List<String> resultList = new ArrayList<>();
    private TextView batchNumText, numText, nameText, resultText, timeStart, timeEnd;
    private ImageView numSelectImg;
    private int resultPosition;
    private String mBatchNum;
    private String mFactoryNum;
    private String mSensorName;
    private Boolean mResult;
    private String mBeginTime;
    private String mEndTime;


    public interface OnPopClickListener {
        void onPopClick(View view);

        void onPopClick(View view, String sensorName);

        void onPopClick(View view, String sensorName, String batchNum);

        void onConfirmClick(String batchNum, String factoryNum, String sensorName, Boolean result, String beginTime, String endTime);
    }

    private OnPopClickListener onPopClickListener;

    public SearchBottomPopup(@NonNull Context context, List<String> str4List, String batchNum, String factoryNum, String sensorName,
                             Boolean result, String beginTime, String endTime) {
        super(context);
        resultList.addAll(str4List);

        mBatchNum = batchNum;
        mFactoryNum = factoryNum;
        mSensorName = sensorName;
        mResult = result;
        mBeginTime = beginTime;
        mEndTime = endTime;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_search_bottom;
    }

    @Override
    protected void onCreate() {
        super.onCreate();

        ImageView cancel = (ImageView) findViewById(R.id.cancel);
        TextView confirm = (TextView) findViewById(R.id.confirm);

        batchNumText = (TextView) findViewById(R.id.batchNumText);
        numText = (EditText) findViewById(R.id.numText);
        numSelectImg = (ImageView) findViewById(R.id.numSelectImg);
        nameText = (TextView) findViewById(R.id.nameText);
        resultText = (TextView) findViewById(R.id.resultText);

        timeStart = (TextView) findViewById(R.id.timeStart);
        timeEnd = (TextView) findViewById(R.id.timeEnd);

        if (!TextUtils.isEmpty(mBatchNum))
            batchNumText.setText(mBatchNum);

        if (!TextUtils.isEmpty(mFactoryNum))
            numText.setText(mFactoryNum);

        if (!TextUtils.isEmpty(mSensorName))
            nameText.setText(mSensorName);

        if (mResult == null) {
            resultPosition = 0;
            resultText.setText("全部");
        } else {
            if (mResult) {
                resultPosition = 1;
                resultText.setText("合格");
            } else {
                resultPosition = 2;
                resultText.setText("不合格");
            }
        }

        if (!TextUtils.isEmpty(mBeginTime))
            timeStart.setText(mBeginTime);
        if (!TextUtils.isEmpty(mEndTime))
            timeEnd.setText(mEndTime);

        confirm.setOnClickListener(this);
        cancel.setOnClickListener(this);
        timeStart.setOnClickListener(this);
        timeEnd.setOnClickListener(this);

        findViewById(R.id.batchNumLayout).setOnClickListener(this);
        findViewById(R.id.numSelectImg).setOnClickListener(this);
        findViewById(R.id.nameLayout).setOnClickListener(this);
        findViewById(R.id.resultLayout).setOnClickListener(this);

    }

    public SearchBottomPopup setListener(OnConfirmListener confirmListener, OnCancelListener onCancelListener, OnPopClickListener onPopClickListener) {
        this.confirmListener = confirmListener;
        this.cancelListener = onCancelListener;
        this.onPopClickListener = onPopClickListener;
        return this;
    }

    public void setBatchNum(List<String> batchList) {
        CommonListBottomPopup commonListBottomPopup = new CommonListBottomPopup(getContext(), batchList);
        new XPopup.Builder(getContext())
                .moveUpToKeyboard(false) //如果不加这个，评论弹窗会移动到软键盘上面
                .enableDrag(true)
                .isDestroyOnDismiss(true)
                .maxHeight(200)
                .asCustom(commonListBottomPopup)/*.enableDrag(false)*/
                .show();
        commonListBottomPopup.setListener(new CommonListBottomPopup.OnPopItemClickListener() {
            @Override
            public void onPopItemClick(View view, int position) {
                batchNumText.setText(batchList.get(position));
                commonListBottomPopup.dismiss();
            }
        });
    }

    public void setFactoryNumList(List<String> factoryNumList) {
        CommonListBottomPopup commonListBottomPopup1 = new CommonListBottomPopup(getContext(), factoryNumList);
        new XPopup.Builder(getContext())
                .moveUpToKeyboard(false) //如果不加这个，评论弹窗会移动到软键盘上面
                .enableDrag(true)
                .isDestroyOnDismiss(true)
                .maxHeight(200)
                .asCustom(commonListBottomPopup1)/*.enableDrag(false)*/
                .show();
        commonListBottomPopup1.setListener(new CommonListBottomPopup.OnPopItemClickListener() {
            @Override
            public void onPopItemClick(View view, int position) {
                numText.setText(factoryNumList.get(position));
                commonListBottomPopup1.dismiss();
            }
        });
    }

    public void setSensorNames(List<String> sensorsDTOList) {
        CommonListBottomPopup commonListBottomPopup2 = new CommonListBottomPopup(getContext(), sensorsDTOList);
        new XPopup.Builder(getContext())
                .moveUpToKeyboard(false) //如果不加这个，评论弹窗会移动到软键盘上面
                .enableDrag(true)
                .isDestroyOnDismiss(true)
                .maxHeight(200)
                .asCustom(commonListBottomPopup2)/*.enableDrag(false)*/
                .show();
        commonListBottomPopup2.setListener(new CommonListBottomPopup.OnPopItemClickListener() {
            @Override
            public void onPopItemClick(View view, int position) {
                nameText.setText(sensorsDTOList.get(position));
                commonListBottomPopup2.dismiss();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.timeStart:
                Calendar calendar = Calendar.getInstance();
                new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        timeStart.setText(year + "-" + (month + 1) + "-" + dayOfMonth + " " + "00:00:00");
//                        new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
//                            @Override
//                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//                                timeStart.setText(year + "-" + (month + 1) + "-" + dayOfMonth + " " + hourOfDay + ":" + minute);
//                            }
//                        }, 0, 0, true).show();

                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
                        .show();
                break;
            case R.id.timeEnd:
                Calendar calendar2 = Calendar.getInstance();
                new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        timeEnd.setText(year + "-" + (month + 1) + "-" + dayOfMonth + " " + "23:59:59");
//                        new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
//                            @Override
//                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//                                timeEnd.setText(year + "-" + (month + 1) + "-" + dayOfMonth + " " + hourOfDay + ":" + minute);
//                            }
//                        }, 0, 0, true).show();
                    }
                }, calendar2.get(Calendar.YEAR), calendar2.get(Calendar.MONTH), calendar2.get(Calendar.DAY_OF_MONTH))
                        .show();
                break;
            case R.id.confirm:
                if (confirmListener != null) confirmListener.onConfirm();

                String batchNum, factoryNum, sensorName, beginTime, endTime;
                Boolean result = null;

                if (nameText.getText().toString().isEmpty() || nameText.getText().toString().equals("全部")) {
                    sensorName = null;
                } else {
                    sensorName = nameText.getText().toString();
                }

                if (batchNumText.getText().toString().isEmpty() || batchNumText.getText().toString().equals("全部")) {
                    batchNum = null;
                } else {
                    batchNum = batchNumText.getText().toString();
                }

                if (numText.getText().toString().isEmpty() || numText.getText().toString().equals("全部")) {
                    factoryNum = null;
                } else {
                    factoryNum = numText.getText().toString();
                }

                if (resultPosition == 0) {
                    result = null;
                } else if (resultPosition == 1) {
                    result = true;
                } else if (resultPosition == 2) {
                    result = false;
                }

                beginTime = timeStart.getText().toString().equals("开始时间") ? null : timeStart.getText().toString();
                endTime = timeEnd.getText().toString().equals("结束时间") ? null : timeEnd.getText().toString();

                onPopClickListener.onConfirmClick(batchNum, factoryNum, sensorName, result, beginTime, endTime);
                dismiss();
                break;
            case R.id.cancel:
                if (cancelListener != null) cancelListener.onCancel();
                dismiss();
                break;
            case R.id.nameLayout:
                onPopClickListener.onPopClick(v);
                break;
            case R.id.batchNumLayout:
                onPopClickListener.onPopClick(v, nameText.getText().toString());
                break;
            case R.id.numSelectImg:
                onPopClickListener.onPopClick(v, nameText.getText().toString(), batchNumText.getText().toString());
                break;
            case R.id.resultLayout:
                CommonListBottomPopup commonListBottomPopup3 = new CommonListBottomPopup(getContext(), resultList);
                new XPopup.Builder(getContext())
                        .moveUpToKeyboard(false) //如果不加这个，评论弹窗会移动到软键盘上面
                        .enableDrag(true)
                        .isDestroyOnDismiss(true)
                        .maxHeight(200)
                        .asCustom(commonListBottomPopup3)/*.enableDrag(false)*/
                        .show();
                commonListBottomPopup3.setListener(new CommonListBottomPopup.OnPopItemClickListener() {
                    @Override
                    public void onPopItemClick(View view, int position) {
                        resultPosition = position;
                        resultText.setText(resultList.get(position));
                        commonListBottomPopup3.dismiss();
                    }
                });
                break;
            default:
                break;
        }
    }

    //完全可见执行
    @Override
    protected void onShow() {
        super.onShow();
    }

    //完全消失执行
    @Override
    protected void onDismiss() {
    }

    @Override
    protected int getMaxHeight() {
//        return XPopupUtils.getWindowHeight(getContext());
        return (int) (XPopupUtils.getWindowHeight(getContext()) * .9f);
    }

}
