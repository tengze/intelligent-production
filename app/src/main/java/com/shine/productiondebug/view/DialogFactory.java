package com.shine.productiondebug.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.widget.TextView;

import com.shine.productiondebug.R;


public class DialogFactory {

    private static Dialog loadingDialog;

    public static void showLoading(Context context) {

        showMsgDialog(context, "loading");
    }

    public static void showMsgDialog(Context context, String title) {

        loadingDialog = new Dialog(context, R.style.dialog);
        loadingDialog.setContentView(R.layout.dialog_loading);
        loadingDialog.setCancelable(false);
        TextView messageTv = loadingDialog.findViewById(R.id.tv_message);
        messageTv.setText(title);

        loadingDialog.show();
    }


    public static Dialog showLoadingDialog(Activity activity) {
        Dialog loading = new Dialog(activity, R.style.dialog);
        loading.setContentView(R.layout.dialog_loading);
        loading.setCancelable(false);
        return loading;
    }

    public static void dissmiss() {

        if (loadingDialog != null && loadingDialog.isShowing()) {

            loadingDialog.dismiss();
        }
    }

}
