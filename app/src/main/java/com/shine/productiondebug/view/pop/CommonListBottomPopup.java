package com.shine.productiondebug.view.pop;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lxj.xpopup.core.BottomPopupView;
import com.lxj.xpopup.interfaces.OnCancelListener;
import com.lxj.xpopup.interfaces.OnConfirmListener;
import com.lxj.xpopup.util.XPopupUtils;
import com.shine.productiondebug.R;
import com.shine.productiondebug.ui.adapter.CommonListPopAdapter;
import com.shine.productiondebug.ui.adapter.SearchFragment1Adapter;

import java.util.ArrayList;
import java.util.List;

import cn.bingoogolapple.baseadapter.BGAOnRVItemClickListener;

public class CommonListBottomPopup extends BottomPopupView implements View.OnClickListener {
    OnConfirmListener confirmListener;
    OnCancelListener cancelListener;

    private List<String> mDataList = new ArrayList<>();


    public interface OnPopItemClickListener {
        void onPopItemClick(View view, int position);
    }

    private OnPopItemClickListener onPopItemClickListener;

    public CommonListBottomPopup(@NonNull Context context, List<String> dataList) {
        super(context);
        mDataList.addAll(dataList);
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_common_list;
    }

    @Override
    protected void onCreate() {
        super.onCreate();

        ImageView cancel = (ImageView) findViewById(R.id.cancel);
        cancel.setOnClickListener(this);

        RecyclerView recyclerView = findViewById(R.id.recyclerView);

        CommonListPopAdapter adapter = new CommonListPopAdapter(recyclerView);
        recyclerView.setAdapter(adapter);
        adapter.setData(mDataList);
        adapter.setOnRVItemClickListener(new BGAOnRVItemClickListener() {
            @Override
            public void onRVItemClick(ViewGroup parent, View itemView, int position) {
                onPopItemClickListener.onPopItemClick(itemView, position);
            }
        });
    }

    public CommonListBottomPopup setListener(OnPopItemClickListener onPopItemClickListener) {
        this.onPopItemClickListener = onPopItemClickListener;
        return this;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancel:
                dismiss();
                break;
        }
    }

    //完全可见执行
    @Override
    protected void onShow() {
        super.onShow();
    }

    //完全消失执行
    @Override
    protected void onDismiss() {
    }

    @Override
    protected int getMaxHeight() {
        return XPopupUtils.getWindowHeight(getContext());
//        return (int) (XPopupUtils.getWindowHeight(getContext()) * .9f);
    }

}
