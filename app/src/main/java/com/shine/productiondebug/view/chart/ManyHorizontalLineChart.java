package com.shine.productiondebug.view.chart;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.View;

import com.shine.productiondebug.utils.ScreenUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ManyHorizontalLineChart extends View {

    private Context mContext;
    private int viewWidth, viewHeight;
    private int leftMargin = 40;

    //底线 上边距
    private int topSpacing;
    //底线 下边距
    private int bottomSpacing;
    //底线 左边距
    private int leftSpacing;
    //底线 右边距
    private int rightSpacing;
    //竖间距
    private int verticalSpacing;
    //横间距
    private int horizontalSpacing;
    //柱状图宽度
    private int barWidth;
    //竖线偏移
    private int verticalLineDeviation = 10;

    private List<String> leftScale = new ArrayList<>();
    private List<Integer> bottomScale = new ArrayList<>();
    private List<Integer> data1List = new ArrayList<>();
    private List<Integer> data2List = new ArrayList<>();
    private List<Integer> data3List = new ArrayList<>();
    private List<Integer> data4List = new ArrayList<>();
    private int stageCount;
    private List<String> stageNameList = new ArrayList<>();
    private List<String> colorList = new ArrayList<>();


    public ManyHorizontalLineChart(Context context, int stageCount, List<String> leftScaleList, List<Integer> bottomScaleList,
                                   Map<Integer, List<Integer>> dataMap) {
        super(context);
        this.mContext = context;
        this.stageCount = stageCount;

        colorList.add("#FD7F4D");
        colorList.add("#1A93EE");
        colorList.add("#B936FE");
        colorList.add("#00D98B");

        leftScale.addAll(leftScaleList);
//        leftScale.add("04/07");
//        leftScale.add("04/06");
//        leftScale.add("04/05");
//        leftScale.add("04/04");
//        leftScale.add("04/03");
//        leftScale.add("04/02");
//        leftScale.add("04/01");

        bottomScale.addAll(bottomScaleList);
//        bottomScale.add(0);
//        bottomScale.add(100);
//        bottomScale.add(200);
//        bottomScale.add(300);
//        bottomScale.add(400);
//        bottomScale.add(500);

        int count = 0;
        for (Map.Entry<Integer, List<Integer>> m : dataMap.entrySet()) {
            stageNameList.add(m.getKey()+"号台");

            switch (count){
                case 0:
                    data1List.addAll(m.getValue());
                    break;
                case 1:
                    data2List.addAll(m.getValue());
                    break;
                case 2:
                    data3List.addAll(m.getValue());
                    break;
                case 3:
                    data4List.addAll(m.getValue());
                    break;
                default:
                    break;
            }

            count++;
        }

//        data1List.add(100);
//        data1List.add(80);
//        data1List.add(95);
//        data1List.add(70);
//        data1List.add(85);
//        data1List.add(78);
//        data1List.add(90);
//
//        data2List.add(80);
//        data2List.add(95);
//        data2List.add(78);
//        data2List.add(70);
//        data2List.add(90);
//        data2List.add(80);
//        data2List.add(85);
//
//        data3List.add(100);
//        data3List.add(80);
//        data3List.add(95);
//        data3List.add(70);
//        data3List.add(85);
//        data3List.add(78);
//        data3List.add(90);
//
//        data4List.add(80);
//        data4List.add(95);
//        data4List.add(78);
//        data4List.add(70);
//        data4List.add(90);
//        data4List.add(80);
//        data4List.add(85);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        viewWidth = ScreenUtils.getScreenWidth(mContext);
        viewHeight = viewWidth / 4 * 2;
        setMeasuredDimension(viewWidth, viewHeight);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        topSpacing = 100;//需要计算
        bottomSpacing = 80;//需要计算
        leftSpacing = 120;//需要计算
        rightSpacing = 80;//需要计算
        //间距
        verticalSpacing = (viewHeight - topSpacing - bottomSpacing) / (leftScale.size());
        horizontalSpacing = (viewWidth - leftMargin * 2 - leftSpacing - rightSpacing) / (bottomScale.size() - 1);

        barWidth = horizontalSpacing / 3;

        //绘制背景
        drawBg(canvas);
        //绘制柱状图
        drawLine(canvas);
        //绘制分类
        drawType(canvas);

    }

    /**
     * 绘制背景
     *
     * @param canvas
     */
    private void drawBg(Canvas canvas) {
        Paint bgPaint = new Paint();
        bgPaint.setStyle(Paint.Style.FILL);//充满
        bgPaint.setColor(Color.parseColor("#212A49"));
        bgPaint.setAntiAlias(true);// 设置画笔的锯齿效果
        RectF bgRectF = new RectF(leftMargin, 0, viewWidth - leftMargin, viewHeight);// 设置个新的长方形
        canvas.drawRoundRect(bgRectF, 30, 30, bgPaint);//第二个参数是x半径，第三个参数是y半径

        Paint linePaint = new Paint();
        linePaint.setColor(Color.parseColor("#424E5D"));

        Paint scaleTextPaint = new Paint();
        scaleTextPaint.setColor(Color.WHITE);
        Rect scaleTextRect = new Rect();
        scaleTextPaint.setTextSize(30);


        for (int i = 0; i < leftScale.size() + 1; i++) {
            //横底线
            if (i == 0 || i == leftScale.size()) {
                canvas.drawLine(leftMargin + leftSpacing, topSpacing + i * verticalSpacing, leftMargin + leftSpacing + verticalLineDeviation, topSpacing + i * verticalSpacing, linePaint);
            } else {
                canvas.drawLine(leftMargin + leftSpacing, topSpacing + i * verticalSpacing, viewWidth - leftMargin - rightSpacing, topSpacing + i * verticalSpacing, linePaint);
            }
        }

        for (int i = 0; i < leftScale.size(); i++) {
            //左刻度
            String mText = leftScale.get(i);
            scaleTextPaint.getTextBounds(mText, 0, mText.length(), scaleTextRect);
            canvas.drawText(mText, leftMargin + leftSpacing - scaleTextRect.width() - 20, topSpacing + i * verticalSpacing + verticalSpacing / 2 + scaleTextRect.height() / 2, scaleTextPaint);
        }

        //竖底线
        canvas.drawLine(leftMargin + leftSpacing + verticalLineDeviation, topSpacing, leftMargin + leftSpacing + verticalLineDeviation, topSpacing + leftScale.size() * verticalSpacing, linePaint);

        for (int i = 0; i < bottomScale.size(); i++) {
            //刻度
            String mText = bottomScale.get(i) + "";
            scaleTextPaint.getTextBounds(mText, 0, mText.length(), scaleTextRect);
            canvas.drawText(mText, leftMargin + leftSpacing + verticalLineDeviation + i * horizontalSpacing - scaleTextRect.width() / 2, topSpacing + (leftScale.size()) * verticalSpacing + scaleTextRect.height() + 20, scaleTextPaint);
        }

    }

    private void drawLine(Canvas canvas) {
        Paint bar1Paint = new Paint();
        bar1Paint.setColor(Color.parseColor(colorList.get(0)));
        bar1Paint.setStyle(Paint.Style.FILL);

        Paint bar2Paint = new Paint();
        bar2Paint.setColor(Color.parseColor(colorList.get(1)));
        bar2Paint.setStyle(Paint.Style.FILL);

        Paint bar3Paint = new Paint();
        bar3Paint.setColor(Color.parseColor(colorList.get(2)));
        bar3Paint.setStyle(Paint.Style.FILL);

        Paint bar4Paint = new Paint();
        bar4Paint.setColor(Color.parseColor(colorList.get(3)));
        bar4Paint.setStyle(Paint.Style.FILL);

        Paint textPaint = new Paint();
        textPaint.setColor(Color.WHITE);
        Rect textRect = new Rect();
        textPaint.setTextSize(25);
        String mText = "";

        for (int i = 0; i < data1List.size(); i++) {
            if (stageCount == 0)
                return;
            //数据1柱状图
            int width1 = data1List.get(i) * (viewWidth - (leftMargin + rightSpacing) * 2) / bottomScale.get(bottomScale.size() - 1);
            canvas.drawRect(leftMargin + leftSpacing + verticalLineDeviation,
                    topSpacing + i * verticalSpacing + 5,
                    leftMargin + leftSpacing + verticalLineDeviation + width1,
                    topSpacing + (i + 1) * verticalSpacing - 5, bar1Paint);
            //数据1数据
            mText = data1List.get(i) + "";
            textPaint.getTextBounds(mText, 0, mText.length(), textRect);
            canvas.drawText(mText, leftMargin + leftSpacing + verticalLineDeviation + width1 / 2 - textRect.width() / 2,
                    topSpacing + i * verticalSpacing + verticalSpacing / 2 + textRect.height() / 2, textPaint);

            if (stageCount == 1)
                continue;
            //数据2柱状图
            int width2 = data2List.get(i) * (viewWidth - (leftMargin + rightSpacing) * 2) / bottomScale.get(bottomScale.size() - 1);
            canvas.drawRect(leftMargin + leftSpacing + verticalLineDeviation + width1,
                    topSpacing + i * verticalSpacing + 5,
                    leftMargin + leftSpacing + verticalLineDeviation + width1 + width2,
                    topSpacing + (i + 1) * verticalSpacing - 5, bar2Paint);

            //数据2数据
            mText = data2List.get(i) + "";
            textPaint.getTextBounds(mText, 0, mText.length(), textRect);
            canvas.drawText(mText, leftMargin + leftSpacing + verticalLineDeviation + width1 + width2 / 2 - textRect.width() / 2,
                    topSpacing + i * verticalSpacing + verticalSpacing / 2 + textRect.height() / 2, textPaint);

            if (stageCount == 2)
                continue;
            //数据3柱状图
            int width3 = data3List.get(i) * (viewWidth - (leftMargin + rightSpacing) * 2) / bottomScale.get(bottomScale.size() - 1);
            canvas.drawRect(leftMargin + leftSpacing + verticalLineDeviation + width1 + width2,
                    topSpacing + i * verticalSpacing + 5,
                    leftMargin + leftSpacing + verticalLineDeviation + width1 + width2 + width3,
                    topSpacing + (i + 1) * verticalSpacing - 5, bar3Paint);

            //数据3数据
            mText = data3List.get(i) + "";
            textPaint.getTextBounds(mText, 0, mText.length(), textRect);
            canvas.drawText(mText, leftMargin + leftSpacing + verticalLineDeviation + width1 + width2 + width3 / 2 - textRect.width() / 2,
                    topSpacing + i * verticalSpacing + verticalSpacing / 2 + textRect.height() / 2, textPaint);

            if (stageCount == 3)
                continue;
            //数据4柱状图
            int width4 = data4List.get(i) * (viewWidth - (leftMargin + rightSpacing) * 2) / bottomScale.get(bottomScale.size() - 1);
            canvas.drawRect(leftMargin + leftSpacing + verticalLineDeviation + width1 + width2 + width3,
                    topSpacing + i * verticalSpacing + 5,
                    leftMargin + leftSpacing + verticalLineDeviation + width1 + width2 + width3 + width4,
                    topSpacing + (i + 1) * verticalSpacing - 5, bar4Paint);

            //数据4数据
            mText = data4List.get(i) + "";
            textPaint.getTextBounds(mText, 0, mText.length(), textRect);
            canvas.drawText(mText, leftMargin + leftSpacing + verticalLineDeviation + width1 + width2 + width3 + width4 / 2 - textRect.width() / 2,
                    topSpacing + i * verticalSpacing + verticalSpacing / 2 + textRect.height() / 2, textPaint);
        }

    }

    private void drawType(Canvas canvas) {
        //色块和文字间距
        int textSpacing = 15;
        //两个类别的间距
        int typeSpacing = 50;
        //色块宽度
        int colorWidth = 40;

        int rightPosition = viewWidth - leftMargin - rightSpacing;
        int y = topSpacing / 2;

        Paint textPaint = new Paint();
        textPaint.setColor(Color.WHITE);// 设置红色
        textPaint.setTextSize(40);

        Paint squarePaint = new Paint();
        squarePaint.setStyle(Paint.Style.FILL);//设置填满
        Rect textRect = new Rect();
        String mText = "";


        if (stageCount == 1) {
            mText = stageNameList.get(0);
            textPaint.getTextBounds(mText, 0, mText.length(), textRect);
            canvas.drawText(mText, rightPosition - textRect.width(), y + textRect.height() / 2, textPaint);

            squarePaint.setColor(Color.parseColor(colorList.get(0)));
            canvas.drawRect(rightPosition - textRect.width() - textSpacing - colorWidth,
                    y - textRect.height() / 2 + 10,
                    rightPosition - textRect.width() - textSpacing,
                    y + textRect.height() / 2, squarePaint);
        }

        if (stageCount == 2) {
            mText = stageNameList.get(1);
            textPaint.getTextBounds(mText, 0, mText.length(), textRect);
            canvas.drawText(mText, rightPosition - textRect.width(), y + textRect.height() / 2, textPaint);

            squarePaint.setColor(Color.parseColor(colorList.get(1)));
            canvas.drawRect(rightPosition - textRect.width() - textSpacing - colorWidth,
                    y - textRect.height() / 2 + 10,
                    rightPosition - textRect.width() - textSpacing,
                    y + textRect.height() / 2, squarePaint);


            rightPosition = rightPosition - textRect.width() - textSpacing - colorWidth - typeSpacing;
            mText = stageNameList.get(0);
            textPaint.getTextBounds(mText, 0, mText.length(), textRect);
            canvas.drawText(mText, rightPosition - textRect.width(), y + textRect.height() / 2, textPaint);

            squarePaint.setColor(Color.parseColor(colorList.get(0)));
            canvas.drawRect(rightPosition - textRect.width() - textSpacing - colorWidth,
                    y - textRect.height() / 2 + 10,
                    rightPosition - textRect.width() - textSpacing,
                    y + textRect.height() / 2, squarePaint);
        }

        if (stageCount == 3) {
            mText = stageNameList.get(2);
            textPaint.getTextBounds(mText, 0, mText.length(), textRect);
            canvas.drawText(mText, rightPosition - textRect.width(), y + textRect.height() / 2, textPaint);

            squarePaint.setColor(Color.parseColor(colorList.get(2)));
            canvas.drawRect(rightPosition - textRect.width() - textSpacing - colorWidth,
                    y - textRect.height() / 2 + 10,
                    rightPosition - textRect.width() - textSpacing,
                    y + textRect.height() / 2, squarePaint);


            rightPosition = rightPosition - textRect.width() - textSpacing - colorWidth - typeSpacing;
            mText = stageNameList.get(1);
            textPaint.getTextBounds(mText, 0, mText.length(), textRect);
            canvas.drawText(mText, rightPosition - textRect.width(), y + textRect.height() / 2, textPaint);

            squarePaint.setColor(Color.parseColor(colorList.get(1)));
            canvas.drawRect(rightPosition - textRect.width() - textSpacing - colorWidth,
                    y - textRect.height() / 2 + 10,
                    rightPosition - textRect.width() - textSpacing,
                    y + textRect.height() / 2, squarePaint);


            rightPosition = rightPosition - textRect.width() - textSpacing - colorWidth - typeSpacing;
            mText = stageNameList.get(0);
            textPaint.getTextBounds(mText, 0, mText.length(), textRect);
            canvas.drawText(mText, rightPosition - textRect.width(), y + textRect.height() / 2, textPaint);

            squarePaint.setColor(Color.parseColor(colorList.get(0)));
            canvas.drawRect(rightPosition - textRect.width() - textSpacing - colorWidth,
                    y - textRect.height() / 2 + 10,
                    rightPosition - textRect.width() - textSpacing,
                    y + textRect.height() / 2, squarePaint);


        }

        if (stageCount == 4) {
            mText = stageNameList.get(3);
            textPaint.getTextBounds(mText, 0, mText.length(), textRect);
            canvas.drawText(mText, rightPosition - textRect.width(), y + textRect.height() / 2, textPaint);

            squarePaint.setColor(Color.parseColor(colorList.get(3)));
            canvas.drawRect(rightPosition - textRect.width() - textSpacing - colorWidth,
                    y - textRect.height() / 2 + 10,
                    rightPosition - textRect.width() - textSpacing,
                    y + textRect.height() / 2, squarePaint);


            rightPosition = rightPosition - textRect.width() - textSpacing - colorWidth - typeSpacing;
            mText = stageNameList.get(2);
            textPaint.getTextBounds(mText, 0, mText.length(), textRect);
            canvas.drawText(mText, rightPosition - textRect.width(), y + textRect.height() / 2, textPaint);

            squarePaint.setColor(Color.parseColor(colorList.get(2)));
            canvas.drawRect(rightPosition - textRect.width() - textSpacing - colorWidth,
                    y - textRect.height() / 2 + 10,
                    rightPosition - textRect.width() - textSpacing,
                    y + textRect.height() / 2, squarePaint);


            rightPosition = rightPosition - textRect.width() - textSpacing - colorWidth - typeSpacing;
            mText = stageNameList.get(1);
            textPaint.getTextBounds(mText, 0, mText.length(), textRect);
            canvas.drawText(mText, rightPosition - textRect.width(), y + textRect.height() / 2, textPaint);

            squarePaint.setColor(Color.parseColor(colorList.get(1)));
            canvas.drawRect(rightPosition - textRect.width() - textSpacing - colorWidth,
                    y - textRect.height() / 2 + 10,
                    rightPosition - textRect.width() - textSpacing,
                    y + textRect.height() / 2, squarePaint);


            rightPosition = rightPosition - textRect.width() - textSpacing - colorWidth - typeSpacing;
            mText = stageNameList.get(0);
            textPaint.getTextBounds(mText, 0, mText.length(), textRect);
            canvas.drawText(mText, rightPosition - textRect.width(), y + textRect.height() / 2, textPaint);

            squarePaint.setColor(Color.parseColor(colorList.get(0)));
            canvas.drawRect(rightPosition - textRect.width() - textSpacing - colorWidth,
                    y - textRect.height() / 2 + 10,
                    rightPosition - textRect.width() - textSpacing,
                    y + textRect.height() / 2, squarePaint);
        }

    }

}
