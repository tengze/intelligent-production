package com.shine.productiondebug.view.pop;

import android.content.Context;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.lxj.xpopup.core.PositionPopupView;
import com.shine.productiondebug.R;

/**
 * Description: 自定义自由定位Position弹窗
 * Create by dance, at 2019/6/14
 */
public class NetStatusPopup extends PositionPopupView {
    private boolean netAvailable;
    public NetStatusPopup(@NonNull Context context, boolean netAvailable) {
        super(context);
        this.netAvailable = netAvailable;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_net_status;
    }

    @Override
    protected void onCreate() {
        super.onCreate();

        TextView popText = (TextView) findViewById(R.id.popText);
        if (netAvailable)
            popText.setText("网络已连接");
        else
            popText.setText("网络已断开");
    }
}
