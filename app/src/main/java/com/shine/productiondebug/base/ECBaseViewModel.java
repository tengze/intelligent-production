package com.shine.productiondebug.base;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;
import androidx.databinding.ObservableList;
import androidx.fragment.app.Fragment;

import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.shine.productiondebug.R;
import com.shine.productiondebug.app.Constants;
import com.shine.productiondebug.http.ApiException;
import com.shine.productiondebug.utils.ToastUtils;


/**
 * Copyright (C), 2015-2018
 * FileName: ECBaseViewModel
 * Author: Jesse
 * Date: 2018/9/5 15:59
 * Description: ${DESCRIPTION}
 * ======================Version: 1.0 ======================
 * 新增根布局loading页面、空页面及加载错误页面，之前onErrorObservable和onEmptyObservable弃用
 * 使用方法：{@link ErrorModel}
 * 1、xml中layout中添加
 * <variable
 * name="error"
 * type="com.empire.manyipay.model.ErrorModel"/>
 * <p>
 * 2、xml需要显示部分添加
 * <include
 * layout="@layout/layout_error_content"
 * binding:error="@{viewModel.error}"/>
 * 3、根据业务调用方法
 * showLoadingLayout、showEmpty、showContent、showError
 * ========================Version: 1.1=======================
 */
public class ECBaseViewModel extends BaseViewModel {

    public ObservableField<String> onErrorObservable = new ObservableField<>();

    public ObservableInt onEmptyObservable = new ObservableInt(View.GONE);

    public ObservableBoolean loadingObservable = new ObservableBoolean(true);

    public ObservableBoolean hasMoreObservable = new ObservableBoolean(true);

    public ObservableField<ErrorModel> error = new ObservableField<>(new ErrorModel(ErrorModel.TYPE_LOADING));

    private boolean mIsServiceBound;

    protected BasePopupView popupView;

    public ECBaseViewModel(Context context) {
        super(context);
    }

    public ECBaseViewModel(Fragment fragment) {
        super(fragment);
    }

    private Dialog loadingDialog;

    public void showLoading() {
        showLoading("Please Wait...");
    }

    public void showLoading(String title) {
//        if (loadingDialog == null) {
//
//            loadingDialog = new Dialog(context, R.style.dialog);
//            loadingDialog.setContentView(R.layout.dialog_loading);
//            loadingDialog.setCancelable(false);
//        }
//        TextView messageTv = loadingDialog.findViewById(R.id.tv_message);
//        messageTv.setText(title);
//        loadingDialog.show();

        if(popupView == null)
            popupView = new XPopup.Builder(context).hasShadowBg(true).dismissOnTouchOutside(false).asLoading().show();

        else popupView.show();
    }

    public void dismissDialog() {
//        if (loadingDialog != null && loadingDialog.isShowing()) {
//            loadingDialog.dismiss();
//        }

        if(popupView != null){
            popupView.dismiss();
        }
    }

    public Context getContext() {
        return context;
    }

    public void showLoadingLayout() {

        loadingObservable.set(true);
        hasMoreObservable.set(true);
    }

    public void showEmpty() {

        loadingObservable.set(false);
        error.set(new ErrorModel(ErrorModel.TYPE_EMPTY));
    }

    public void showContent() {

        loadingObservable.set(false);
        hasMoreObservable.set(true);
        error.set(new ErrorModel(ErrorModel.TYPE_CONTENT));
    }

    public void showContentWithNomore() {

        loadingObservable.set(false);
        hasMoreObservable.set(false);
        error.set(new ErrorModel(ErrorModel.TYPE_CONTENT));
    }

    public void showError() {

        loadingObservable.set(false);
        error.set(new ErrorModel(ErrorModel.TYPE_ERROR));
    }

    public void showError(ApiException ex) {

        loadingObservable.set(false);
        error.set(new ErrorModel(ErrorModel.TYPE_ERROR));
        ToastUtils.showShort(ex.message);
    }

    public void showError(Throwable ex) {

        if(ex instanceof ApiException) {
            showError((ApiException)ex);
        } else {
            ToastUtils.showShort(ex.getMessage());
        }
        dismissDialog();
    }

    public void showLoading(int progress) {

        loadingObservable.set(true);
        ErrorModel errorModel = new ErrorModel(ErrorModel.TYPE_LOADING);
        errorModel.setShowLoading(true);
        errorModel.setProgress(progress);
        error.set(errorModel);
    }

    public void showCalculateContent(ObservableList observableList) {

        if (observableList.isEmpty()) {

            showEmpty();
        } else if (observableList.size() < Constants.PAGE_SIZE) {

            showContentWithNomore();
        } else {
            showContent();
        }
    }

    public void showCalculateContent(ObservableList observableList, int page) {

        if (observableList.isEmpty() && page == 1) {

            showEmpty();
        } else if (observableList.size() < Constants.PAGE_SIZE) {

            showContentWithNomore();
        } else {
            showContent();
        }
    }

    public void showCalculateContent(ObservableList observableList, int page, int pageSize) {

        if (observableList.isEmpty()) {

            showEmpty();
        } else if (observableList.size() < pageSize * page) {

            showContentWithNomore();
        } else {
            showContent();
        }
    }

    public void ToastCopyMessage(String titles) {
        //LayoutInflater的作用：对于一个没有被载入或者想要动态载入的界面，都需要LayoutInflater.inflate()来载入，LayoutInflater是用来找res/layout/下的xml布局文件，并且实例化
        View view = LayoutInflater.from(context).inflate(R.layout.copy_right, null); //加載layout下的布局
        TextView title = view.findViewById(R.id.tvTitleToast);
        title.setText(titles); //toast内容
        Toast toast = new Toast(context);
        toast.setGravity(Gravity.CENTER, 12, 20);//setGravity用来设置Toast显示的位置，相当于xml中的android:gravity或android:layout_gravity
        toast.setDuration(Toast.LENGTH_SHORT);//setDuration方法：设置持续时间，以毫秒为单位。该方法是设置补间动画时间长度的主要方法
        toast.setView(view); //添加视图文件
        toast.show();
//        ToastUtils.showShort(titles);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
