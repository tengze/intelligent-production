package com.shine.productiondebug.base;

/**
 * Created by Jesse on 2018/12/29 0029.
 * Version 1.0
 */
public class AppStatusManager {


    public int appStatus = AppStatus.STATUS_RECYVLE;    //APP状态 初始值为不在前台状态

    public static AppStatusManager appStatusManager;

    private AppStatusManager(){}

    //单例模式
    public static AppStatusManager getInstance() {
        if (appStatusManager == null) {
            appStatusManager = new AppStatusManager();
        }
        return appStatusManager;
    }

    public int getAppStatus() {
        return appStatus;
    }

    public void setAppStatus(int appStatus) {
        this.appStatus = appStatus;
    }
}
