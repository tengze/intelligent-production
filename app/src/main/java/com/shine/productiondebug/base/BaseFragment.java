package com.shine.productiondebug.base;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.shine.productiondebug.R;
import com.shine.productiondebug.utils.ToastUtils;
import com.shine.productiondebug.view.DialogFactory;


/**
 * Copyright (C), 2015-2018
 * FileName: ECBaseFragment
 * Author: Jesse
 * Date: 2018/9/7 12:12
 * Description: ${DESCRIPTION}
 * Version: 1.0
 */
public abstract class BaseFragment<V extends ViewDataBinding, VM extends BaseViewModel> extends BGASwipeBackBaseFragment implements IBaseActivity, View.OnClickListener {

    protected V binding;
    protected VM viewModel;
    private static final String TAG = "BaseFragment";
    private Dialog loadDialog;
    private TextView loadingmsg;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initParam();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Log.d(TAG, getClass().getName() + "fragment: onCreateView");
        binding = DataBindingUtil.inflate(inflater, initContentView(inflater, container, savedInstanceState), container, false);
        binding.setVariable(initVariableId(), viewModel = initViewModel());
        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        isViewCreated = true;
        lazyLoad();

        initData();

        initViewObservable();

        if (viewModel != null) {

            viewModel.onCreate();

            viewModel.registerRxBus();
        }

    }

    @Override
    public void initParam() {

    }

    //刷新布局
    public void refreshLayout() {
        if (viewModel != null) {
            binding.setVariable(initVariableId(), viewModel);
        }
    }

    /**
     * 初始化根布局
     *
     * @return 布局layout的id
     */
    public abstract int initContentView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState);

    /**
     * 初始化ViewModel的id
     *
     * @return BR的id
     */
    public abstract int initVariableId();

    /**
     * 初始化ViewModel
     *
     * @return 继承BaseViewModel的ViewModel
     */
    public abstract VM initViewModel();

    @Override
    public void initData() {

    }

    @Override
    public void initViewObservable() {

    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (viewModel != null) {
            viewModel.removeRxBus();
            viewModel.onDestroy();
            viewModel = null;
            binding.unbind();
        }
    }

    public void startActivity(Class cls) {

        Intent intent = new Intent(getContext(), cls);
        startActivity(intent);
    }

    public void startActivity(Class cls, Intent intent) {

        intent.setClass(getContext(), cls);
        startActivity(intent);
    }

    public void ToastMessage(String titles) {
        //LayoutInflater的作用：对于一个没有被载入或者想要动态载入的界面，都需要LayoutInflater.inflate()来载入，LayoutInflater是用来找res/layout/下的xml布局文件，并且实例化
//        LayoutInflater inflater = getLayoutInflater();//调用Activity的getLayoutInflater()
////        View view = inflater.inflate(R.layout.toast_layout, null); //加載layout下的布局
////        TextView title = view.findViewById(R.id.tvTitleToast);
////        title.setText(titles); //toast内容
////        Toast toast = new Toast(getActivity());
////        toast.setGravity(Gravity.CENTER, 12, 20);//setGravity用来设置Toast显示的位置，相当于xml中的android:gravity或android:layout_gravity
////        toast.setDuration(Toast.LENGTH_SHORT);//setDuration方法：设置持续时间，以毫秒为单位。该方法是设置补间动画时间长度的主要方法
////        toast.setView(view); //添加视图文件
////        toast.show();

        ToastUtils.showShort(titles);
    }

    public void showLoading(String s) {
        loadDialog = DialogFactory.showLoadingDialog(getActivity());
        loadingmsg = loadDialog.findViewById(R.id.tv_message);
        loadingmsg.setText(s);
        loadDialog.show();
    }

    public void changeMsg(String s) {
        if (loadingmsg != null) {
            loadingmsg.setText(s);
        }

    }

    public void dismissLoading() {
        if (loadDialog != null) {
            loadDialog.dismiss();
        }
    }


    public void onRefresh() {
    }

    //Fragment的View加载完毕的标记
    private boolean isViewCreated;

    //Fragment对用户可见的标记
    private boolean isUIVisible;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        //isVisibleToUser这个boolean值表示:该Fragment的UI 用户是否可见

        if (isVisibleToUser) {
            isUIVisible = true;
            lazyLoad();
        } else {
            isUIVisible = false;
        }
    }

    private void lazyLoad() {
        //这里进行双重标记判断,是因为setUserVisibleHint会多次回调,并且会在onCreateView执行前回调,必须确保onCreateView加载完毕且页面可见,才加载数据
        if (isViewCreated && isUIVisible) {
            loadDataLazy();
            //数据加载完毕,恢复标记,防止重复加载
            isViewCreated = false;
            isUIVisible = false;

        }
    }

    protected void loadDataLazy() {
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    protected void switchActivity(Class<?> cls) {
        switchActivity(cls, null);
    }

    protected void switchActivity(Class<?> cls, Bundle extras) {
        Intent intent = new Intent(getActivity(), cls);
        if (extras != null) {
            intent.putExtras(extras);
        }
        startActivity(intent);
    }

    protected void switchActivity(Class<?> cls, Bundle extras, int flags) {
        Intent intent = new Intent(getActivity(), cls);
        if (extras != null) {
            intent.putExtras(extras);
        }
        intent.setFlags(flags);
        startActivity(intent);
    }

    protected void switchActivityForResult(Class<?> cls, int requestCode, Bundle extras) {
        Intent intent = new Intent(getActivity(), cls);
        if (extras != null) {
            intent.putExtras(extras);
        }
        startActivityForResult(intent, requestCode);
    }

    protected void switchActivityForResult(Class<?> cls, int requestCode,
                                           Bundle extras, int flags) {
        Intent intent = new Intent(getActivity(), cls);
        if (extras != null) {
            intent.putExtras(extras);
        }
        intent.setFlags(flags);
        startActivityForResult(intent, requestCode);

    }

    protected void switchActivityReorder2Front(Class<?> cls) {
        Intent intent = new Intent(getActivity(), cls);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }

    public void setText(TextView textView, String text) {
        textView.setText(text == null ? "" : text);
    }

    public void setText(TextView textView, String text, String data) {
        textView.setText(text == null ? data : text);
    }
}
