package com.shine.productiondebug.base;

import android.graphics.drawable.Drawable;
import android.view.View;

import androidx.core.content.ContextCompat;

import com.shine.productiondebug.R;
import com.shine.productiondebug.app.BaseApplication;


/**
 * Created by Jesse on 2018/11/10 0010.
 * Version 1.0
 */
public class ErrorModel {

    //加载
    public final static int TYPE_LOADING = 1;

    //空页面
    public final static int TYPE_EMPTY = 2;

    //错误页面
    public final static int TYPE_ERROR = 3;

    //显示数据页面
    public final static int TYPE_CONTENT = 4;

    private int type;

    private boolean showLoading;

    private int progress;

    public ErrorModel(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int loading(){

        return showLoading ? View.VISIBLE : View.GONE;
    }

    public int visible() {

        return type < TYPE_CONTENT ? View.VISIBLE : View.GONE;
    }

    public boolean isShowLoading() {
        return showLoading;
    }

    public void setShowLoading(boolean showLoading) {
        this.showLoading = showLoading;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public Drawable errorImage() {

        int icon = R.mipmap.nouser;

        switch (type) {

            case TYPE_LOADING:
                icon = R.mipmap.loading;
                break;
            case TYPE_EMPTY:
                icon = R.mipmap.nodate;
                break;
            case TYPE_ERROR:
//                icon = R.mipmap.nouser;
                break;
        }
        return ContextCompat.getDrawable(BaseApplication.getContext(), icon);
    }

}
