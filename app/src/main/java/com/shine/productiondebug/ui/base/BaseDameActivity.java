package com.shine.productiondebug.ui.base;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.shine.productiondebug.BR;
import com.shine.productiondebug.R;
import com.shine.productiondebug.app.Constants;
import com.shine.productiondebug.base.BaseActivity;
import com.shine.productiondebug.databinding.ActivityBaseBinding;

public class BaseDameActivity extends BaseActivity<ActivityBaseBinding, BaseDameViewModel> {


    @Override
    public int initContentView(Bundle savedInstanceState) {
        return R.layout.activity_base;
    }

    @Override
    public int initVariableId() {
        return BR.viewModel;
    }

    @Override
    public BaseDameViewModel initViewModel() {
        return new BaseDameViewModel(this);
    }

    @Override
    public int appTheme() {
        return Constants.THEME_FULL;
    }

    @Override
    public void initData() {
        super.initData();
        setStatusBar();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            default:
                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                default:
                    break;
            }
        }
    }
}
