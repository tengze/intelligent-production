package com.shine.productiondebug.ui.login;

import android.Manifest;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;

import com.shine.productiondebug.R;
import com.shine.productiondebug.app.Constants;
import com.shine.productiondebug.base.BaseActivity;
import com.shine.productiondebug.bean.LoginBean;
import com.shine.productiondebug.databinding.ActivityChangePasswordBinding;
import com.shine.productiondebug.databinding.ActivityLoginNewBinding;
import com.shine.productiondebug.http.ApiException;
import com.shine.productiondebug.http.ECHttpResponse;
import com.shine.productiondebug.http.ECObserver;
import com.shine.productiondebug.http.RetrofitClient;
import com.shine.productiondebug.http.RxUtils;
import com.shine.productiondebug.http.Service;
import com.shine.productiondebug.ui.main.MainActivity;
import com.shine.productiondebug.utils.SPUtils;
import com.shine.productiondebug.utils.SystemUtil;
import com.tbruyelle.rxpermissions2.RxPermissions;

import io.reactivex.functions.Consumer;

public class ChangePasswordActivity extends BaseActivity<ActivityChangePasswordBinding, ChangePasswordViewModel> {

    @Override
    public int initContentView(Bundle savedInstanceState) {
        return R.layout.activity_change_password;
    }

    @Override
    public int initVariableId() {
        return com.shine.productiondebug.BR.viewModel;
    }

    @Override
    public ChangePasswordViewModel initViewModel() {
        return new ChangePasswordViewModel(this);
    }

    @SuppressLint("CheckResult")
    @Override
    public void initData() {
        super.initData();
        binding.login.setOnClickListener(this);

        RxPermissions mRxPermission = new RxPermissions(this);
        mRxPermission.request(Manifest.permission.READ_PHONE_STATE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .subscribe(new Consumer<Boolean>() {
                    @Override
                    public void accept(Boolean aBoolean) throws Exception {
                        if (aBoolean) {

                        }
                    }
                });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login:
                if (binding.userName.getText().toString().isEmpty()) {
                    ToastMessage("请输入密码");
                    return;
                }

                if (binding.passWord.getText().toString().isEmpty()) {
                    ToastMessage("请再次输入密码");
                    return;
                }

//                SPUtils.getInstance().put(Constants.SP_LOGIN_NAME, binding.userName.getText().toString());
                SPUtils.getInstance().put(Constants.SP_LOGIN_PASS, binding.passWord.getText().toString());

                finish();
//                loginByPassword();
                break;
            default:
                break;
        }
    }

    private void loginByPassword() {
        showLoading("");
        RetrofitClient.getInstance().create(Service.class)
                .loginpassword("android", "", 0, 1, binding.userName.getText().toString(), binding.passWord.getText().toString(), SystemUtil.getDeviceBrand() + SystemUtil.getSystemModel(), 0)
                .compose(RxUtils.<ECHttpResponse<LoginBean>>bindToLifecycle(this))
                .compose(RxUtils.<ECHttpResponse<LoginBean>>schedulersTransformer())
                .compose(RxUtils.<ECHttpResponse<LoginBean>>exceptionTransformer())
                .subscribe(new ECObserver<LoginBean>() {
                    @Override
                    protected void _onNext(LoginBean loginBean) {
//                        dismissLoading();
                        SPUtils.getInstance().put(Constants.SP_LOGIN_NAME, binding.userName.getText().toString());
                        SPUtils.getInstance().put(Constants.SP_LOGIN_PASS, binding.passWord.getText().toString());
                        loginSuccess(loginBean);
                    }

                    @Override
                    protected void _onError(ApiException ex) {
                        ToastMessage(ex.message);
                        dismissLoading();
                    }
                });
    }


    private void loginSuccess(LoginBean loginBean) {
//        if (isRememberPassword) {
//            SPUtils.getInstance().put(Constants.SP_IS_REMEMBER_PASSWORD, true);
//        } else {
//            SPUtils.getInstance().put(Constants.SP_IS_REMEMBER_PASSWORD, false);
//        }
//        SPUtils.getInstance().put(Constants.SP_TOKEN, loginBean.getToken());

//        LoginBeanCookie loginBeanCookie = new LoginBeanCookie();
//        loginBeanCookie.setId(loginBean.getId());
//        loginBeanCookie.setMobile(loginBean.getMobile());
//        loginBeanCookie.setVipLevel(loginBean.getVipLevel());
//        SPUtils.getInstance().put(Constants.SP_COOKIE, new Gson().toJson(loginBeanCookie));

        loginMe();
    }

    private void loginMe() {
//        showLoading("");
//        RetrofitClient.getInstance().create(Service.class)
//                .loginMe()
//                .compose(RxUtils.<ECHttpResponse<MeBean>>bindToLifecycle(this))
//                .compose(RxUtils.<ECHttpResponse<MeBean>>schedulersTransformer())
//                .compose(RxUtils.<ECHttpResponse<MeBean>>exceptionTransformer())
//                .subscribe(new ECObserver<MeBean>() {
//                    @Override
//                    protected void _onNext(MeBean meBean) {
//                        dismissLoading();
//
//                        SPUtils.getInstance().put(Constants.SP_ME, new Gson().toJson(meBean));
//                        MeHelper.updateMe();
//                        RxBus.getDefault().post(meBean);
//
//                        if (MeHelper.getInstance().isVip() || (meBean.getIsCompleteBcc() == 1 && meBean.getIsIdCheck() == 1)) {
//                            MainActivity.start(LoginActivity.this, 0);
//                        } else {
//                            MainOrdinaryActivity.start(LoginActivity.this, 0);
//                        }
//                        ToastMessage("登录成功！");
//                        finish();
//                    }
//
//                    @Override
//                    protected void _onError(ApiException ex) {
//                        dismissLoading();
//                        ToastUtils.showShort(ex.message);
//                    }
//                });
    }

}
