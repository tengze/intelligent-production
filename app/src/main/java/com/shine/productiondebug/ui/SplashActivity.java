package com.shine.productiondebug.ui;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;

import com.shine.productiondebug.BR;
import com.shine.productiondebug.R;
import com.shine.productiondebug.app.Constants;
import com.shine.productiondebug.base.BaseActivity;
import com.shine.productiondebug.databinding.ActivitySplashBinding;
import com.shine.productiondebug.ui.login.LoginNewActivity;
import com.shine.productiondebug.ui.video.VideoSurveillanceActivity;
import com.shine.productiondebug.ui.viewModel.SplashActivityViewModel;


public class SplashActivity extends BaseActivity<ActivitySplashBinding, SplashActivityViewModel> {
    private long countDownTime = 1000;
    private CountDownTimer countDownTimer;

    @Override
    public int initContentView(Bundle savedInstanceState) {
        return R.layout.activity_splash;
    }

    @Override
    public int initVariableId() {
        return BR.viewModel;
    }

    @Override
    public SplashActivityViewModel initViewModel() {
        return new SplashActivityViewModel(this);
    }

    @Override
    public int appTheme() {
        return Constants.THEME_FULL;
    }

    @Override
    public void initData() {
        super.initData();
        setStatusBar();

        setCountDownTime(true);
        countDownTimer.start();


    }

    @Override
    public void onClick(View view) {

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        setCountDownTime(true);
        countDownTimer.start();
    }

    private void setCountDownTime(boolean isFirst) {
        countDownTimer = new CountDownTimer(countDownTime, 1000) {
            @Override
            public void onTick(long l) {
            }

            @Override
            public void onFinish() {
                switchActivityAndFinish(LoginNewActivity.class);
            }
        };
    }

}
