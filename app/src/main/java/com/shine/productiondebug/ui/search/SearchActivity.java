package com.shine.productiondebug.ui.search;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.shine.productiondebug.BR;
import com.shine.productiondebug.R;
import com.shine.productiondebug.app.Constants;
import com.shine.productiondebug.base.BaseActivity;
import com.shine.productiondebug.databinding.ActivitySearchBinding;
import com.shine.productiondebug.ui.adapter.SearchFragmentPageAdapter;
import com.shine.productiondebug.ui.fragment.SearchChild1Fragment;
import com.shine.productiondebug.ui.fragment.SearchChild2Fragment;
import com.shine.productiondebug.ui.fragment.SearchChild3Fragment;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends BaseActivity<ActivitySearchBinding, SearchViewModel> {

    @Override
    public int initContentView(Bundle savedInstanceState) {
        return R.layout.activity_search;
    }

    @Override
    public int initVariableId() {
        return BR.viewModel;
    }

    @Override
    public SearchViewModel initViewModel() {
        return new SearchViewModel(this);
    }

    @Override
    public int appTheme() {
        return Constants.THEME_FULL;
    }

    @Override
    public void initData() {
        super.initData();
        setStatusBar();

        List<Fragment> fragmentList = new ArrayList<>();
        fragmentList.add(new SearchChild1Fragment());
        fragmentList.add(new SearchChild2Fragment());
        fragmentList.add(new SearchChild3Fragment());

        SearchFragmentPageAdapter searchFragmentPageAdapter = new SearchFragmentPageAdapter(this, fragmentList);
        binding.viewPage2.setAdapter(searchFragmentPageAdapter);

        TabLayoutMediator tabLayoutMediator = new TabLayoutMediator(binding.tabLayout, binding.viewPage2, true, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                switch (position) {
                    case 0:
                        tab.setText("总流程检验报告");
                        break;
                    case 1:
                        tab.setText("子流程检验报告");
                        break;
                    case 2:
                        tab.setText("合格证");
                        break;
                }
            }
        });
        tabLayoutMediator.attach();

        binding.back.setOnClickListener(this);
        binding.search.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            default:
                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                default:
                    break;
            }
        }
    }


}
