package com.shine.productiondebug.ui.adapter;

import androidx.recyclerview.widget.RecyclerView;

import com.shine.productiondebug.R;
import com.shine.productiondebug.bean.BaseBean;

import cn.bingoogolapple.baseadapter.BGARecyclerViewAdapter;
import cn.bingoogolapple.baseadapter.BGAViewHolderHelper;

public class CommonListPopAdapter extends BGARecyclerViewAdapter<String> {

    public CommonListPopAdapter(RecyclerView recyclerView) {
        super(recyclerView, R.layout.item_list_pop);
    }

    @Override
    protected void fillData(BGAViewHolderHelper helper, int position, String str) {
        helper.setText(R.id.dataText, str);
    }
}
