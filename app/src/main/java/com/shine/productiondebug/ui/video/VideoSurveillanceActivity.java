package com.shine.productiondebug.ui.video;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;

import androidx.annotation.Nullable;

import com.shine.productiondebug.BR;
import com.shine.productiondebug.R;
import com.shine.productiondebug.base.BaseActivity;
import com.shine.productiondebug.databinding.ActivityVideoSurveillanceBinding;
import com.shine.productiondebug.utils.HikUtil;

public class VideoSurveillanceActivity extends BaseActivity<ActivityVideoSurveillanceBinding, VideoSurveillanceViewModel> {

//    海康：
//    rtsp://[username]:[password]@[ip]:[port]/[codec]/[channel]/[subtype]/av_stream
//    username: 用户名。例如admin。
//    password: 密码。例如12345。
//    ip: 为设备IP。例如 192.0.0.64。
//    port: 端口号默认为554，若为默认可不填写。
//    codec：有h264、MPEG-4、mpeg4这几种。
//    channel: 通道号，起始为1。例如通道1，则为ch1。
//    subtype: 码流类型，主码流为main，辅码流为sub。

//rtsp://admin:admin123@192.168.1.166/h264/ch1/main/av_stream

    //----------------------------------------------------------------------------------------------
    private static final int PLAY_HIK_STREAM_CODE = 1001;
    private static final int PLAY_HIK_STREAM_CODE_2 = 1002;
    private static final String IP_ADDRESS = "192.168.1.162";
    private static final String IP_ADDRESS_2 = "172.16.19.181";
    private static final int PORT = 8000;
    private static final String USER_NAME = "admin";
    private static final String USER_NAME_2 = "admin";
    private static final String PASSWORD = "admin123";
    private static final String PASSWORD_2 = "admin123";
    //----------------------------------------------------------------------------------------------

    private Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case PLAY_HIK_STREAM_CODE:
                    hikUtil.playOrStopStream();
                    break;
                case PLAY_HIK_STREAM_CODE_2:
                    hikUtil2.playOrStopStream();
                    break;
                default:
                    break;
            }
            return false;
        }
    });
    private HikUtil hikUtil;
    private HikUtil hikUtil2;

    @Override
    public int initContentView(Bundle savedInstanceState) {
        return R.layout.activity_video_surveillance;
    }

    @Override
    public int initVariableId() {
        return BR.viewModel;
    }

    @Override
    public VideoSurveillanceViewModel initViewModel() {
        return new VideoSurveillanceViewModel(this);
    }

    @Override
    public void initData() {
        super.initData();

        HikUtil.initSDK();
        hikUtil = new HikUtil();
        hikUtil.initView(binding.surfaceViewLeft);
        hikUtil.setDeviceData(IP_ADDRESS, PORT, USER_NAME, PASSWORD);
        hikUtil.loginDevice(mHandler, PLAY_HIK_STREAM_CODE);

        hikUtil2 = new HikUtil();
        hikUtil2.initView(binding.surfaceViewRight);
        hikUtil2.setDeviceData(IP_ADDRESS_2, PORT, USER_NAME_2, PASSWORD_2);
        hikUtil2.loginDevice(mHandler, PLAY_HIK_STREAM_CODE_2);

        binding.back.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            default:
                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                default:
                    break;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        hikUtil.playOrStopStream();
        hikUtil2.playOrStopStream();
    }
}
