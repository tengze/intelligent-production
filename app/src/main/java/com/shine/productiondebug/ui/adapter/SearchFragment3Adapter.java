package com.shine.productiondebug.ui.adapter;

import android.graphics.Color;

import androidx.recyclerview.widget.RecyclerView;

import com.shine.productiondebug.R;
import com.shine.productiondebug.bean.CertificateBean;

import java.util.ArrayList;
import java.util.List;

import cn.bingoogolapple.baseadapter.BGARecyclerViewAdapter;
import cn.bingoogolapple.baseadapter.BGAViewHolderHelper;
import cn.bingoogolapple.swipeitemlayout.BGASwipeItemLayout;

public class SearchFragment3Adapter extends BGARecyclerViewAdapter<CertificateBean.CertificateModelDTO> {
    /**
     * 当前处于打开状态的item
     */
    private List<BGASwipeItemLayout> mOpenedSil = new ArrayList<>();

    public SearchFragment3Adapter(RecyclerView recyclerView) {
        super(recyclerView, R.layout.item_certificate);
    }

    @Override
    public void setItemChildListener(BGAViewHolderHelper viewHolderHelper, int viewType) {
        BGASwipeItemLayout swipeItemLayout = viewHolderHelper.getView(R.id.sil_item_bgaswipe_root);
        swipeItemLayout.setDelegate(new BGASwipeItemLayout.BGASwipeItemLayoutDelegate() {
            @Override
            public void onBGASwipeItemLayoutOpened(BGASwipeItemLayout swipeItemLayout) {
                closeOpenedSwipeItemLayoutWithAnim();
                mOpenedSil.add(swipeItemLayout);
            }

            @Override
            public void onBGASwipeItemLayoutClosed(BGASwipeItemLayout swipeItemLayout) {
                mOpenedSil.remove(swipeItemLayout);
            }

            @Override
            public void onBGASwipeItemLayoutStartOpen(BGASwipeItemLayout swipeItemLayout) {
                closeOpenedSwipeItemLayoutWithAnim();
            }
        });
//        viewHolderHelper.setItemChildClickListener(R.id.tv_item_bgaswipe_delete);
//        viewHolderHelper.setItemChildLongClickListener(R.id.tv_item_bgaswipe_delete);
    }

    @Override
    public void fillData(BGAViewHolderHelper helper, int position, CertificateBean.CertificateModelDTO model) {
        helper
                .setText(R.id.InspectionUser, model.getProductionUserName())
                .setText(R.id.ProductionTime, model.getProductionTime())
                .setText(R.id.QualityTime, model.getQualityTime())
                .setText(R.id.CheckName, model.getQualityUserName())
        ;

        if (model.isProductionResult()) {
            helper.setText(R.id.ProductionResult, "[合格]");
            helper.setTextColor(R.id.ProductionResult, Color.parseColor("#05EE44"));
        } else {
            helper.setText(R.id.ProductionResult, "[不合格]");
            helper.setTextColor(R.id.ProductionResult, Color.parseColor("#FFED4134"));
        }

        if (model.isQualityResult()) {
            helper.setText(R.id.QualityResult, "[合格]");
            helper.setTextColor(R.id.QualityResult, Color.parseColor("#05EE44"));
        } else {
            helper.setText(R.id.QualityResult, "[不合格]");
            helper.setTextColor(R.id.QualityResult, Color.parseColor("#FFED4134"));
        }

        helper.setItemChildClickListener(R.id.tv_item_bgaswipe_delete);
        helper.setItemChildClickListener(R.id.tv_item_bgaswipe_select);
    }

    public void closeOpenedSwipeItemLayoutWithAnim() {
        for (BGASwipeItemLayout sil : mOpenedSil) {
            sil.closeWithAnim();
        }
        mOpenedSil.clear();
    }
}