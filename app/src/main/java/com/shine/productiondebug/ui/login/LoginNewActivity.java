package com.shine.productiondebug.ui.login;

import android.Manifest;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;

import com.google.gson.Gson;
import com.shine.productiondebug.R;
import com.shine.productiondebug.app.Constants;
import com.shine.productiondebug.base.BaseActivity;
import com.shine.productiondebug.bean.BatchNumBean;
import com.shine.productiondebug.bean.FactoryNumBean;
import com.shine.productiondebug.bean.LoginBean;
import com.shine.productiondebug.databinding.ActivityLoginNewBinding;
import com.shine.productiondebug.http.ApiException;
import com.shine.productiondebug.http.BaseReturn;
import com.shine.productiondebug.http.ECHttpResponse;
import com.shine.productiondebug.http.ECObserver;
import com.shine.productiondebug.http.ECObserverString;
import com.shine.productiondebug.http.RetrofitClient;
import com.shine.productiondebug.http.RxUtils;
import com.shine.productiondebug.http.Service;
import com.shine.productiondebug.ui.fragment.SearchChild1Fragment;
import com.shine.productiondebug.ui.fragment.SearchChild2Fragment;
import com.shine.productiondebug.ui.main.MainActivity;
import com.shine.productiondebug.utils.SPUtils;
import com.shine.productiondebug.utils.SystemUtil;
import com.tbruyelle.rxpermissions2.RxPermissions;

import io.reactivex.functions.Consumer;

public class LoginNewActivity extends BaseActivity<ActivityLoginNewBinding, LoginNewViewModel> {

    @Override
    public int initContentView(Bundle savedInstanceState) {
        return R.layout.activity_login_new;
    }

    @Override
    public int initVariableId() {
        return com.shine.productiondebug.BR.viewModel;
    }

    @Override
    public LoginNewViewModel initViewModel() {
        return new LoginNewViewModel(this);
    }

    @SuppressLint("CheckResult")
    @Override
    public void initData() {
        super.initData();
        //清楚所有本地数据
        binding.userName.setText(SPUtils.getInstance().getString(Constants.SP_LOGIN_NAME, ""));
        binding.passWord.setText(SPUtils.getInstance().getString(Constants.SP_LOGIN_PASS, ""));

//        SPUtils.getInstance().clear();

//        SPUtils.getInstance().put(Constants.SP_LOGIN_NAME, binding.userName.getText().toString());
//        SPUtils.getInstance().put(Constants.SP_LOGIN_PASS, binding.passWord.getText().toString());

        binding.login.setOnClickListener(this);

        RxPermissions mRxPermission = new RxPermissions(this);
        mRxPermission.request(Manifest.permission.READ_PHONE_STATE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .subscribe(new Consumer<Boolean>() {
                    @Override
                    public void accept(Boolean aBoolean) throws Exception {
                        if (aBoolean) {

                        } else {
                        }
                    }
                });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login:
                if (binding.userName.getText().toString().isEmpty()) {
                    ToastMessage("请输入您的用户名");
                    return;
                }

                if (binding.passWord.getText().toString().isEmpty()) {
                    ToastMessage("请输入密码");
                    return;
                }

//                switchActivity(MainActivity.class);
                loginByPassword();
                break;
            default:
                break;
        }
    }

    private void loginByPassword() {
        showLoading("");
        RetrofitClient.getInstance().create(Service.class)
                .LoginAuthentication(binding.userName.getText().toString().trim(), binding.passWord.getText().toString().trim())
                .compose(RxUtils.bindToLifecycle(this))
                .compose(RxUtils.schedulersTransformer())
                .compose(RxUtils.exceptionTransformer())
                .subscribe(new ECObserverString<String>() {
                    @Override
                    protected void _onNext(String str) {
                        dismissLoading();
                        LoginBean loginBean = new Gson().fromJson(str, LoginBean.class);
                        if (loginBean.getUsers().isEmpty()){
                            ToastMessage("用户名或密码错误");
                            return;
                        }
                        LoginBean.UsersDTO usersDTO = loginBean.getUsers().get(0);
                        SPUtils.getInstance().put(Constants.SP_LOGIN_NAME, usersDTO.getUserName());
                        SPUtils.getInstance().put(Constants.SP_LOGIN_PASS, usersDTO.getUserPwd());
                        SPUtils.getInstance().put(Constants.SP_LOGIN_USER_ID, usersDTO.getUserId());
                        switchActivity(MainActivity.class);
                    }

                    @Override
                    protected void _onError(ApiException ex) {
                        ToastMessage(ex.message);
                        dismissLoading();
                    }
                });
    }


    private void loginSuccess(LoginBean loginBean) {
//        if (isRememberPassword) {
//            SPUtils.getInstance().put(Constants.SP_IS_REMEMBER_PASSWORD, true);
//        } else {
//            SPUtils.getInstance().put(Constants.SP_IS_REMEMBER_PASSWORD, false);
//        }
//        SPUtils.getInstance().put(Constants.SP_TOKEN, loginBean.getToken());

//        LoginBeanCookie loginBeanCookie = new LoginBeanCookie();
//        loginBeanCookie.setId(loginBean.getId());
//        loginBeanCookie.setMobile(loginBean.getMobile());
//        loginBeanCookie.setVipLevel(loginBean.getVipLevel());
//        SPUtils.getInstance().put(Constants.SP_COOKIE, new Gson().toJson(loginBeanCookie));

    }


}
