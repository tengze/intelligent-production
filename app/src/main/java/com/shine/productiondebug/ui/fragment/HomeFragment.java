package com.shine.productiondebug.ui.fragment;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.shine.productiondebug.BR;
import com.shine.productiondebug.R;
import com.shine.productiondebug.app.GlideShapeImageLoader;
import com.shine.productiondebug.base.BaseFragment;
import com.shine.productiondebug.bean.CheckNumberContrastBean;
import com.shine.productiondebug.bean.GetSubFlowListBean;
import com.shine.productiondebug.bean.ProductQualificationRateBean;
import com.shine.productiondebug.bean.SpotCheckRateBean;
import com.shine.productiondebug.bean.UnqualifiedPercentageBean;
import com.shine.productiondebug.databinding.FragmentHomeBinding;
import com.shine.productiondebug.http.ApiException;
import com.shine.productiondebug.http.ECObserverString;
import com.shine.productiondebug.http.RetrofitClient;
import com.shine.productiondebug.http.RxUtils;
import com.shine.productiondebug.http.Service;
import com.shine.productiondebug.ui.fragment.viewModel.HomeViewModel;
import com.shine.productiondebug.ui.resultinput.ResultInputActivity;
import com.shine.productiondebug.ui.search.SearchActivity;
import com.shine.productiondebug.ui.video.VideoSurveillanceActivity;
import com.shine.productiondebug.utils.ScreenUtils;
import com.shine.productiondebug.view.chart.BarAndLineChart;
import com.shine.productiondebug.view.chart.HorizontalLineChart;
import com.shine.productiondebug.view.chart.ManyHorizontalLineChart;
import com.shine.productiondebug.view.chart.RingChart;
import com.youth.banner.listener.OnBannerListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomeFragment extends BaseFragment<FragmentHomeBinding, HomeViewModel> {

    @Override
    public int initContentView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return R.layout.fragment_home;
    }

    @Override
    public int initVariableId() {
        return BR.viewModel;
    }

    @Override
    public HomeViewModel initViewModel() {
        return new HomeViewModel(getActivity());
    }

    @Override
    public void initData() {
        super.initData();

        View view = binding.statusBar;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            ViewGroup.LayoutParams params = view.getLayoutParams();
            params.height = ScreenUtils.getStatusBarHeight(view.getContext());
            view.setLayoutParams(params);
        }


        binding.refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                binding.refreshLayout.finishRefresh(1000);
                productQualificationRate();
            }
        });
        binding.refreshLayout.setEnableLoadMore(false);


        binding.advertBanner.setImageLoader(new GlideShapeImageLoader());
        List<Integer> imgList = new ArrayList<>();
        imgList.add(R.mipmap.banner_img1);
        binding.advertBanner.setImages(imgList);
        binding.advertBanner.start();

        binding.advertBanner.setOnBannerListener(new OnBannerListener() {
            @Override
            public void OnBannerClick(int position) {

            }
        });

        binding.video.setOnClickListener(this);
        binding.search.setOnClickListener(this);
        binding.resultInput.setOnClickListener(this);


        binding.message.setMarqueeRepeatLimit(Integer.MAX_VALUE);
        binding.message.setFocusable(true);
        binding.message.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        binding.message.setSingleLine();
        binding.message.setFocusableInTouchMode(true);
        binding.message.setHorizontallyScrolling(true);

        productQualificationRate();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.search:
                switchActivity(SearchActivity.class);
                break;
            case R.id.resultInput:
                switchActivity(ResultInputActivity.class);
                break;
            case R.id.video:
                switchActivity(VideoSurveillanceActivity.class);
                break;
            default:
                break;
        }

    }

    /**
     * 获取包装列表信息
     */
    private void productQualificationRate() {
        showLoading("");
        RetrofitClient.getInstance().create(Service.class)
                .ProductQualificationRate()
                .compose(RxUtils.bindToLifecycle(this))
                .compose(RxUtils.schedulersTransformer())
                .compose(RxUtils.exceptionTransformer())
                .subscribe(new ECObserverString<String>() {
                    @Override
                    protected void _onNext(String str) {
//                        dismissLoading();

                        checkNumberContrast();

                        ProductQualificationRateBean rateBean = new Gson().fromJson(str, ProductQualificationRateBean.class);

                        if (rateBean.getProductQualificationRateModel().isEmpty()) {
                            binding.noDataView1.setVisibility(View.VISIBLE);
                            return;
                        } else {
                            binding.noDataView1.setVisibility(View.GONE);
                        }

                        List<Integer> leftScaleList = new ArrayList<>();
                        List<String> bottomScaleList = new ArrayList<>();
                        List<Integer> inspectDataList = new ArrayList<>();//检查
                        List<Integer> qualifiedDataList = new ArrayList<>();//合格

                        for (ProductQualificationRateBean.ProductQualificationRateModelDTO modelDTO : rateBean.getProductQualificationRateModel()) {
                            bottomScaleList.add(modelDTO.getStatisticsTime().substring(5));
                            inspectDataList.add(modelDTO.getTotalNum());
                            qualifiedDataList.add(modelDTO.getQualifiedNum());
                        }

                        int maxInspect = Collections.max(inspectDataList);
                        maxInspect = (int) Math.ceil(Double.valueOf(maxInspect) / 10) * 10;//进位取整

                        for (int i = 0; i < 6; i++) {
                            leftScaleList.add(i * maxInspect / 5);
                        }
                        Collections.reverse(leftScaleList);//list翻转

                        BarAndLineChart barAndLineChart = new BarAndLineChart(getActivity(), leftScaleList, bottomScaleList, inspectDataList, qualifiedDataList);
                        binding.barAndLineChartLayout.addView(barAndLineChart);
                    }

                    @Override
                    protected void _onError(ApiException ex) {
                        ToastMessage(ex.message);
                        dismissLoading();
                    }
                });
    }


    /**
     * 工作台检测数量对比
     */
    private void checkNumberContrast() {
//        showLoading("");
        RetrofitClient.getInstance().create(Service.class)
                .CheckNumberContrast()
                .compose(RxUtils.bindToLifecycle(this))
                .compose(RxUtils.schedulersTransformer())
                .compose(RxUtils.exceptionTransformer())
                .subscribe(new ECObserverString<String>() {
                    @Override
                    protected void _onNext(String str) {
//                        dismissLoading();

                        spotCheckRate();
                        CheckNumberContrastBean contrastBean = new Gson().fromJson(str, CheckNumberContrastBean.class);

                        if (contrastBean.getCheckNumberContrastModel().isEmpty()) {
                            binding.noDataView2.setVisibility(View.VISIBLE);
                            return;
                        } else {
                            binding.noDataView2.setVisibility(View.GONE);
                        }

                        List<String> leftScaleList = new ArrayList<>();
                        List<Integer> bottomScaleList = new ArrayList<>();
                        int stageCount = 0;
                        Map<Integer, List<Integer>> dataMap = new HashMap<>();

                        for (CheckNumberContrastBean.CheckNumberContrastModelDTO modelDTO : contrastBean.getCheckNumberContrastModel()) {
                            leftScaleList.add(modelDTO.getStatisticsTime().substring(5));
                            List<CheckNumberContrastBean.CheckNumberContrastModelDTO.ModelDTO> modelDTOList = modelDTO.getModel();
                            stageCount = modelDTOList.size();

                            for (int i = 0; i < modelDTOList.size(); i++) {
                                int stage = modelDTOList.get(i).getStage();

                                List<Integer> stageDataList;
                                if (dataMap.containsKey(stage)) {
                                    stageDataList = dataMap.get(stage);
                                } else {
                                    stageDataList = new ArrayList<>();
                                }
                                stageDataList.add(modelDTOList.get(i).getTotalNum());
                                dataMap.put(stage, stageDataList);
                            }

                        }


                        List<Integer> stageNumList = new ArrayList<>();
                        for (int i = 0; i < leftScaleList.size(); i++) {
                            int stageNum = 0;
                            for (Map.Entry<Integer, List<Integer>> m : dataMap.entrySet()) {
                                stageNum = stageNum + m.getValue().get(i);
                            }
                            stageNumList.add(stageNum);
                        }

                        int maxInspect = Collections.max(stageNumList);
                        maxInspect = (int) Math.ceil(Double.valueOf(maxInspect) / 10) * 10;//进位取整

                        for (int i = 0; i < 6; i++) {
                            bottomScaleList.add(i * maxInspect / 5);
                        }
//                        Collections.reverse(bottomScaleList);//list翻转


                        ManyHorizontalLineChart manyHorizontalLineChart = new ManyHorizontalLineChart(getActivity(), stageCount, leftScaleList, bottomScaleList,
                                dataMap);
                        binding.manyHorizontalLineChartLayout.addView(manyHorizontalLineChart);
                    }

                    @Override
                    protected void _onError(ApiException ex) {
                        ToastMessage(ex.message);
                        dismissLoading();
                    }
                });
    }

    /**
     * 质检抽查合格率
     */
    private void spotCheckRate() {
//        showLoading("");
        RetrofitClient.getInstance().create(Service.class)
                .SpotCheckRate()
                .compose(RxUtils.bindToLifecycle(this))
                .compose(RxUtils.schedulersTransformer())
                .compose(RxUtils.exceptionTransformer())
                .subscribe(new ECObserverString<String>() {
                    @Override
                    protected void _onNext(String str) {
//                        dismissLoading();
                        unqualifiedPercentage();
                        SpotCheckRateBean spotCheckRateBean = new Gson().fromJson(str, SpotCheckRateBean.class);

                        if (spotCheckRateBean.getSpotCheckRateModel().isEmpty()) {
                            binding.noDataView3.setVisibility(View.VISIBLE);
                            return;
                        } else {
                            binding.noDataView3.setVisibility(View.GONE);
                        }

                        List<Integer> bottomScaleList = new ArrayList<>();
                        List<String> leftScaleList = new ArrayList<>();

                        List<Integer> productionNumList = new ArrayList<>();//生产合格
                        List<Integer> qualityNumList = new ArrayList<>();//质检抽查
                        List<Integer> spotCheckNumList = new ArrayList<>();//抽查合格

                        for (SpotCheckRateBean.SpotCheckRateModelDTO modelDTO : spotCheckRateBean.getSpotCheckRateModel()) {
                            leftScaleList.add(modelDTO.getStatisticsTime().substring(5));
                            productionNumList.add(modelDTO.getProductionNum());
                            qualityNumList.add(modelDTO.getQualityNum());
                            spotCheckNumList.add(modelDTO.getSpotCheckNum());
                        }

                        int maxProductionNum = Collections.max(productionNumList);
                        int maxSpotCheckNum = Collections.max(spotCheckNumList);
                        int maxQualityNum = Collections.max(qualityNumList);

                        List<Integer> maxList = new ArrayList<>();
                        maxList.add(maxProductionNum);
                        maxList.add(maxSpotCheckNum);
                        maxList.add(maxQualityNum);

                        int max = Collections.max(maxList);
                        ;
                        max = (int) Math.ceil(Double.valueOf(max) / 10) * 10;//进位取整

                        for (int i = 0; i < 6; i++) {
                            bottomScaleList.add(i * max / 5);
                        }
//                        Collections.reverse(bottomScaleList);//list翻转

                        System.out.println(bottomScaleList.toString());

                        HorizontalLineChart horizontalLineChart = new HorizontalLineChart(getActivity(), leftScaleList, bottomScaleList, productionNumList, qualityNumList, spotCheckNumList);
                        binding.horizontalLineChartLayout.addView(horizontalLineChart);
                    }

                    @Override
                    protected void _onError(ApiException ex) {
                        ToastMessage(ex.message);
                        dismissLoading();
                    }
                });
    }


    /**
     * 获得子流程列表
     */
    private void getSubFlowList() {
//        showLoading("");
        RetrofitClient.getInstance().create(Service.class)
                .GetSubFlowList()
                .compose(RxUtils.bindToLifecycle(this))
                .compose(RxUtils.schedulersTransformer())
                .compose(RxUtils.exceptionTransformer())
                .subscribe(new ECObserverString<String>() {
                    @Override
                    protected void _onNext(String str) {
//                        dismissLoading();
                        GetSubFlowListBean flowListBean = new Gson().fromJson(str, GetSubFlowListBean.class);
                        unqualifiedPercentage();
                    }

                    @Override
                    protected void _onError(ApiException ex) {
                        ToastMessage(ex.message);
                        dismissLoading();
                    }
                });
    }

    /**
     * 不合格结果占比
     */
    private void unqualifiedPercentage() {
//        showLoading("");
        RetrofitClient.getInstance().create(Service.class)
                .UnqualifiedPercentage()
                .compose(RxUtils.bindToLifecycle(this))
                .compose(RxUtils.schedulersTransformer())
                .compose(RxUtils.exceptionTransformer())
                .subscribe(new ECObserverString<String>() {
                    @Override
                    protected void _onNext(String str) {
                        dismissLoading();
                        UnqualifiedPercentageBean percentageBean = new Gson().fromJson(str, UnqualifiedPercentageBean.class);

                        if (percentageBean.getUnqualifiedPercentageModel().isEmpty()) {
                            binding.noDataView4.setVisibility(View.VISIBLE);
                            return;
                        } else {
                            binding.noDataView4.setVisibility(View.GONE);
                        }
                        List<String> nameList = new ArrayList<>();
                        List<Integer> dataList = new ArrayList<>();
                        for (UnqualifiedPercentageBean.UnqualifiedPercentageModelDTO modelDTO : percentageBean.getUnqualifiedPercentageModel()) {
                            nameList.add(modelDTO.getFlowName());
                            dataList.add(modelDTO.getUnqualifiedNumber());
                        }

                        RingChart ringChart = new RingChart(getActivity(), dataList, nameList);
                        binding.ringChartLayout.addView(ringChart);
                    }

                    @Override
                    protected void _onError(ApiException ex) {
                        ToastMessage(ex.message);
                        dismissLoading();
                    }
                });
    }


}