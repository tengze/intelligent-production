package com.shine.productiondebug.ui.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.interfaces.OnCancelListener;
import com.lxj.xpopup.interfaces.OnConfirmListener;
import com.lxj.xpopup.interfaces.SimpleCallback;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.shine.productiondebug.BR;
import com.shine.productiondebug.R;
import com.shine.productiondebug.app.Constants;
import com.shine.productiondebug.base.BaseFragment;
import com.shine.productiondebug.bean.BatchNumBean;
import com.shine.productiondebug.bean.FactoryNumBean;
import com.shine.productiondebug.bean.SensorNamesBean;
import com.shine.productiondebug.bean.TotalFlowReportBean;
import com.shine.productiondebug.databinding.FragmentSearchChild1Binding;
import com.shine.productiondebug.http.ApiException;
import com.shine.productiondebug.http.ECObserverString;
import com.shine.productiondebug.http.RetrofitClient;
import com.shine.productiondebug.http.RxUtils;
import com.shine.productiondebug.http.Service;
import com.shine.productiondebug.ui.adapter.SearchFragment1Adapter;
import com.shine.productiondebug.ui.fragment.viewModel.SearchChild1ViewModel;
import com.shine.productiondebug.utils.DateUtils;
import com.shine.productiondebug.view.pop.SearchBottomPopup;

import java.util.ArrayList;
import java.util.List;

public class SearchChild1Fragment extends BaseFragment<FragmentSearchChild1Binding, SearchChild1ViewModel> {
    private SearchFragment1Adapter searchFragment1Adapter;
    private String batchNum;
    private String factoryNum;
    private String sensorName;
    private Boolean result;
    private String beginTime = DateUtils.getCurrDate() + " 00:00:00";
    private String endTime = DateUtils.getCurrDate() + " 23:59:59";
    private int page = 1;
    private List<String> batchList = new ArrayList<>();
    private List<String> factoryList = new ArrayList<>();
    private List<String> sensorsList = new ArrayList<>();
    private List<String> resultList = new ArrayList<>();
    private SearchBottomPopup searchBottomPopup;

    @Override
    public int initContentView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return R.layout.fragment_search_child1;
    }

    @Override
    public int initVariableId() {
        return BR.viewModel;
    }

    @Override
    public SearchChild1ViewModel initViewModel() {
        return new SearchChild1ViewModel(getActivity());
    }

    @Override
    public void initData() {
        super.initData();

        searchFragment1Adapter = new SearchFragment1Adapter(binding.recyclerView);
        binding.recyclerView.setAdapter(searchFragment1Adapter);

        binding.refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                binding.refreshLayout.finishRefresh(1000);
                binding.refreshLayout.setEnableLoadMore(true);
                searchFragment1Adapter.clear();
                page = 1;
                getTotalFlowReport();
            }
        });
//        binding.refreshLayout.setEnableLoadMore(false);
        binding.refreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                binding.refreshLayout.finishLoadMore(1000);
                page++;
                getTotalFlowReport();
            }
        });

        binding.search.setOnClickListener(this);

        getTotalFlowReport();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.search:
                search();
                break;
        }
    }

    /**
     * 批次列表
     */
    private void getTotalFlowReport() {
        showLoading("");
        RetrofitClient.getInstance().create(Service.class)
                .GetTotalFlowReport(batchNum, factoryNum, sensorName, result, beginTime, endTime, page, Constants.PAGE_SIZE)
                .compose(RxUtils.bindToLifecycle(this))
                .compose(RxUtils.schedulersTransformer())
                .compose(RxUtils.exceptionTransformer())
                .subscribe(new ECObserverString<String>() {
                    @Override
                    protected void _onNext(String str) {
                        dismissLoading();

                        TotalFlowReportBean totalFlowReportBean = new Gson().fromJson(str, TotalFlowReportBean.class);
                        searchFragment1Adapter.addMoreData(totalFlowReportBean.getAllProcessInspectionSearch());
                        binding.totalNum.setText(totalFlowReportBean.getTotalNum() + "");

                        if (totalFlowReportBean.getAllProcessInspectionSearch().size() < Constants.PAGE_SIZE) {
                            binding.refreshLayout.setEnableLoadMore(false);
                        }
                    }

                    @Override
                    protected void _onError(ApiException ex) {
                        ToastMessage(ex.message);
                        dismissLoading();
                    }
                });
    }

    private void search() {
        resultList.clear();
        resultList.add("全部");
        resultList.add("合格");
        resultList.add("不合格");

        searchBottomPopup = new SearchBottomPopup(getActivity(), resultList, batchNum, factoryNum, sensorName, result, beginTime, endTime);
        new XPopup.Builder(getActivity())
                .moveUpToKeyboard(false) //如果不加这个，评论弹窗会移动到软键盘上面
                .enableDrag(true)
                .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
//                        .isThreeDrag(true) //是否开启三阶拖拽，如果设置enableDrag(false)则无效
                .setPopupCallback(new SimpleCallback() {
                    @Override
                    public void onShow(BasePopupView popupView) {
                    }

                    @Override
                    public void onDismiss(BasePopupView popupView) {
                    }
                })
                .asCustom(searchBottomPopup)/*.enableDrag(false)*/
                .show();
        searchBottomPopup.show();
        searchBottomPopup.setListener(new OnConfirmListener() {
            @Override
            public void onConfirm() {
            }
        }, new OnCancelListener() {
            @Override
            public void onCancel() {
            }
        }, new SearchBottomPopup.OnPopClickListener() {
            @Override
            public void onPopClick(View view) {
                if (view.getId() == R.id.nameLayout) {
                    getSensorNames(null, null);
                }
            }

            @Override
            public void onPopClick(View view, String sensorName) {
                if (view.getId() == R.id.batchNumLayout) {
                    if (sensorName.equals("全部") || TextUtils.isEmpty(sensorName)) {
                        sensorName = null;
                    }
                    getBatchNum(sensorName, null);
                }
            }

            @Override
            public void onPopClick(View view, String sensorName, String batchNum) {
                if (view.getId() == R.id.numSelectImg) {
                    if (batchNum.equals("全部") || TextUtils.isEmpty(batchNum)) {
                        batchNum = null;
                    }
                    if (sensorName.equals("全部") || TextUtils.isEmpty(sensorName)) {
                        sensorName = null;
                    }
                    getFactoryNum(sensorName, batchNum);
                }
            }

            @Override
            public void onConfirmClick(String mBatchNum, String mFactoryNum, String mSensorName, Boolean mResult, String mBeginTime, String mEndTime) {
                batchNum = mBatchNum;
                factoryNum = mFactoryNum;
                sensorName = mSensorName;
                result = mResult;
                beginTime = mBeginTime;
                endTime = mEndTime;

                binding.refreshLayout.setEnableLoadMore(true);
                searchFragment1Adapter.clear();
                page = 1;
                getTotalFlowReport();

            }
        });
    }

    /**
     * 批次列表
     */
    private void getBatchNum(String sensorName, String factoryNum) {
        showLoading("");
        RetrofitClient.getInstance().create(Service.class)
                .GetBatchNum(sensorName, factoryNum)
                .compose(RxUtils.bindToLifecycle(this))
                .compose(RxUtils.schedulersTransformer())
                .compose(RxUtils.exceptionTransformer())
                .subscribe(new ECObserverString<String>() {
                    @Override
                    protected void _onNext(String str) {
                        dismissLoading();
                        BatchNumBean batchNumBean = new Gson().fromJson(str, BatchNumBean.class);

                        batchList.clear();
                        batchList.add("全部");
                        for (BatchNumBean.HistoryInspectionBatchDTO batchDTO : batchNumBean.getHistoryInspectionBatch()) {
                            batchList.add(batchDTO.getBatchNum());
                        }
                        searchBottomPopup.setBatchNum(batchList);
                    }

                    @Override
                    protected void _onError(ApiException ex) {
                        ToastMessage(ex.message);
                        dismissLoading();
                    }
                });
    }

    /**
     * 获取出厂编号
     */
    private void getFactoryNum(String sensorName, String batchNum) {
        showLoading("");
        RetrofitClient.getInstance().create(Service.class)
                .GetFactoryNum(sensorName, batchNum)
                .compose(RxUtils.bindToLifecycle(this))
                .compose(RxUtils.schedulersTransformer())
                .compose(RxUtils.exceptionTransformer())
                .subscribe(new ECObserverString<String>() {
                    @Override
                    protected void _onNext(String str) {
                        dismissLoading();
                        FactoryNumBean factoryNumBean = new Gson().fromJson(str, FactoryNumBean.class);

                        factoryList.clear();
                        factoryList.add("全部");
                        for (FactoryNumBean.HistoryInspectionSensorsDTO batchDTO : factoryNumBean.getHistoryInspectionSensors()) {
                            factoryList.add(batchDTO.getFactoryNum());
                        }

                        searchBottomPopup.setFactoryNumList(factoryList);
                    }

                    @Override
                    protected void _onError(ApiException ex) {
                        ToastMessage(ex.message);
                        dismissLoading();
                    }
                });
    }

    /**
     * 获取在检传感器
     */
    private void getSensorNames(String batchNum, String factoryNum) {
        showLoading("");
        RetrofitClient.getInstance().create(Service.class)
                .GetSensorNames(batchNum, factoryNum)
                .compose(RxUtils.bindToLifecycle(this))
                .compose(RxUtils.schedulersTransformer())
                .compose(RxUtils.exceptionTransformer())
                .subscribe(new ECObserverString<String>() {
                    @Override
                    protected void _onNext(String str) {
                        dismissLoading();
                        SensorNamesBean sensorNamesBean = new Gson().fromJson(str, SensorNamesBean.class);

                        sensorsList.clear();
                        sensorsList.add("全部");
                        for (SensorNamesBean.HistoryInspectionSensorsDTO batchDTO : sensorNamesBean.getHistoryInspectionSensors()) {
                            sensorsList.add(batchDTO.getSensorName());
                        }

                        searchBottomPopup.setSensorNames(sensorsList);
                    }

                    @Override
                    protected void _onError(ApiException ex) {
                        ToastMessage(ex.message);
                        dismissLoading();
                    }
                });
    }

}