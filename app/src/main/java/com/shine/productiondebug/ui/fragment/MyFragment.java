package com.shine.productiondebug.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.shine.productiondebug.BR;
import com.shine.productiondebug.R;
import com.shine.productiondebug.app.Constants;
import com.shine.productiondebug.base.BaseFragment;
import com.shine.productiondebug.databinding.FragmentMyBinding;
import com.shine.productiondebug.ui.fragment.viewModel.MyViewModel;
import com.shine.productiondebug.ui.login.ChangePasswordActivity;
import com.shine.productiondebug.ui.login.LoginNewActivity;
import com.shine.productiondebug.ui.main.MainActivity;
import com.shine.productiondebug.utils.SPUtils;

public class MyFragment extends BaseFragment<FragmentMyBinding, MyViewModel> {

    @Override
    public int initContentView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return R.layout.fragment_my;
    }

    @Override
    public int initVariableId() {
        return BR.viewModel;
    }

    @Override
    public MyViewModel initViewModel() {
        return new MyViewModel(getActivity());
    }

    @Override
    public void initData() {
        super.initData();

//        XXFragment xxFragment = new XXsFragment();
//        Bundle bundle = new Bundle();
//        bundle.putInt("id",1);
//        xxFragment.setArguments(bundle);

//        int id = getArguments().getInt("id", 0);

        binding.userName.setText(SPUtils.getInstance().getString(Constants.SP_LOGIN_NAME));
        binding.changePassword.setOnClickListener(this);
        binding.logout.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.changePassword:
                switchActivity(ChangePasswordActivity.class);
                break;
            case R.id.logout:
                getActivity().finish();
                switchActivity(LoginNewActivity.class);
                break;
            default:
                break;
        }
    }
}