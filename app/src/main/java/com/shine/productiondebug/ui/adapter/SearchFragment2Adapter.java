package com.shine.productiondebug.ui.adapter;

import android.graphics.Color;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.shine.productiondebug.R;
import com.shine.productiondebug.bean.BaseBean;
import com.shine.productiondebug.bean.SubFlowReport;

import cn.bingoogolapple.baseadapter.BGARecyclerViewAdapter;
import cn.bingoogolapple.baseadapter.BGAViewHolderHelper;

public class SearchFragment2Adapter extends BGARecyclerViewAdapter<SubFlowReport.ProcessInspectionSearchDTO> {

    public SearchFragment2Adapter(RecyclerView recyclerView) {
        super(recyclerView, R.layout.item_test_report_child);
    }

    @Override
    protected void fillData(BGAViewHolderHelper helper, int position, SubFlowReport.ProcessInspectionSearchDTO model) {
        helper.setText(R.id.factoryNum, model.getFactoryNum())
                .setText(R.id.SensorName, model.getSensorName())
                .setText(R.id.BatchNum, model.getBatchNum())
                .setText(R.id.TotalFlowName, model.getTotalFlowName())
                .setText(R.id.SubFlowName, model.getSubFlowName())
                .setText(R.id.time, model.getBeginTime() + "~" + model.getEndTime())
        ;

        if (model.isResult()) {
            helper.setText(R.id.result, "[合格]");
            helper.setTextColor(R.id.result, Color.parseColor("#05EE44"));
        } else {
            helper.setText(R.id.result, "[不合格]");
            helper.setTextColor(R.id.result, Color.parseColor("#FFED4134"));
        }
    }
}
