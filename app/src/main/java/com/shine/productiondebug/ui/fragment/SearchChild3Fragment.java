package com.shine.productiondebug.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.interfaces.OnCancelListener;
import com.lxj.xpopup.interfaces.OnConfirmListener;
import com.lxj.xpopup.interfaces.SimpleCallback;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.shine.productiondebug.BR;
import com.shine.productiondebug.R;
import com.shine.productiondebug.app.Constants;
import com.shine.productiondebug.base.BaseFragment;
import com.shine.productiondebug.bean.BaseBean;
import com.shine.productiondebug.bean.BatchNumBean;
import com.shine.productiondebug.bean.CertificateBean;
import com.shine.productiondebug.bean.CheckNameBean;
import com.shine.productiondebug.bean.SensorNamesBean;
import com.shine.productiondebug.bean.TotalFlowReportBean;
import com.shine.productiondebug.databinding.FragmentSearchChild3Binding;
import com.shine.productiondebug.http.ApiException;
import com.shine.productiondebug.http.ECObserverString;
import com.shine.productiondebug.http.RetrofitClient;
import com.shine.productiondebug.http.RxUtils;
import com.shine.productiondebug.http.Service;
import com.shine.productiondebug.ui.adapter.SearchFragment3Adapter;
import com.shine.productiondebug.ui.fragment.viewModel.SearchChild1ViewModel;
import com.shine.productiondebug.utils.DateUtils;
import com.shine.productiondebug.utils.SPUtils;
import com.shine.productiondebug.view.pop.SearchBottomPopup;
import com.shine.productiondebug.view.pop.SearchBottomPopup1;

import java.util.ArrayList;
import java.util.List;

import cn.bingoogolapple.baseadapter.BGADivider;
import cn.bingoogolapple.baseadapter.BGAOnItemChildClickListener;
import retrofit2.http.Query;

public class SearchChild3Fragment extends BaseFragment<FragmentSearchChild3Binding, SearchChild1ViewModel> {
    private SearchFragment3Adapter searchFragment3Adapter;
    //    private String batchNum;
//    private String factoryNum;
//    private String sensorName;
    private Boolean productionResult;
    private String pBeginTime = DateUtils.getCurrDate() + " 00:00:00";
    private String pEndTime = DateUtils.getCurrDate() + " 23:59:59";
    private String checkName;
    private Boolean qualityResult;
    private String qBeginTime = DateUtils.getCurrDate() + " 00:00:00";
    private String qEndTime = DateUtils.getCurrDate() + " 23:59:59";
    private int page = 1;
    private static List<CheckNameBean.CheckReportDTO> checkReportDTOList = new ArrayList<>();
    private List<String> productionConclusionList = new ArrayList<>();
    private List<String> auditorList = new ArrayList<>();
    private List<String> auditorConclusionList = new ArrayList<>();
    private boolean power = false;

    @Override
    public int initContentView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return R.layout.fragment_search_child3;
    }

    @Override
    public int initVariableId() {
        return BR.viewModel;
    }

    @Override
    public SearchChild1ViewModel initViewModel() {
        return new SearchChild1ViewModel(getActivity());
    }

    @Override
    public void initData() {
        super.initData();

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        searchFragment3Adapter = new SearchFragment3Adapter(binding.recyclerView);
        binding.recyclerView.setAdapter(searchFragment3Adapter);
//        binding.recyclerView.addItemDecoration(BGADivider.newBitmapDivider());
        binding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                if (RecyclerView.SCROLL_STATE_DRAGGING == newState) {
                    searchFragment3Adapter.closeOpenedSwipeItemLayoutWithAnim();
                }
            }
        });

        searchFragment3Adapter.setOnItemChildClickListener(new BGAOnItemChildClickListener() {
            @Override
            public void onItemChildClick(ViewGroup parent, View childView, int position) {
                if (!power) {
                    ToastMessage("您暂无权限");
                    return;
                }
                switch (childView.getId()) {
                    case R.id.tv_item_bgaswipe_delete:
                        updateCertificate(searchFragment3Adapter.getData().get(position).getCertificateID(), true);
                        break;
                    case R.id.tv_item_bgaswipe_select:
                        updateCertificate(searchFragment3Adapter.getData().get(position).getCertificateID(), false);
                        break;
                    default:
                        break;
                }
            }
        });

        binding.refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                binding.refreshLayout.finishRefresh(1000);
                binding.refreshLayout.setEnableLoadMore(true);
                searchFragment3Adapter.clear();
                page = 1;
                getBatchNum();
            }
        });
//        binding.refreshLayout.setEnableLoadMore(false);
        binding.refreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                binding.refreshLayout.finishLoadMore(1000);
                page++;
                getBatchNum();
            }
        });


        binding.search.setOnClickListener(this);

        judgmentAuthority();
        getCheckName();
        getBatchNum();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.search:
                search();
                break;
        }
    }

    /**
     * 是否有修改合格证权限
     */
    private void judgmentAuthority() {
//        showLoading("");
        RetrofitClient.getInstance().create(Service.class)
                .JudgmentAuthority(SPUtils.getInstance().getString(Constants.SP_LOGIN_USER_ID))
                .compose(RxUtils.bindToLifecycle(this))
                .compose(RxUtils.schedulersTransformer())
                .compose(RxUtils.exceptionTransformer())
                .subscribe(new ECObserverString<String>() {
                    @Override
                    protected void _onNext(String str) {
                        dismissLoading();
                        power = str.equals("true");
                    }

                    @Override
                    protected void _onError(ApiException ex) {
                        ToastMessage(ex.message);
                        dismissLoading();
                    }
                });
    }

    /**
     * 批次列表
     */
    private void getBatchNum() {
        showLoading("");
        RetrofitClient.getInstance().create(Service.class)
                .GetCertificate(productionResult, pBeginTime, pEndTime, checkName, qualityResult, qBeginTime, qEndTime, page, Constants.PAGE_SIZE)
                .compose(RxUtils.bindToLifecycle(this))
                .compose(RxUtils.schedulersTransformer())
                .compose(RxUtils.exceptionTransformer())
                .subscribe(new ECObserverString<String>() {
                    @Override
                    protected void _onNext(String str) {
                        dismissLoading();

                        CertificateBean certificateBean = new Gson().fromJson(str, CertificateBean.class);
                        searchFragment3Adapter.addMoreData(certificateBean.getCertificateModel());
                        binding.totalNum.setText(certificateBean.getTotalNum() + "");

                        if (certificateBean.getCertificateModel().size() < Constants.PAGE_SIZE) {
                            binding.refreshLayout.setEnableLoadMore(false);
                        }
                    }

                    @Override
                    protected void _onError(ApiException ex) {
                        ToastMessage(ex.message);
                        dismissLoading();
                    }
                });
    }

    /**
     * 获取在检传感器
     */
    private void getCheckName() {
//        showLoading("");
        RetrofitClient.getInstance().create(Service.class)
                .GetCheckName()
                .compose(RxUtils.bindToLifecycle(this))
                .compose(RxUtils.schedulersTransformer())
                .compose(RxUtils.exceptionTransformer())
                .subscribe(new ECObserverString<String>() {
                    @Override
                    protected void _onNext(String str) {
                        dismissLoading();
                        CheckNameBean checkNameBean = new Gson().fromJson(str, CheckNameBean.class);
                        checkReportDTOList.addAll(checkNameBean.getCheckReport());
                    }

                    @Override
                    protected void _onError(ApiException ex) {
                        ToastMessage(ex.message);
                        dismissLoading();
                    }
                });
    }

    /**
     * 修改合格证
     */
    private void updateCertificate(String certificateID, boolean result) {
        showLoading("");
        RetrofitClient.getInstance().create(Service.class)
                .UpdateCertificate(SPUtils.getInstance().getString(Constants.SP_LOGIN_NAME), SPUtils.getInstance().getString(Constants.SP_LOGIN_USER_ID), certificateID, result)
                .compose(RxUtils.bindToLifecycle(this))
                .compose(RxUtils.schedulersTransformer())
                .compose(RxUtils.exceptionTransformer())
                .subscribe(new ECObserverString<String>() {
                    @Override
                    protected void _onNext(String str) {
                        dismissLoading();
                        if (str.equals("true")) {
                            ToastMessage("提交成功");
                            binding.refreshLayout.setEnableLoadMore(true);
                            searchFragment3Adapter.clear();
                            page = 1;
                            getBatchNum();
                        }
                    }

                    @Override
                    protected void _onError(ApiException ex) {
                        ToastMessage(ex.message);
                        dismissLoading();
                    }
                });
    }

    private void search() {
        productionConclusionList.clear();
        auditorList.clear();
        auditorConclusionList.clear();

        auditorList.add("全部");

        for (CheckNameBean.CheckReportDTO batchDTO : checkReportDTOList) {
            auditorList.add(batchDTO.getCheckName());
        }

        productionConclusionList.add("全部");
        productionConclusionList.add("合格");
        productionConclusionList.add("不合格");

        auditorConclusionList.add("全部");
        auditorConclusionList.add("合格");
        auditorConclusionList.add("不合格");

        SearchBottomPopup1 searchBottomPopup = new SearchBottomPopup1(getActivity(), productionConclusionList, auditorList, auditorConclusionList,
                productionResult, pBeginTime, pEndTime, checkName, qualityResult, qBeginTime, qEndTime);
        new XPopup.Builder(getActivity())
                .moveUpToKeyboard(false) //如果不加这个，评论弹窗会移动到软键盘上面
                .enableDrag(true)
                .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
//                        .isThreeDrag(true) //是否开启三阶拖拽，如果设置enableDrag(false)则无效
                .setPopupCallback(new SimpleCallback() {
                    @Override
                    public void onShow(BasePopupView popupView) {
                    }

                    @Override
                    public void onDismiss(BasePopupView popupView) {
                    }
                })
                .asCustom(searchBottomPopup)/*.enableDrag(false)*/
                .show();
        searchBottomPopup.show();
        searchBottomPopup.setListener(new OnConfirmListener() {
            @Override
            public void onConfirm() {
            }
        }, new OnCancelListener() {
            @Override
            public void onCancel() {
            }
        }, new SearchBottomPopup1.OnPopClickListener() {
            @Override
            public void onConfirmClick(int position1, int position2, int position3, String time1Start, String time1End, String time2Start, String time2End) {
                if (position1 == 0) {
                    productionResult = null;
                } else if (position1 == 1) {
                    productionResult = true;
                } else if (position1 == 2) {
                    productionResult = false;
                }

                if (position2 == 0) {
                    checkName = null;
                } else {
                    checkName = auditorList.get(position1);
                }

                if (position3 == 0) {
                    qualityResult = null;
                } else if (position3 == 1) {
                    qualityResult = true;
                } else if (position3 == 2) {
                    qualityResult = false;
                }

                pBeginTime = time1Start.equals("开始时间") ? null : time1Start;
                pEndTime = time1End.equals("结束时间") ? null : time1End;
                qBeginTime = time2Start.equals("开始时间") ? null : time2Start;
                qEndTime = time2End.equals("结束时间") ? null : time2End;

                binding.refreshLayout.setEnableLoadMore(true);
                searchFragment3Adapter.clear();
                page = 1;
                getBatchNum();

            }
        });
    }
}