package com.shine.productiondebug.ui.main;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.shine.productiondebug.BR;
import com.shine.productiondebug.R;
import com.shine.productiondebug.app.Constants;
import com.shine.productiondebug.base.BaseActivity;
import com.shine.productiondebug.databinding.ActivityMainBinding;

import cn.jpush.android.api.JPushInterface;

public class MainActivity extends BaseActivity<ActivityMainBinding, MainViewModel> {
    private MenuItem lastItem; // 上一个选中的item

    @Override
    public int initContentView(Bundle savedInstanceState) {
        return R.layout.activity_main;
    }

    @Override
    public int initVariableId() {
        return BR.viewModel;
    }

    @Override
    public MainViewModel initViewModel() {
        return new MainViewModel(this);
    }

    @Override
    public int appTheme() {
        return Constants.THEME_FULL;
    }

    /**
     * 主界面不需要支持滑动返回，重写该方法永久禁用当前界面的滑动返回功能
     *
     * @return
     */
    @Override
    public boolean isSupportSwipeBack() {
        return false;
    }

    @Override
    public void initData() {
        super.initData();
        registerMessageReceiver();
        initTitle(this, "");

        NavController navController = Navigation.findNavController(MainActivity.this, R.id.nav_host_fragment);
        binding.navView.setItemIconTintList(null);


        binding.navView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (lastItem != item) { // 判断当前点击是否为item自身
                    lastItem = item;

                    refreshItemIcon();
                    switch (item.getItemId()) {
                        case R.id.navigation_home:
                            switchStateBar(0);
                            navController.navigate(R.id.navigation_home);
                            item.setIcon(R.mipmap.tabbar_home_select);
                            break;
//                        case R.id.navigation_dashboard:
//                            switchStateBar(1);
//                            navController.navigate(R.id.navigation_dashboard);
//                            item.setIcon(R.mipmap.tabbar_loans_select);
//                            break;
//                        case R.id.navigation_notifications:
//                            switchStateBar(2);
//                            navController.navigate(R.id.navigation_notifications);
//                            item.setIcon(R.mipmap.tabbar_icon_ququan);
//                            break;
                        case R.id.navigation_launcher:
                            switchStateBar(0);
                            navController.navigate(R.id.navigation_launcher);
                            item.setIcon(R.mipmap.tabbar_my_select);
                            break;
                        default:
                            break;
                    }
                    return true;
                }
                return false;
            }
        });

        binding.navView.getMenu().getItem(0).setIcon(R.mipmap.tabbar_home_select);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            default:
                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                default:
                    break;
            }
        }
    }


    /**
     * 未选中时加载默认的图片
     */
    public void refreshItemIcon() {
        MenuItem item1 = binding.navView.getMenu().findItem(R.id.navigation_home);
        item1.setIcon(R.mipmap.tabbar_home_default);
//        MenuItem item2 = binding.navView.getMenu().findItem(R.id.navigation_dashboard);
//        item2.setIcon(R.mipmap.tabbar_loans_default);
//        MenuItem item3 = binding.navView.getMenu().findItem(R.id.navigation_notifications);
//        item3.setIcon(R.mipmap.tabbar_icon_qq);
        MenuItem item4 = binding.navView.getMenu().findItem(R.id.navigation_launcher);
        item4.setIcon(R.mipmap.tabbar_my_default);
    }

    private boolean exit;

    /**
     * 两次返回退出应用
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (exit) {
//                    finish();
                moveTaskToBack(true);
                return true;
            } else {
//                    ToastUtils.showShort("再按一次退出");
                exit = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 2000);
            }
        }
        return false;
    }


    @Override
    public boolean isMainActivity() {
        return true;
    }

    //--------------极光推送-----------------------

    private void setJPushAlias() {
        JPushInterface.setAlias(this, 1, JPushInterface.getRegistrationID(this));
    }

    //for receive customer msg from jpush server
    private MessageReceiver mMessageReceiver;
    public static final String MESSAGE_RECEIVED_ACTION = "MESSAGE_RECEIVED_ACTION";
    public static final String KEY_TITLE = "title";
    public static final String KEY_MESSAGE = "message";
    public static final String KEY_EXTRAS = "extras";

    /**
     * 注册消息队列
     */
    public void registerMessageReceiver() {
        mMessageReceiver = new MessageReceiver();
        IntentFilter filter = new IntentFilter();
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
        filter.addAction(MESSAGE_RECEIVED_ACTION);
        //   registerReceiver(mMessageReceiver, filter);
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, filter);

        //设置极光推送别名
        setJPushAlias();
    }

    public class MessageReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (MESSAGE_RECEIVED_ACTION.equals(intent.getAction())) {
                String messageStr = intent.getStringExtra(KEY_MESSAGE);
                String extras = intent.getStringExtra(KEY_EXTRAS);
                System.out.println("Main=jpush messge=:" + messageStr);
                System.out.println("Main=jpush extras=:" + extras);

//                if (messageStr.equals("下线通知")) {
//                    dictionaryCodeDao.deleteAll();
//                    intentionLevelDao.deleteAll();
//                    SPUtils.remove(HomeActivity.this, "login_time");
//                    BaseApplication.clearActivity();
//                    Bundle bundle = new Bundle();
//                    bundle.putInt(Key.POST_LOGIN, Key.POST_TO_LOGIN);
//                    switchActivity(LoginActivity.class, bundle);
//                } else if (messageStr.contains("auctionJpush")) {
//                    MessageEvent messageEvent = new MessageEvent();
//                    messageEvent.setMessage(messageStr);
//                    EventBus.getDefault().post(messageEvent);
//                }


//                    Gson gson = new Gson();
//                    MessageBean messageBean = gson.fromJson(messageStr, MessageBean.class);
//                    if (messageBean.getTitle() != null && "auctionJpush".equals(messageBean.getTitle())) {
//                        // 发布事件
//                        MessageEvent messageEvent = new MessageEvent();
//                        messageEvent.setMessage("auctionJpush");
//                        messageEvent.setAuctionNum(messageBean.getAuctionNum());
//                        EventBus.getDefault().post(messageEvent);
//                    }

//                    messageBean.setCreateTime(DateUtils.date2String(new Date(), DateUtils.COMMON_DATETIME_ALl));
//                    messageFragment.JPushMessage(messageBean);


            }
        }
    }
    //--------------极光推送-----------------------

}