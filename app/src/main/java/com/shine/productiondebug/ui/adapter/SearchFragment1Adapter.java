package com.shine.productiondebug.ui.adapter;

import android.graphics.Color;

import androidx.recyclerview.widget.RecyclerView;

import com.shine.productiondebug.R;
import com.shine.productiondebug.bean.BaseBean;
import com.shine.productiondebug.bean.TotalFlowReportBean;

import cn.bingoogolapple.baseadapter.BGARecyclerViewAdapter;
import cn.bingoogolapple.baseadapter.BGAViewHolderHelper;

public class SearchFragment1Adapter extends BGARecyclerViewAdapter<TotalFlowReportBean.AllProcessInspectionSearchDTO> {

    public SearchFragment1Adapter(RecyclerView recyclerView) {
        super(recyclerView, R.layout.item_test_report_total);
    }

    @Override
    protected void fillData(BGAViewHolderHelper helper, int position, TotalFlowReportBean.AllProcessInspectionSearchDTO model) {
        helper.setText(R.id.factoryNum, model.getFactoryNum())
                .setText(R.id.SensorName, model.getSensorName())
                .setText(R.id.BatchNum, model.getBatchNum())
                .setText(R.id.TotalFlowName, model.getTotalFlowName())
                .setText(R.id.TotalFlowID, model.getTotalFlowID())
                .setText(R.id.time, model.getBeginTime() + "~" + model.getEndTime())
        ;

        if (model.getDept() == 0) {
            helper.setText(R.id.Dept, "生产");
        } else if (model.getDept() == 1) {
            helper.setText(R.id.Dept, "质量");
        } else if (model.getDept() == 2) {
            helper.setText(R.id.Dept, "研发");
        }

        if (model.isResult()) {
            helper.setText(R.id.result, "[合格]");
            helper.setTextColor(R.id.result, Color.parseColor("#05EE44"));
        } else {
            helper.setText(R.id.result, "[不合格]");
            helper.setTextColor(R.id.result, Color.parseColor("#FFED4134"));
        }

    }
}
