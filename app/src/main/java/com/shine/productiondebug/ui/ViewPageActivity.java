package com.shine.productiondebug.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.google.android.material.snackbar.Snackbar;
import com.shine.productiondebug.BR;
import com.shine.productiondebug.R;
import com.shine.productiondebug.base.BaseActivity;
import com.shine.productiondebug.databinding.ActivityViewPageBinding;
import com.shine.productiondebug.ui.main.SectionsPagerAdapter;
import com.shine.productiondebug.ui.viewModel.ViewPageViewModel;

public class ViewPageActivity extends BaseActivity<ActivityViewPageBinding, ViewPageViewModel> {


    @Override
    public int initContentView(Bundle savedInstanceState) {
        return R.layout.activity_view_page;
    }

    @Override
    public int initVariableId() {
        return BR.viewModel;
    }

    @Override
    public ViewPageViewModel initViewModel() {
        return new ViewPageViewModel(this);
    }

    @Override
    public void initData() {
        super.initData();

        initTitle(this, "");


        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        binding.viewPager.setAdapter(sectionsPagerAdapter);
        binding.tabs.setupWithViewPager(binding.viewPager);

        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            default:
                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                default:
                    break;
            }
        }
    }
}
