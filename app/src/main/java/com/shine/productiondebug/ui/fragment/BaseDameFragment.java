package com.shine.productiondebug.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.shine.productiondebug.BR;
import com.shine.productiondebug.R;
import com.shine.productiondebug.base.BaseFragment;
import com.shine.productiondebug.databinding.FragmentBaseDameBinding;
import com.shine.productiondebug.ui.fragment.viewModel.BaseBameViewModel;

public class BaseDameFragment extends BaseFragment<FragmentBaseDameBinding, BaseBameViewModel> {

    @Override
    public int initContentView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return R.layout.fragment_base_dame;
    }

    @Override
    public int initVariableId() {
        return BR.viewModel;
    }

    @Override
    public BaseBameViewModel initViewModel() {
        return new BaseBameViewModel(getActivity());
    }

    @Override
    public void initData() {
        super.initData();

//        XXFragment xxFragment = new XXsFragment();
//        Bundle bundle = new Bundle();
//        bundle.putInt("id",1);
//        xxFragment.setArguments(bundle);

        int id = getArguments().getInt("id", 0);


    }


    @Override
    public void onClick(View view) {

    }
}