package com.shine.productiondebug.ui.adapter;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.shine.productiondebug.R;
import com.shine.productiondebug.bean.BaseBean;
import com.shine.productiondebug.bean.PackListBean;

import cn.bingoogolapple.baseadapter.BGARecyclerViewAdapter;
import cn.bingoogolapple.baseadapter.BGAViewHolderHelper;

public class PackageCheckAdapter extends BGARecyclerViewAdapter<PackListBean.PackListDTO> {

    public PackageCheckAdapter(RecyclerView recyclerView) {
        super(recyclerView, R.layout.item_package_check);
    }

    @Override
    protected void fillData(BGAViewHolderHelper helper, int position, PackListBean.PackListDTO model) {
        helper.setText(R.id.checkText, model.getPackName());
        if (model.isSelect()) {
            helper.setBackgroundRes(R.id.checkText, R.drawable.bg_border_yellow_6);
            helper.setVisibility(R.id.checkImg, View.VISIBLE);
        } else {
            helper.setBackgroundRes(R.id.checkText, R.drawable.bg_border_bule_6);
            helper.setVisibility(R.id.checkImg, View.GONE);
        }
    }
}
