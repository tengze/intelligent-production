package com.shine.productiondebug.http;

import com.google.gson.annotations.SerializedName;

/**
 * Copyright (C), 2015-2018
 * FileName: ECHttpResponse
 * Author: Jesse
 * Date: 2018/9/6 15:10
 * Description: ${DESCRIPTION}
 * Version: 1.0
 */
public class ECHttpResponse<T> {

    @SerializedName("code")
    private int code;

    @SerializedName("msg")
    private String message;

    @SerializedName("data")
    private T result;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public boolean isOk() {
        return code == 0;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
