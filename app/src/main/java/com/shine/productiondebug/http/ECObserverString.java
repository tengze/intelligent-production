package com.shine.productiondebug.http;

import android.text.TextUtils;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Copyright (C), 2015-2018
 * FileName: ECObserver
 * Author: Jesse
 * Date: 2018/9/6 15:27
 * Description: ${DESCRIPTION}
 * Version: 1.0
 */
public abstract class ECObserverString<T> implements Observer<T> {

    private final static int CODE_SUCCESS = 0;

    @Override
    public void onError(Throwable e) {
        if (e instanceof ApiException) {
            _onError((ApiException) e);
        } else {
            _onError(new ApiException(e, ExceptionHandle.ERROR.UNKNOWN));
        }
    }

    @Override
    public void onSubscribe(Disposable d) {

    }

    @Override
    public void onNext(T t) {
        if (TextUtils.isEmpty((String) t)){
            _onError(new ApiException(new Throwable(), ExceptionHandle.ERROR.PARAM_ERROR));
            return;
        }

        System.out.println((String)t);
        _onNext(t);
    }

    @Override
    public void onComplete() {

    }

    protected abstract void _onNext(T t);

    /**
     * 错误回调
     */
    protected abstract void _onError(ApiException ex);
}
