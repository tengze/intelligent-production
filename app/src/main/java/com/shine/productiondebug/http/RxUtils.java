package com.shine.productiondebug.http;

import android.content.Context;

import androidx.fragment.app.Fragment;

import com.trello.rxlifecycle2.LifecycleProvider;
import com.trello.rxlifecycle2.LifecycleTransformer;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.ObservableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.CheckReturnValue;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by goldze on 2017/6/19.
 * 有关Rx的工具类
 */
public class RxUtils {
    /**
     * 生命周期绑定
     *
     * @param lifecycle Activity
     */
    public static <T> LifecycleTransformer<T> bindToLifecycle(Context lifecycle) {
        if (lifecycle instanceof LifecycleProvider) {
            return ((LifecycleProvider) lifecycle).bindToLifecycle();
        } else {
            throw new IllegalArgumentException("context not the LifecycleProvider type");
        }
    }

    /**
     * 生命周期绑定
     *
     * @param lifecycle Fragment
     */
    public static <T> LifecycleTransformer bindToLifecycle(Fragment lifecycle) {
        if (lifecycle instanceof LifecycleProvider) {
            return ((LifecycleProvider) lifecycle).bindToLifecycle();
        } else {
            throw new IllegalArgumentException("fragment not the LifecycleProvider type");
        }
    }

    /**
     * 线程调度器
     */
    @CheckReturnValue
    public static <T> ObservableTransformer<T, T> schedulersTransformer() {
        return new ObservableTransformer<T, T>() {
            @Override
            public ObservableSource<T> apply(Observable<T> upstream) {
                return upstream.subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());
            }
        };
    }

    public static <T> ObservableTransformer<T, T> exceptionTransformer() {

        return new ObservableTransformer<T, T>() {
            @Override
            public ObservableSource<T> apply(Observable<T> upstream) {
                return upstream
                        .onErrorResumeNext(new HttpResponseFunc<T>());
            }
        };
    }

    public static <T> ObservableTransformer<T, T> responseTransformer() {

        return new ObservableTransformer<T, T>() {
            @Override
            public ObservableSource<T> apply(Observable<T> upstream) {
                return upstream
                        .onErrorResumeNext(new HttpResponseFunc<T>());
            }
        };
    }

    private static class HttpResponseFunc<T> implements Function<Throwable, Observable<T>> {
        @Override
        public Observable<T> apply(Throwable t) {
            return Observable.error(ExceptionHandle.handleException(t));
        }
    }

    private static class HttpEmptyResponseFunc<T> implements Function<Throwable, Observable<String>> {
        @Override
        public Observable<String> apply(Throwable t) {
            return Observable.error(ExceptionHandle.handleException(t));
        }
    }

    public static <T> ObservableTransformer<ECHttpResponse<T>, T> combineTransformer() {

        return new ObservableTransformer<ECHttpResponse<T>, T>() {
            @Override
            public ObservableSource<T> apply(Observable<ECHttpResponse<T>> upstream) {

                return upstream.map(new Function<ECHttpResponse<T>, T>() {
                    @Override
                    public T apply(ECHttpResponse<T> tecHttpResponse) throws Exception {
                        return tecHttpResponse.getResult();
                    }
                })
                        .onErrorResumeNext(new HttpResponseFunc<T>())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());
            }
        };
    }

    public static <T> ObservableTransformer<ECHttpResponse<T>, String> combineEmptyTransformer() {

        return new ObservableTransformer<ECHttpResponse<T>, String>() {
            @Override
            public ObservableSource<String> apply(Observable<ECHttpResponse<T>> upstream) {
                return upstream.map(new Function<ECHttpResponse<T>, String>() {
                    @Override
                    public String apply(ECHttpResponse<T> tecHttpResponse) throws Exception {

                        return tecHttpResponse.getMessage();
                    }
                })
                        .onErrorResumeNext(new HttpEmptyResponseFunc<T>())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());
            }
        };
    }

}
