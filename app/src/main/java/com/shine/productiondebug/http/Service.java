package com.shine.productiondebug.http;

import com.shine.productiondebug.bean.LoginBean;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by Administrator on 2018/10/11.
 */

public interface Service {

    /**
     * 移动设备获取批次列表
     */
    @GET("GetTotalFlowReport")
    Observable<String> GetTotalFlowReport(
            @Query("batchNum") String batchNum,
            @Query("factoryNum") String factoryNum,
            @Query("sensorName") String sensorName,
            @Query("result") Boolean result,
            @Query("beginTime") String beginTime,
            @Query("endTime") String endTime,
            @Query("curPageNum") Integer curPageNum,
            @Query("pageSize") Integer pageSize
    );

    /**
     * 获取子流程检验报告
     */
    @GET("GetSubFlowReport")
    Observable<String> GetSubFlowReport(
            @Query("batchNum") String batchNum,
            @Query("factoryNum") String factoryNum,
            @Query("sensorName") String sensorName,
            @Query("result") Boolean result,
            @Query("beginTime") String beginTime,
            @Query("endTime") String endTime,
            @Query("curPageNum") Integer curPageNum,
            @Query("pageSize") Integer pageSize
    );

    /**
     * 获取合格证
     */
    @GET("GetCertificate")
    Observable<String> GetCertificate(
            @Query("productionResult") Boolean productionResult,
            @Query("pBeginTime") String pBeginTime,
            @Query("pEndTime") String pEndTime,
            @Query("checkName") String checkName,
            @Query("qualityResult") Boolean qualityResult,
            @Query("qBeginTime") String qBeginTime,
            @Query("qEndTime") String qEndTime,
            @Query("curPageNum") Integer curPageNum,
            @Query("pageSize") Integer pageSize
    );

    /**
     * 获取在检传感器
     */
    @GET("GetSensorNames")
    Observable<String> GetSensorNames(
            @Query("batchNum") String batchNum,
            @Query("factoryNum") String factoryNum
    );

    /**
     * 移动设备获取批次列表
     */
    @GET("GetBatchNum")
    Observable<String> GetBatchNum(
            @Query("sensorName") String sensorName,
            @Query("factoryNum") String factoryNum
    );

    /**
     * 获取出厂编号
     */
    @GET("GetFactoryNum")
    Observable<String> GetFactoryNum(
            @Query("sensorName") String sensorName,
            @Query("batchNum") String batchNum
    );

    /**
     * 获取审核人员
     */
    @GET("GetCheckName")
    Observable<String> GetCheckName(
    );

    /**
     * 登录
     */
    @GET("LoginAuthentication")
    Observable<String> LoginAuthentication(
            @Query("username") String username,
            @Query("psw") String psw
    );


    /**
     * 是否有修改合格证权限
     */
    @GET("JudgmentAuthority")
    Observable<String> JudgmentAuthority(
            @Query("userId") String userId
    );

    /**
     * 修改合格证
     */
    @GET("UpdateCertificate")
    Observable<String> UpdateCertificate(
            @Query("userName") String userName,
            @Query("userId") String userId,
            @Query("certificateID") String certificateID,
            @Query("result") boolean result
    );

    /**
     * 获取包装列表信息
     */
    @GET("GetPackList")
    Observable<String> GetPackList(
    );

    /**
     * 结果录入
     */
    @GET("InputtingResult")
    Observable<String> InputtingResult(
            @Query("productName") String productName,
            @Query("specification") String specification,
            @Query("batchNum") String batchNum,
            @Query("userName") String userName,
            @Query("inspectionID") String inspectionID,
            @Query("structure") String structure,
            @Query("packList") String packList
    );

    /**
     * 检测产品数量合格率
     */
    @GET("ProductQualificationRate")
    Observable<String> ProductQualificationRate(
    );

    /**
     * 工作台检测数量对比
     */
    @GET("CheckNumberContrast")
    Observable<String> CheckNumberContrast(
    );
    /**
     * 质检抽查合格率
     */
    @GET("SpotCheckRate")
    Observable<String> SpotCheckRate(
    );
    /**
     * 不合格结果占比
     */
    @GET("UnqualifiedPercentage")
    Observable<String> UnqualifiedPercentage(
    );
    /**
     * 获得子流程列表
     */
    @GET("GetSubFlowList")
    Observable<String> GetSubFlowList(
    );






    /**
     * login/login
     * 登录接口
     */
    @FormUrlEncoded
    @POST("login/login")
    Observable<ECHttpResponse<LoginBean>> login(
            @Field("appPlatform") String appPlatform,
            @Field("channel") String channel,
            @Field("code") String code,
            @Field("inviteId") Integer inviteId,
            @Field("isApp") Integer isApp,
            @Field("mobile") String mobile,
            @Field("model") String model
    );

    /**
     * 领金币、赚钱吧产品UV
     */
    @GET("uv/outlinks")
    Observable<ECHttpResponse<BaseReturn>> outlinks(
            @Query("ip") String ip,
            @Query("key") String key,
            @Query("linksId") Integer linksId,
            @Query("userid") String userid
    );



    //上传文件
//    @Headers({"Domain-Name: douban"}) // 加上 Domain-Name header
//    @Headers({"multipartFile:file"})
    @POST("common/upload")
    Observable<ECHttpResponse<FileBean>> upLoad(
//            @Field("uid") String uid,
//            @Field("token") String token,
//            @Field("ext") String ext,
//            @Part("file") RequestBody file
            @Body RequestBody Body
    );

    /**
     * 上传文件
     *
     * @param description
     * @param file
     */
    @POST("files/files")
    @Multipart
    Observable<ECHttpResponse<FileBean>> uploadFile(
            @Part("description") RequestBody description,
            @Part MultipartBody.Part file
    );

    @GET
    Observable<ECHttpResponse<BaseReturn>> getTimestamp(@Url String url);


    /**
     * login/loginpassword
     * 登录接口
     */
    @FormUrlEncoded
    @POST("login/loginpassword")
    Observable<ECHttpResponse<LoginBean>> loginpassword(
            @Field("appPlatform") String appPlatform,
            @Field("channel") String channel,
            @Field("inviteId") Integer inviteId,
            @Field("isApp") Integer isApp,
            @Field("mobile") String mobile,
            @Field("password") String password,
            @Field("model") String model,
            @Field("auto") Integer auto
    );


    /**
     * login/sendSms
     * 发送短信
     */
    @GET("login/sendSms")
    Observable<ECHttpResponse<BaseReturn>> sendSms(
            @Query("mobile") String mobile
    );
}
