package com.shine.productiondebug.http;

public class ApiException  extends Exception {

    public int code;
    public String message;

    public ApiException(Throwable throwable, int code) {
        super(throwable);
        this.code = code;
        this.message = throwable.getMessage();
    }

    public String getMsg(){

        return message;
    }

}
