package com.shine.productiondebug.http;

import android.net.ParseException;

import com.google.gson.JsonParseException;
import com.google.gson.stream.MalformedJsonException;

import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONException;

import java.net.ConnectException;

import retrofit2.HttpException;


/**
 * Created by goldze on 2017/5/11.
 */
public class ExceptionHandle {

    private static final int UNAUTHORIZED = 401;
    private static final int FORBIDDEN = 403;
    private static final int NOT_FOUND = 404;
    private static final int REQUEST_TIMEOUT = 408;
    private static final int INTERNAL_SERVER_ERROR = 500;
    private static final int SERVICE_UNAVAILABLE = 503;

    public static ApiException handleException(Throwable e) {
        ApiException ex;
        if (e instanceof HttpException) {
            HttpException httpException = (HttpException) e;
            ex = new ApiException(e, ERROR.HTTP_ERROR);
            switch (httpException.code()) {
                case UNAUTHORIZED:
                    ex.message = "操作未授权";
                    break;
                case FORBIDDEN:
                    ex.message = "请求被拒绝";
                    break;
                case NOT_FOUND:
                    ex.message = "资源不存在";
                    break;
                case REQUEST_TIMEOUT:
                    ex.message = "服务器执行超时";
                    break;
                case INTERNAL_SERVER_ERROR:
                    ex.message = "服务器内部错误";
                    break;
                case SERVICE_UNAVAILABLE:
                    ex.message = "服务器不可用";
                    break;
                default:
                    ex.message = "";
                    break;
            }
            return ex;
        } else if (e instanceof JsonParseException
                || e instanceof JSONException
                || e instanceof ParseException || e instanceof MalformedJsonException) {
            ex = new ApiException(e, ERROR.PARSE_ERROR);
            ex.message = "解析错误";
            return ex;
        } else if (e instanceof ConnectException) {
            ex = new ApiException(e, ERROR.NETWORK_ERROR);
            ex.message = "";
            return ex;
        } else if (e instanceof javax.net.ssl.SSLHandshakeException) {
            ex = new ApiException(e, ERROR.SSL_ERROR);
            ex.message = "";
            return ex;
        } else if (e instanceof ConnectTimeoutException) {
            ex = new ApiException(e, ERROR.TIMEOUT_ERROR);
            ex.message = "连接超时";
            return ex;
        } else if (e instanceof java.net.SocketTimeoutException) {
            ex = new ApiException(e, ERROR.TIMEOUT_ERROR);
            ex.message = "连接超时";
            return ex;
        } else if (e instanceof java.net.UnknownHostException) {
            ex = new ApiException(e, ERROR.TIMEOUT_ERROR);
            ex.message = "连接超时";
            return ex;
        } else if (e instanceof ApiException) {
            ex = (ApiException) e;
            switch (ex.code) {
                case ERROR.PARAM_ERROR:
                    ex.message = "参数错误";
                    break;
                case ERROR.ID_ERROR:
                    ex.message = "用户ID错误";
                    break;
                case ERROR.TOKEN_ERROR:
                    ex.message = "用户TOKEN错误";
                    break;
                case ERROR.EXECUTE_ERROR:
                    ex.message = "执行失败";
                    break;
                case ERROR.FILE_FORMAT_ERROR:
                    ex.message = "文件格式错误";
                    break;
                case ERROR.TIMEOUT_ERROR:
                    ex.message = "连接超时";
                    break;
                case ERROR.DATA_NOT_EXISTS_ERROR:
                    ex.message = "数据不存在";
                    break;
                case ERROR.PHONE_FORMAT_ERROR:
                    ex.message = "手机号格式错误";
                    break;
                case ERROR.CODE_SEND_ERROR:
                    ex.message = "验证码发送失败";
                    break;
                case ERROR.CODE_SEND_EARLY_ERROR:
                    ex.message = "验证码发送间隔太短";
                case ERROR.CODE_ERROR:
                    ex.message = "验证码错误";
                    break;
                case ERROR.CODE_OVERDUE_ERROR:
                    ex.message = "验证码超期";
                    break;
                case ERROR.USER_EXCUSE_ERROR:
                    ex.message = "用户被禁言";
                    break;
                default:
                    ex.message = "未知错误";
                    break;
            }
            return ex;
        } else {
            ex = new ApiException(e, ERROR.UNKNOWN);
            ex.message = "未知错误";
            return ex;
        }
    }


    /**
     * 约定异常 这个具体规则需要与服务端或者领导商讨定义
     */
    public class ERROR {

        /**
         * 未知错误
         */
        public static final int UNKNOWN = 1000;

        /**
         * 参数错误
         */
        public static final int PARAM_ERROR = 1001;
        /**
         * 用户ID错误
         */
        public static final int ID_ERROR = 1002;
        /**
         * 用户TOKEN错误
         */
        public static final int TOKEN_ERROR = 1003;
        /**
         * 执行失败
         */
        public static final int EXECUTE_ERROR = 1004;

        /**
         * 文件格式错误
         */
        public static final int FILE_FORMAT_ERROR = 1005;

        /**
         * 数据已存在
         */
        public static final int DATA_EXISTS_ERROR = 1006;

        /**
         * 数据不存在
         */
        public static final int DATA_NOT_EXISTS_ERROR = 1007;

        /**
         * 手机号格式错误
         */
        public static final int PHONE_FORMAT_ERROR = 1011;

        /**
         * 验证码发送间隔太短
         */
        public static final int CODE_SEND_EARLY_ERROR = 1012;

        /**
         * 验证码发送失败
         */
        public static final int CODE_SEND_ERROR = 1013;

        /**
         * 验证码错误
         */
        public static final int CODE_ERROR = 1021;

        /**
         * 验证码超期
         */
        public static final int CODE_OVERDUE_ERROR = 1022;

        /**
         * 用户被禁言
         */
        public static final int USER_EXCUSE_ERROR = 1031;

        /**
         * 解析错误
         */
        public static final int PARSE_ERROR = 10001;
        /**
         * 网络错误
         */
        public static final int NETWORK_ERROR = 10002;

        /**
         * 协议出错
         */
        public static final int HTTP_ERROR = 1003;

        /**
         * 证书出错
         */
        public static final int SSL_ERROR = 1005;

        /**
         * 连接超时
         */
        public static final int TIMEOUT_ERROR = 1006;
    }

}

